; Mon Dec 18 22:11:06 CET 2017
; 
;+ (version "3.5")
;+ (build "Build 663")


(defclass %3ACLIPS_TOP_LEVEL_SLOT_CLASS "Fake class to save top-level slot information"
	(is-a USER)
	(role abstract)
	(single-slot altura-duplex
		(type INTEGER)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot posy
		(type FLOAT)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot posx
		(type FLOAT)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot precio
		(type FLOAT)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot y
		(type FLOAT)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot x
		(type FLOAT)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot balcon
		(type SYMBOL)
		(allowed-values FALSE TRUE)
;+		(cardinality 0 1)
		(create-accessor read-write))
	(single-slot longitud
		(type FLOAT)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot latitud
		(type FLOAT)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot garaje
		(type SYMBOL)
		(allowed-values FALSE TRUE)
;+		(cardinality 0 1)
		(create-accessor read-write))
	(single-slot ascensor-piso
		(type SYMBOL)
		(allowed-values FALSE TRUE)
;+		(cardinality 0 1)
		(create-accessor read-write))
	(single-slot piscina
		(type SYMBOL)
		(allowed-values FALSE TRUE)
;+		(cardinality 0 1)
		(create-accessor read-write))
	(single-slot altura-piso
		(type INTEGER)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot habitaciones
		(type INTEGER)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot amueblada
		(type SYMBOL)
		(allowed-values FALSE TRUE)
;+		(cardinality 0 1)
		(create-accessor read-write))
	(single-slot terraza
		(type SYMBOL)
		(allowed-values FALSE TRUE)
;+		(cardinality 0 1)
		(create-accessor read-write))
	(single-slot ascensor-duplex
		(type SYMBOL)
		(allowed-values FALSE TRUE)
;+		(cardinality 0 1)
		(create-accessor read-write))
	(single-slot nombre
		(type STRING)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot admite-mascotas
		(type SYMBOL)
		(allowed-values FALSE TRUE)
;+		(cardinality 0 1)
		(create-accessor read-write))
	(single-slot superficie
		(type INTEGER)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot direccion
		(type STRING)
;+		(cardinality 1 1)
		(create-accessor read-write)))

(defclass Oferta
	(is-a USER)
	(role concrete)
	(single-slot precio
		(type FLOAT)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot longitud
		(type FLOAT)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot garaje
		(type SYMBOL)
		(allowed-values FALSE TRUE)
;+		(cardinality 0 1)
		(create-accessor read-write))
	(single-slot latitud
		(type FLOAT)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot admite-mascotas
		(type SYMBOL)
		(allowed-values FALSE TRUE)
;+		(cardinality 0 1)
		(create-accessor read-write))
	(single-slot superficie
		(type INTEGER)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot balcon
		(type SYMBOL)
		(allowed-values FALSE TRUE)
;+		(cardinality 0 1)
		(create-accessor read-write))
	(single-slot piscina
		(type SYMBOL)
		(allowed-values FALSE TRUE)
;+		(cardinality 0 1)
		(create-accessor read-write))
	(single-slot direccion
		(type STRING)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot habitaciones
		(type INTEGER)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot amueblada
		(type SYMBOL)
		(allowed-values FALSE TRUE)
;+		(cardinality 0 1)
		(create-accessor read-write))
	(single-slot terraza
		(type SYMBOL)
		(allowed-values FALSE TRUE)
;+		(cardinality 0 1)
		(create-accessor read-write)))

(defclass piso
	(is-a Oferta)
	(role concrete)
	(single-slot altura-piso
		(type INTEGER)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot ascensor-piso
		(type SYMBOL)
		(allowed-values FALSE TRUE)
;+		(cardinality 0 1)
		(create-accessor read-write)))

(defclass duplex
	(is-a Oferta)
	(role concrete)
	(single-slot altura-duplex
		(type INTEGER)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot ascensor-duplex
		(type SYMBOL)
		(allowed-values FALSE TRUE)
;+		(cardinality 0 1)
		(create-accessor read-write)))

(defclass unifamiliar
	(is-a Oferta)
	(role concrete))

(defclass servicio
	(is-a USER)
	(role concrete)
	(single-slot y
		(type FLOAT)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot nombre
		(type STRING)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot x
		(type FLOAT)
;+		(cardinality 1 1)
		(create-accessor read-write)))

(defclass parada_metro
	(is-a servicio)
	(role concrete))

(defclass parada_bus
	(is-a servicio)
	(role concrete))

(defclass parada_tram
	(is-a servicio)
	(role concrete))

(defclass hipermercado
	(is-a servicio)
	(role concrete))

(defclass zona_verde
	(is-a servicio)
	(role concrete))

(defclass escuela
	(is-a servicio)
	(role concrete))


(definstances instancies



([onto_Class4171] of piso

  (precio 1300.0)
  (superficie 50)
  (habitaciones 1)
  (altura-piso 6)
  (direccion "de Catalunya, 10  Barrio La Dreta de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.388186)
  (longitud 2.1680682)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4172] of piso

  (precio 1100.0)
  (superficie 80)
  (habitaciones 2)
  (altura-piso 3)
  (direccion "n  Barrio La Bordeta   Distrito Sants-Montjuïc   Bar")
  (latitud 41.366689)
  (longitud 2.1380396)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina TRUE)
  (ascensor-piso TRUE)
)

([onto_Class4173] of piso

  (precio 1300.0)
  (superficie 80)
  (habitaciones 3)
  (altura-piso 2)
  (direccion "o La Dreta de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.389403)
  (longitud 2.1780392)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4174] of piso

  (precio 425.0)
  (superficie 39)
  (habitaciones 1)
  (altura-piso 1)
  (direccion "o Poble Nou   Distrito Nord-Oest   Te")
  (latitud 41.575535)
  (longitud 2.0043576)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4175] of piso

  (precio 550.0)
  (superficie 70)
  (habitaciones 3)
  (direccion "e sant m")
  (latitud 42.047804)
  (longitud 2.2635041)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso FALSE)
)

([onto_Class4176] of piso

  (precio 550.0)
  (superficie 75)
  (habitaciones 3)
  (direccion "e sant m")
  (latitud 42.047804)
  (longitud 2.2635041)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso FALSE)
)

([onto_Class4177] of piso

  (precio 875.0)
  (superficie 70)
  (habitaciones 2)
  (direccion "oritzontal  Barrio Porta   Distrito Nou Barris   Bar")
  (latitud 41.438717)
  (longitud 2.1795124)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4178] of piso

  (precio 690.0)
  (superficie 60)
  (habitaciones 3)
  (altura-piso 5)
  (direccion "altasar  Barrio Sants   Distrito Sants-Montjuïc   Bar")
  (latitud 41.370987)
  (longitud 2.1355237)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4180] of piso

  (precio 1150.0)
  (superficie 81)
  (habitaciones 2)
  (altura-piso 1)
  (direccion "e Viladomat  Barrio La Nova Esquerra de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.380943)
  (longitud 2.1549774)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4181] of piso

  (precio 1150.0)
  (superficie 70)
  (habitaciones 2)
  (altura-piso 2)
  (direccion " Barrio Sants - Badal   Distrito Sants-Montjuïc   Bar")
  (latitud 41.376189)
  (longitud 2.1325339)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4182] of piso

  (precio 4200.0)
  (superficie 124)
  (habitaciones 3)
  (altura-piso 1)
  (direccion "e garcia faria  Barrio Diagonal Mar i el Front Maritim del Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.404206)
  (longitud 2.2133733)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-piso TRUE)
)

([onto_Class4183] of piso

  (precio 699.0)
  (superficie 85)
  (habitaciones 3)
  (altura-piso 7)
  (direccion "n Oriol  Urb. Can Oriol   Distrito Zona Nord - Can 'Oriol - Can Bertran ")
  (latitud 41.496113)
  (longitud 2.0342038)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina TRUE)
  (ascensor-piso TRUE)
)

([onto_Class4184] of piso

  (precio 1050.0)
  (superficie 50)
  (habitaciones 1)
  (altura-piso 1)
  (direccion "e Modolell  Barrio Sant Gervasi - Galvany   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.399331)
  (longitud 2.1363136)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4185] of piso

  (precio 825.0)
  (superficie 45)
  (habitaciones 1)
  (altura-piso 4)
  (direccion "alvador  Barrio El Raval   Distrito Ciutat Vella   Bar")
  (latitud 41.378701)
  (longitud 2.1643367)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4186] of piso

  (precio 1300.0)
  (superficie 80)
  (habitaciones 3)
  (altura-piso 2)
  (direccion "o Sant Pere - Santa Caterina i la Ribera   Distrito Ciutat Vella   Bar")
  (latitud 41.389331)
  (longitud 2.1801345)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4188] of piso

  (precio 1400.0)
  (superficie 115)
  (habitaciones 4)
  (altura-piso 4)
  (direccion "Barrio La Dreta de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.397184)
  (longitud 2.1640821)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)


([onto_Class4192] of piso

  (precio 1390.0)
  (superficie 102)
  (habitaciones 2)
  (altura-piso 7)
  (direccion "'Arago  Barrio L'Antiga Esquerra de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.388116)
  (longitud 2.1608257)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4193] of piso

  (precio 975.0)
  (superficie 85)
  (habitaciones 3)
  (altura-piso 2)
  (direccion "ito Centre   Castelld")
  (latitud 41.280970)
  (longitud 1.9797142)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4194] of piso

  (precio 925.0)
  (superficie 110)
  (habitaciones 4)
  (altura-piso 3)
  (direccion "el pare rodes  Distrito La Creu Alta   Sa")
  (latitud 41.551536)
  (longitud 2.1017735)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4195] of piso

  (precio 995.0)
  (superficie 59)
  (habitaciones 1)
  (altura-piso 1)
  (direccion " 7  Barrio Sant Gervasi - Galvany   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.398487)
  (longitud 2.1522404)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4196] of piso

  (precio 1300.0)
  (superficie 85)
  (habitaciones 2)
  (altura-piso 1)
  (direccion "cals  Barrio Diagonal Mar i el Front Maritim del Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.405999)
  (longitud 2.2130357)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4197] of piso

  (precio 1100.0)
  (superficie 45)
  (habitaciones 1)
  (altura-piso 2)
  (direccion "'en Gignas  Barrio El Gotic   Distrito Ciutat Vella   Bar")
  (latitud 41.382449)
  (longitud 2.1800685)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4198] of piso

  (precio 900.0)
  (superficie 30)
  (habitaciones 1)
  (altura-piso 3)
  (direccion "'en Gignas  Barrio El Gotic   Distrito Ciutat Vella   Bar")
  (latitud 41.380249)
  (longitud 2.1792685)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4199] of piso

  (precio 550.0)
  (superficie 60)
  (habitaciones 3)
  (altura-piso 2)
  (direccion "'Antoni Botey s/n  Distrito Montigala -Sant Cri")
  (latitud 41.451989)
  (longitud 2.2348961)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)
([onto_Class4201] of piso

  (precio 750.0)
  (superficie 70)
  (habitaciones 3)
  (altura-piso 5)
  (direccion "e Josep Feliu i Codina, 24  Distrito El Pedro   Cornella de Llob")
  (latitud 41.365222)
  (longitud 2.0723629)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4202] of piso

  (precio 1650.0)
  (superficie 70)
  (habitaciones 3)
  (altura-piso 5)
  (direccion "'Arago  Barrio L'Antiga Esquerra de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.391388)
  (longitud 2.1599751)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4203] of piso

  (precio 850.0)
  (superficie 140)
  (habitaciones 3)
  (altura-piso 1)
  (direccion "e l'Havana  Distrito La Salut - Llore")
  (latitud 41.441049)
  (longitud 2.2259282)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4204] of piso

  (precio 1650.0)
  (superficie 100)
  (habitaciones 2)
  (altura-piso 2)
  (direccion "'en Rosic  Barrio Sant Pere - Santa Caterina i la Ribera   Distrito Ciutat Vella   Bar")
  (latitud 41.385454)
  (longitud 2.1799948)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4205] of piso

  (precio 975.0)
  (superficie 55)
  (habitaciones 1)
  (direccion "e Pallars  Barrio Diagonal Mar i el Front Maritim del Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.409189)
  (longitud 2.2082098)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4206] of piso

  (precio 1400.0)
  (superficie 87)
  (habitaciones 3)
  (altura-piso 2)
  (direccion "e Roger de Lluria  Barrio La Dreta de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.393235)
  (longitud 2.1687676)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4207] of piso

  (precio 1600.0)
  (superficie 105)
  (habitaciones 5)
  (altura-piso 5)
  (direccion " Diagonal  Barrio El Parc i la Llacuna del Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.406303)
  (longitud 2.1919112)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4208] of piso

  (precio 2200.0)
  (superficie 135)
  (habitaciones 2)
  (altura-piso 3)
  (direccion "usta  Barrio Les Tres Torres   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.399056)
  (longitud 2.1284549)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4209] of piso

  (precio 1500.0)
  (superficie 89)
  (habitaciones 4)
  (altura-piso 2)
  (direccion "el Comte Borrell  Barrio La Nova Esquerra de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.382199)
  (longitud 2.1546515)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4210] of piso

  (precio 1700.0)
  (superficie 102)
  (habitaciones 3)
  (altura-piso 1)
  (direccion "e Provencals  Barrio Diagonal Mar i el Front Maritim del Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.405175)
  (longitud 2.2118592)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4211] of piso

  (precio 905.0)
  (superficie 75)
  (habitaciones 3)
  (altura-piso 4)
  (direccion "e Socrates  Barrio Sant Andreu   Distrito Sant Andreu   Bar")
  (latitud 41.433773)
  (longitud 2.1895744)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)
([onto_Class4213] of piso

  (precio 1400.0)
  (superficie 94)
  (habitaciones 3)
  (altura-piso 1)
  (direccion "el Mar  Barrio La Barceloneta   Distrito Ciutat Vella   Bar")
  (latitud 41.381572)
  (longitud 2.1894774)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4214] of piso

  (precio 1150.0)
  (superficie 78)
  (habitaciones 2)
  (altura-piso 2)
  (direccion "errisa  Barrio El Gotic   Distrito Ciutat Vella   Bar")
  (latitud 41.385143)
  (longitud 2.1754804)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4215] of piso

  (precio 950.0)
  (superficie 77)
  (habitaciones 3)
  (altura-piso 2)
  (direccion "e Samaniego  Barrio La Teixonera   Distrito Horta Guinardo   Bar")
  (latitud 41.423093)
  (longitud 2.1458185)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4216] of piso

  (precio 890.0)
  (superficie 65)
  (habitaciones 3)
  (altura-piso 1)
  (direccion "e maragall  Barrio Vilapicina i la Torre Llobeta   Distrito Nou Barris   Bar")
  (latitud 41.429127)
  (longitud 2.1679407)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4217] of piso

  (precio 650.0)
  (superficie 80)
  (habitaciones 3)
  (altura-piso 1)
  (direccion "NA MERCAT  Urb. ZONA MERCAT   Distrito Centre - Mercat ")
  (latitud 41.493042)
  (longitud 2.0361442)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4218] of piso

  (precio 2100.0)
  (superficie 147)
  (habitaciones 4)
  (altura-piso 1)
  (direccion "ge del carme  Distrito Arxius   Sant Cugat del ")
  (latitud 41.467770)
  (longitud 2.0789629)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4219] of piso

  (precio 1100.0)
  (superficie 72)
  (habitaciones 2)
  (altura-piso 2)
  (direccion "aris  Barrio La Dreta de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.394539)
  (longitud 2.1651869)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4220] of piso

  (precio 1500.0)
  (superficie 122)
  (habitaciones 4)
  (altura-piso 4)
  (direccion " del Paraŀlel  Barrio El Poble Sec - Parc de Montjuïc   Distrito Sants-Montjuïc   Bar")
  (latitud 41.373921)
  (longitud 2.1687459)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)


([onto_Class4222] of piso

  (precio 1090.0)
  (superficie 72)
  (habitaciones 2)
  (altura-piso 1)
  (direccion "de Molina  Barrio Sants   Distrito Sants-Montjuïc   Bar")
  (latitud 41.374937)
  (longitud 2.1330539)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-piso TRUE)
)

([onto_Class4225] of piso

  (precio 630.0)
  (superficie 33)
  (habitaciones 1)
  (altura-piso 1)
  (direccion " Ribalta  Barrio La Maternitat i Sant Ramon   Distrito Les Corts   Bar")
  (latitud 41.379836)
  (longitud 2.1119064)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4226] of piso

  (precio 1150.0)
  (superficie 81)
  (habitaciones 2)
  (altura-piso 1)
  (direccion "mat  Barrio La Nova Esquerra de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.382143)
  (longitud 2.1549774)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4227] of piso

  (precio 900.0)
  (superficie 90)
  (habitaciones 1)
  (direccion "e Maragall  Barrio La Font d'En Fargues   Distrito Horta Guinardo   Bar")
  (latitud 41.426058)
  (longitud 2.1681762)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)


([onto_Class4229] of piso

  (precio 1050.0)
  (superficie 63)
  (habitaciones 1)
  (altura-piso 1)
  (direccion "t  Barrio El Camp d'En Grassot i Gracia Nova   Distrito Gracia   Bar")
  (latitud 41.405429)
  (longitud 2.1690632)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4230] of piso

  (precio 1400.0)
  (superficie 70)
  (habitaciones 2)
  (altura-piso 1)
  (direccion "cals  Barrio Diagonal Mar i el Front Maritim del Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.405099)
  (longitud 2.2119357)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4231] of piso

  (precio 730.0)
  (superficie 50)
  (habitaciones 2)
  (altura-piso 3)
  (direccion "o Santa Eulalia   Distrito Santa Eulalia   Hospitalet")
  (latitud 41.365238)
  (longitud 2.1255013)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4232] of piso

  (precio 1800.0)
  (superficie 100)
  (habitaciones 3)
  (altura-piso 4)
  (direccion "e Mallorca  Barrio La Dreta de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.392841)
  (longitud 2.1646147)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4233] of piso

  (precio 1200.0)
  (superficie 80)
  (habitaciones 3)
  (altura-piso 7)
  (direccion "ia de les Corts Catalanes  Barrio Hostafrancs   Distrito Sants-Montjuïc   Bar")
  (latitud 41.371032)
  (longitud 2.1407706)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4234] of piso

  (precio 1200.0)
  (superficie 62)
  (habitaciones 2)
  (altura-piso 2)
  (direccion "La  Barrio El Raval   Distrito Ciutat Vella   Bar")
  (latitud 41.380318)
  (longitud 2.1728839)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4235] of piso

  (precio 600.0)
  (superficie 52)
  (habitaciones 2)
  (altura-piso 2)
  (direccion "ranqueses de")
  (latitud 41.647523)
  (longitud 2.2995213)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4236] of piso

  (precio 1300.0)
  (superficie 95)
  (habitaciones 3)
  (altura-piso 1)
  (direccion "e la Independencia  Barrio El Camp de l'Arpa del Clot   Distrito Sant Marti   Bar")
  (latitud 41.410378)
  (longitud 2.1802095)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4237] of piso

  (precio 800.0)
  (superficie 45)
  (habitaciones 1)
  (altura-piso 2)
  (direccion "igre, 16  Barrio El Raval   Distrito Ciutat Vella   Bar")
  (latitud 41.382582)
  (longitud 2.1643786)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4238] of piso

  (precio 1200.0)
  (superficie 70)
  (habitaciones 3)
  (altura-piso 4)
  (direccion "e Calabria  Barrio Sant Antoni   Distrito Eixample   Bar")
  (latitud 41.377934)
  (longitud 2.1562961)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4239] of piso

  (precio 750.0)
  (superficie 95)
  (habitaciones 3)
  (altura-piso 4)
  (direccion "UROPA  Barrio Espai Tolra - Els Pedrissos - Can Carner   Distrito Centre   Castellar del ")
  (latitud 41.608217)
  (longitud 2.0883754)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4240] of piso

  (precio 1500.0)
  (superficie 75)
  (habitaciones 2)
  (altura-piso 1)
  (direccion "el Perello  Barrio Diagonal Mar i el Front Maritim del Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.401158)
  (longitud 2.2074091)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina TRUE)
  (ascensor-piso TRUE)
)

([onto_Class4241] of piso

  (precio 1250.0)
  (superficie 107)
  (habitaciones 3)
  (altura-piso 2)
  (direccion "alvat Papasseit, 6  Distrito Coll Fava - Can Magi   Sant Cugat del ")
  (latitud 41.480401)
  (longitud 2.0826579)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-piso TRUE)
)

([onto_Class4242] of piso

  (precio 1500.0)
  (superficie 85)
  (habitaciones 4)
  (altura-piso 1)
  (direccion "e Joaquin Costa  Barrio El Raval   Distrito Ciutat Vella   Bar")
  (latitud 41.381571)
  (longitud 2.1653171)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4243] of piso

  (precio 1400.0)
  (superficie 82)
  (habitaciones 2)
  (altura-piso 1)
  (direccion "e Salva  Barrio El Poble Sec - Parc de Montjuïc   Distrito Sants-Montjuïc   Bar")
  (latitud 41.374267)
  (longitud 2.1656805)
  (admite-mascotas TRUE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4244] of piso

  (precio 1150.0)
  (superficie 70)
  (habitaciones 2)
  (altura-piso 7)
  (direccion "epant  Barrio La Sagrada Familia   Distrito Eixample   Bar")
  (latitud 41.408700)
  (longitud 2.1750695)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4245] of piso

  (precio 1200.0)
  (superficie 70)
  (habitaciones 2)
  (altura-piso 3)
  (direccion "'Escudellers Blancs  Barrio El Gotic   Distrito Ciutat Vella   Bar")
  (latitud 41.380960)
  (longitud 2.1773917)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4246] of piso

  (precio 1300.0)
  (superficie 80)
  (habitaciones 2)
  (altura-piso 1)
  (direccion "el Camp, 55  Barrio Sant Gervasi - La Bonanova   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.403564)
  (longitud 2.1358354)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4247] of piso

  (precio 950.0)
  (superficie 60)
  (habitaciones 2)
  (altura-piso 1)
  (direccion "el Segre  Barrio Sant Andreu   Distrito Sant Andreu   Bar")
  (latitud 41.432273)
  (longitud 2.1931258)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4248] of piso

  (precio 950.0)
  (superficie 70)
  (habitaciones 1)
  (altura-piso 8)
  (direccion "de Guipuscoa  Barrio Sant Marti de Provencals   Distrito Sant Marti   Bar")
  (latitud 41.415992)
  (longitud 2.1989333)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4249] of piso

  (precio 875.0)
  (superficie 45)
  (habitaciones 2)
  (altura-piso 2)
  (direccion "e Sant Pacia, 23  Barrio El Raval   Distrito Ciutat Vella   Bar")
  (latitud 41.377599)
  (longitud 2.1674774)
  (admite-mascotas TRUE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4250] of piso

  (precio 2500.0)
  (superficie 111)
  (habitaciones 1)
  (direccion "e la Llibertat  Barrio Vila de Gracia   Distrito Gracia   Bar")
  (latitud 41.398626)
  (longitud 2.1636208)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4251] of piso

  (precio 1500.0)
  (superficie 95)
  (habitaciones 4)
  (altura-piso 4)
  (direccion "e Balmes, 379  Barrio Sant Gervasi - La Bonanova   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.404795)
  (longitud 2.1399143)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)


([onto_Class4253] of piso

  (precio 1400.0)
  (superficie 59)
  (habitaciones 1)
  (altura-piso 9)
  (direccion "ia de les Corts Catalanes  Barrio La Nova Esquerra de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.381376)
  (longitud 2.1586033)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4254] of piso

  (precio 820.0)
  (superficie 65)
  (habitaciones 2)
  (direccion "e Pedrell  Barrio La Font d'En Fargues   Distrito Horta Guinardo   Bar")
  (latitud 41.425754)
  (longitud 2.1642936)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)
([onto_Class4256] of piso

  (precio 800.0)
  (superficie 64)
  (habitaciones 3)
  (altura-piso 1)
  (direccion "els Periodistes  Barrio El Guinardo   Distrito Horta Guinardo   Bar")
  (latitud 41.420864)
  (longitud 2.1758048)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)


([onto_Class4259] of piso

  (precio 1030.0)
  (superficie 40)
  (habitaciones 1)
  (altura-piso 8)
  (direccion "e Felip II, 70  Barrio La Sagrera   Distrito Sant Andreu   Bar")
  (latitud 41.419494)
  (longitud 2.1883749)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4260] of piso

  (precio 1350.0)
  (superficie 70)
  (habitaciones 2)
  (altura-piso 1)
  (direccion "e la Independencia  Barrio El Clot   Distrito Sant Marti   Bar")
  (latitud 41.407043)
  (longitud 2.1855666)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4262] of piso

  (precio 1500.0)
  (superficie 98)
  (habitaciones 2)
  (altura-piso 2)
  (direccion "e les Flors  Barrio El Raval   Distrito Ciutat Vella   Bar")
  (latitud 41.374447)
  (longitud 2.1663743)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4263] of piso

  (precio 1200.0)
  (superficie 68)
  (habitaciones 2)
  (altura-piso 3)
  (direccion " Barrio El Poble Sec - Parc de Montjuïc   Distrito Sants-Montjuïc   Bar")
  (latitud 41.370263)
  (longitud 2.1657703)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4265] of piso

  (precio 980.0)
  (superficie 67)
  (habitaciones 3)
  (altura-piso 7)
  (direccion "e l'Ermita de Bellvitge  Barrio Bellvitge   Distrito Bellvitge   Hospitalet")
  (latitud 41.345971)
  (longitud 2.1096491)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4266] of piso

  (precio 1200.0)
  (superficie 90)
  (habitaciones 4)
  (altura-piso 2)
  (direccion "e Pedrell  Barrio La Font d'En Fargues   Distrito Horta Guinardo   Bar")
  (latitud 41.423284)
  (longitud 2.1667694)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4267] of piso

  (precio 850.0)
  (superficie 54)
  (habitaciones 1)
  (altura-piso 2)
  (direccion "e l'Hospital  Barrio El Raval   Distrito Ciutat Vella   Bar")
  (latitud 41.378776)
  (longitud 2.1693318)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4268] of piso

  (precio 950.0)
  (superficie 30)
  (habitaciones 1)
  (direccion "arrio La Barceloneta   Distrito Ciutat Vella   Bar")
  (latitud 41.376917)
  (longitud 2.1911183)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4269] of piso

  (precio 990.0)
  (superficie 60)
  (habitaciones 2)
  (altura-piso 1)
  (direccion "e l'Estrella de Mar  Distrito Can Bou   Castelld")
  (latitud 41.268895)
  (longitud 1.9659572)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4270] of piso

  (precio 730.0)
  (superficie 45)
  (habitaciones 1)
  (altura-piso 4)
  (direccion "'en Serra  Barrio El Gotic   Distrito Ciutat Vella   Bar")
  (latitud 41.379505)
  (longitud 2.1771007)
  (admite-mascotas TRUE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)


([onto_Class4272] of piso

  (precio 1650.0)
  (superficie 160)
  (habitaciones 5)
  (altura-piso 2)
  (direccion "e sant pere  Barrio La Dreta de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.389685)
  (longitud 2.1804249)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4273] of piso

  (precio 1300.0)
  (superficie 70)
  (habitaciones 2)
  (altura-piso 2)
  (direccion "e Bellafila  Barrio El Gotic   Distrito Ciutat Vella   Bar")
  (latitud 41.380671)
  (longitud 2.1795043)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4274] of piso

  (precio 1100.0)
  (superficie 70)
  (habitaciones 1)
  (altura-piso 1)
  (direccion "els Carders  Barrio Sant Pere - Santa Caterina i la Ribera   Distrito Ciutat Vella   Bar")
  (latitud 41.385399)
  (longitud 2.1801519)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4275] of piso

  (precio 1600.0)
  (superficie 112)
  (habitaciones 1)
  (direccion "e la Cendra  Barrio El Raval   Distrito Ciutat Vella   Bar")
  (latitud 41.380751)
  (longitud 2.1654679)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4276] of piso

  (precio 1500.0)
  (superficie 90)
  (habitaciones 4)
  (altura-piso 4)
  (direccion "e Felip II  Barrio Navas   Distrito Sant Andreu   Bar")
  (latitud 41.418882)
  (longitud 2.1867154)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4277] of piso

  (precio 1250.0)
  (superficie 75)
  (habitaciones 2)
  (altura-piso 3)
  (direccion "e Valencia  Barrio L'Antiga Esquerra de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.386813)
  (longitud 2.1582315)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4278] of piso

  (precio 1650.0)
  (superficie 72)
  (habitaciones 2)
  (altura-piso 1)
  (direccion "e la Vila de Gracia  Barrio Vila de Gracia   Distrito Gracia   Bar")
  (latitud 41.398325)
  (longitud 2.1561947)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4279] of piso

  (precio 4500.0)
  (superficie 230)
  (habitaciones 5)
  (altura-piso 6)
  (direccion "o Les Tres Torres   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.404354)
  (longitud 2.1243012)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4281] of piso

  (precio 880.0)
  (superficie 72)
  (habitaciones 3)
  (altura-piso 6)
  (direccion "els Germans Serra  Barrio El Besos   Distrito Sant Marti   Bar")
  (latitud 41.417633)
  (longitud 2.2127542)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4282] of piso

  (precio 1600.0)
  (superficie 122)
  (habitaciones 3)
  (altura-piso 1)
  (direccion "e Muntaner  Barrio L'Antiga Esquerra de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.386686)
  (longitud 2.1563128)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)


([onto_Class4284] of piso

  (precio 1050.0)
  (superficie 58)
  (habitaciones 2)
  (altura-piso 5)
  (direccion "anc  Barrio Collblanc   Distrito Collblanc   Hospitalet")
  (latitud 41.374382)
  (longitud 2.1138917)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4285] of piso

  (precio 2050.0)
  (superficie 102)
  (habitaciones 3)
  (direccion "e Provenca  Barrio El Camp de l'Arpa del Clot   Distrito Sant Marti   Bar")
  (latitud 41.408675)
  (longitud 2.1790222)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4286] of piso

  (precio 900.0)
  (superficie 110)
  (habitaciones 4)
  (altura-piso 1)
  (direccion "o Pubilla Cases   Distrito Can Serra - Pubilla Cases   Hospitalet")
  (latitud 41.373241)
  (longitud 2.1039394)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4287] of piso

  (precio 650.0)
  (superficie 30)
  (habitaciones 1)
  (altura-piso 1)
  (direccion "eneral Mitre  Barrio El Putxet i el Farro   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.403746)
  (longitud 2.1480383)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4288] of piso

  (precio 850.0)
  (superficie 31)
  (habitaciones 1)
  (altura-piso 1)
  (direccion "p de viana  Barrio El Raval   Distrito Ciutat Vella   Bar")
  (latitud 41.380949)
  (longitud 2.1622657)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4289] of piso

  (precio 1700.0)
  (superficie 90)
  (habitaciones 3)
  (altura-piso 1)
  (direccion "e Cunit  Distrito Gava Mar  ")
  (latitud 41.269610)
  (longitud 2.0228807)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4290] of piso

  (precio 1600.0)
  (superficie 60)
  (habitaciones 2)
  (altura-piso 1)
  (direccion "el Comte de Santa Clara  Barrio La Barceloneta   Distrito Ciutat Vella   Bar")
  (latitud 41.376978)
  (longitud 2.1878715)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4291] of piso

  (precio 1500.0)
  (superficie 45)
  (habitaciones 1)
  (altura-piso 3)
  (direccion "e Sant Antoni Maria Claret  Barrio La Sagrada Familia   Distrito Eixample   Bar")
  (latitud 41.410009)
  (longitud 2.1743455)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4292] of piso

  (precio 780.0)
  (superficie 50)
  (habitaciones 2)
  (direccion "YA  Barrio El Baix Guinardo   Distrito Horta Guinardo   Bar")
  (latitud 41.413907)
  (longitud 2.1600326)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4293] of piso

  (precio 850.0)
  (superficie 56)
  (habitaciones 1)
  (altura-piso 2)
  (direccion "els Corders  Barrio Sant Pere - Santa Caterina i la Ribera   Distrito Ciutat Vella   Bar")
  (latitud 41.386852)
  (longitud 2.1789495)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4294] of piso

  (precio 1500.0)
  (superficie 90)
  (habitaciones 4)
  (altura-piso 2)
  (direccion "e Sant Gabriel  Barrio Vila de Gracia   Distrito Gracia   Bar")
  (latitud 41.397907)
  (longitud 2.1519993)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4295] of piso

  (precio 1200.0)
  (superficie 65)
  (habitaciones 2)
  (altura-piso 2)
  (direccion "e Sant Pere Mitja  Barrio Sant Pere - Santa Caterina i la Ribera   Distrito Ciutat Vella   Bar")
  (latitud 41.390141)
  (longitud 2.1776738)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4296] of piso

  (precio 1500.0)
  (superficie 60)
  (habitaciones 1)
  (altura-piso 4)
  (direccion "el Doctor Aiguader  Barrio Sant Pere - Santa Caterina i la Ribera   Distrito Ciutat Vella   Bar")
  (latitud 41.381777)
  (longitud 2.1859421)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4297] of piso

  (precio 1800.0)
  (superficie 70)
  (habitaciones 1)
  (altura-piso 4)
  (direccion " del Portal de l'angel, 18  Barrio El Gotic   Distrito Ciutat Vella   Bar")
  (latitud 41.385459)
  (longitud 2.1735132)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina TRUE)
  (ascensor-piso TRUE)
)


([onto_Class4299] of piso

  (precio 1150.0)
  (superficie 80)
  (habitaciones 2)
  (direccion "e Viladomat  Barrio La Nova Esquerra de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.383530)
  (longitud 2.1551754)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4300] of piso

  (precio 1700.0)
  (superficie 100)
  (habitaciones 3)
  (altura-piso 1)
  (direccion "e Provencals  Barrio Diagonal Mar i el Front Maritim del Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.406789)
  (longitud 2.2099694)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4301] of piso

  (precio 2000.0)
  (superficie 160)
  (habitaciones 4)
  (altura-piso 1)
  (direccion "a  Barrio La Dreta de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.388036)
  (longitud 2.1683905)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4302] of piso

  (precio 530.0)
  (superficie 70)
  (habitaciones 3)
  (altura-piso 4)
  (direccion "e Menendez y Pelayo  Barrio Can Palet   Distrito Sud   Te")
  (latitud 41.558318)
  (longitud 2.0221684)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4303] of piso

  (precio 1500.0)
  (superficie 60)
  (habitaciones 2)
  (altura-piso 1)
  (direccion "e les Trompetes  Barrio Sant Pere - Santa Caterina i la Ribera   Distrito Ciutat Vella   Bar")
  (latitud 41.381267)
  (longitud 2.1839664)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4304] of piso

  (precio 875.0)
  (superficie 58)
  (habitaciones 2)
  (altura-piso 3)
  (direccion "e Sant Frederic  Barrio Sants - Badal   Distrito Sants-Montjuïc   Bar")
  (latitud 41.371316)
  (longitud 2.1256423)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4305] of piso

  (precio 650.0)
  (superficie 150)
  (habitaciones 2)
  (direccion "octor Eladi Conde, 18  Els Ho")
  (latitud 41.548949)
  (longitud 1.7875103)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4306] of piso

  (precio 2250.0)
  (superficie 80)
  (habitaciones 3)
  (altura-piso 3)
  (direccion "'Antonio Lopez  Barrio El Gotic   Distrito Ciutat Vella   Bar")
  (latitud 41.382383)
  (longitud 2.1821195)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4308] of piso

  (precio 1200.0)
  (superficie 60)
  (habitaciones 2)
  (altura-piso 2)
  (direccion "ajor de Sarria  Barrio Sarria   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.403699)
  (longitud 2.1218118)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4309] of piso

  (precio 3400.0)
  (superficie 150)
  (habitaciones 5)
  (altura-piso 4)
  (direccion "o Les Tres Torres   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.400854)
  (longitud 2.1276012)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)


([onto_Class4311] of piso

  (precio 1150.0)
  (superficie 50)
  (habitaciones 2)
  (altura-piso 4)
  (direccion "e Joan de Borbo  Barrio La Barceloneta   Distrito Ciutat Vella   Bar")
  (latitud 41.373826)
  (longitud 2.1883172)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4312] of piso

  (precio 850.0)
  (superficie 53)
  (habitaciones 2)
  (altura-piso 2)
  (direccion "e l'Encarnacio  Barrio El Baix Guinardo   Distrito Horta Guinardo   Bar")
  (latitud 41.411897)
  (longitud 2.1678056)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4313] of piso

  (precio 1200.0)
  (superficie 100)
  (habitaciones 4)
  (altura-piso 3)
  (direccion "'Aribau  Barrio L'Antiga Esquerra de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.384663)
  (longitud 2.1636192)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)


([onto_Class4315] of piso

  (precio 1900.0)
  (superficie 105)
  (habitaciones 3)
  (altura-piso 2)
  (direccion "e borbo  Barrio La Barceloneta   Distrito Ciutat Vella   Bar")
  (latitud 41.379837)
  (longitud 2.1862174)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4316] of piso

  (precio 1000.0)
  (superficie 35)
  (habitaciones 1)
  (altura-piso 3)
  (direccion "e Sant Pere Mitja  Barrio Sant Pere - Santa Caterina i la Ribera   Distrito Ciutat Vella   Bar")
  (latitud 41.389642)
  (longitud 2.1795193)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4317] of piso

  (precio 1200.0)
  (superficie 95)
  (habitaciones 3)
  (altura-piso 3)
  (direccion " de Roma  Barrio La Nova Esquerra de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.380920)
  (longitud 2.1476592)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4318] of piso

  (precio 1200.0)
  (superficie 60)
  (habitaciones 1)
  (altura-piso 1)
  (direccion "e la Independencia  Barrio El Camp de l'Arpa del Clot   Distrito Sant Marti   Bar")
  (latitud 41.409842)
  (longitud 2.1809935)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4319] of piso

  (precio 1100.0)
  (superficie 94)
  (habitaciones 3)
  (altura-piso 1)
  (direccion "r  Sant A")
  (latitud 41.419994)
  (longitud 2.2177712)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4320] of piso

  (precio 1700.0)
  (superficie 85)
  (habitaciones 1)
  (altura-piso 6)
  (direccion " del Portal de l'angel, 18  Barrio El Gotic   Distrito Ciutat Vella   Bar")
  (latitud 41.385459)
  (longitud 2.1735132)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina TRUE)
  (ascensor-piso TRUE)
)

([onto_Class4321] of piso

  (precio 1080.0)
  (superficie 50)
  (habitaciones 1)
  (altura-piso 5)
  (direccion "e la Ciutat de Balaguer  Barrio Sant Gervasi - La Bonanova   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.401053)
  (longitud 2.1341843)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)


([onto_Class4323] of piso

  (precio 1500.0)
  (superficie 110)
  (habitaciones 3)
  (altura-piso 1)
  (direccion "e Marmella  Barrio El Putxet i el Farro   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.408676)
  (longitud 2.1410606)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4324] of piso

  (precio 850.0)
  (superficie 50)
  (habitaciones 1)
  (altura-piso 2)
  (direccion "el Torrent de l'Olla, 174  Barrio Vila de Gracia   Distrito Gracia   Bar")
  (latitud 41.404215)
  (longitud 2.1549682)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4325] of piso

  (precio 695.0)
  (superficie 50)
  (habitaciones 2)
  (altura-piso 2)
  (direccion "e Berenguer i Mallol, 5  Barrio La Barceloneta   Distrito Ciutat Vella   Bar")
  (latitud 41.380344)
  (longitud 2.1918851)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4326] of piso

  (precio 1200.0)
  (superficie 75)
  (habitaciones 3)
  (altura-piso 4)
  (direccion "e Tavern, 49  Barrio Sant Gervasi - Galvany   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.400807)
  (longitud 2.1419299)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4327] of piso

  (precio 1200.0)
  (superficie 100)
  (habitaciones 3)
  (altura-piso 2)
  (direccion " de Roma  Barrio La Nova Esquerra de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.383952)
  (longitud 2.1478514)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4328] of piso

  (precio 950.0)
  (superficie 77)
  (habitaciones 2)
  (direccion "e Sant Quinti  Barrio El Camp de l'Arpa del Clot   Distrito Sant Marti   Bar")
  (latitud 41.415416)
  (longitud 2.1801362)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4329] of piso

  (precio 1500.0)
  (superficie 120)
  (habitaciones 4)
  (altura-piso 8)
  (direccion "e los Castillejos, 288  Barrio La Sagrada Familia   Distrito Eixample   Bar")
  (latitud 41.407686)
  (longitud 2.1769799)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4330] of piso

  (precio 650.0)
  (superficie 35)
  (habitaciones 1)
  (direccion "ou de la rambla  Barrio El Raval   Distrito Ciutat Vella   Bar")
  (latitud 41.378795)
  (longitud 2.1763999)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso FALSE)
)

([onto_Class4331] of piso

  (precio 750.0)
  (superficie 66)
  (habitaciones 1)
  (direccion "'en Rull  Barrio El Gotic   Distrito Ciutat Vella   Bar")
  (latitud 41.381573)
  (longitud 2.1797829)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4332] of piso

  (precio 2950.0)
  (superficie 160)
  (habitaciones 4)
  (direccion "ajor de Sarria, 183  Barrio Sarria   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.403056)
  (longitud 2.1178899)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-piso TRUE)
)

([onto_Class4333] of piso

  (precio 850.0)
  (superficie 65)
  (habitaciones 2)
  (altura-piso 6)
  (direccion "e Bartrina  Barrio Sant Andreu   Distrito Sant Andreu   Bar")
  (latitud 41.440599)
  (longitud 2.1861114)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4334] of piso

  (precio 800.0)
  (superficie 62)
  (habitaciones 1)
  (altura-piso 4)
  (direccion "e Mata  Barrio El Poble Sec - Parc de Montjuïc   Distrito Sants-Montjuïc   Bar")
  (latitud 41.371299)
  (longitud 2.1688032)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4335] of piso

  (precio 1050.0)
  (superficie 60)
  (habitaciones 2)
  (altura-piso 6)
  (direccion "els Carders, 27  Barrio Sant Pere - Santa Caterina i la Ribera   Distrito Ciutat Vella   Bar")
  (latitud 41.386830)
  (longitud 2.1802329)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4336] of piso

  (precio 1490.0)
  (superficie 95)
  (habitaciones 3)
  (altura-piso 5)
  (direccion "o La Dreta de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.388122)
  (longitud 2.1714407)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4337] of piso

  (precio 950.0)
  (superficie 62)
  (habitaciones 1)
  (altura-piso 1)
  (direccion " Aires, 28  Barrio L'Antiga Esquerra de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.391803)
  (longitud 2.1465797)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4339] of piso

  (precio 1400.0)
  (superficie 92)
  (habitaciones 4)
  (altura-piso 1)
  (direccion "IA  Barrio Les Corts   Distrito Les Corts   Bar")
  (latitud 41.384792)
  (longitud 2.1352419)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4340] of piso

  (precio 900.0)
  (superficie 65)
  (habitaciones 2)
  (altura-piso 4)
  (direccion "e Tetuan, 5  Barrio La Dreta de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.394142)
  (longitud 2.1757231)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4691] of piso

  (precio 1150.0)
  (superficie 78)
  (habitaciones 2)
  (altura-piso 1)
  (direccion "  Barrio El Gotic   Distrito Ciutat Vella   Bar")
  (latitud 41.385449)
  (longitud 2.1724244)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4692] of piso

  (precio 1400.0)
  (superficie 75)
  (habitaciones 2)
  (altura-piso 5)
  (direccion " roda  Barrio Provencals del Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.409497)
  (longitud 2.2026979)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4693] of piso

  (precio 850.0)
  (superficie 75)
  (habitaciones 3)
  (altura-piso 3)
  (direccion " de la Mare de Deu de Montserrat, 233  Barrio El Guinardo   Distrito Horta Guinardo   Bar")
  (latitud 41.422693)
  (longitud 2.1746269)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4694] of piso

  (precio 4200.0)
  (superficie 240)
  (habitaciones 4)
  (altura-piso 4)
  (direccion " Farreras i Valenti  Barrio Sarria   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.397841)
  (longitud 2.1172948)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina TRUE)
  (ascensor-piso TRUE)
)

([onto_Class4696] of piso

  (precio 750.0)
  (superficie 39)
  (habitaciones 1)
  (altura-piso 2)
  (direccion "'Ali Bei, 57  Barrio El Fort Pienc   Distrito Eixample   Bar")
  (latitud 41.393482)
  (longitud 2.1805422)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4697] of piso

  (precio 1400.0)
  (superficie 55)
  (habitaciones 2)
  (altura-piso 5)
  (direccion "el General Mitre  Barrio Sant Gervasi - La Bonanova   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.403607)
  (longitud 2.1407869)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4698] of piso

  (precio 1950.0)
  (superficie 180)
  (habitaciones 3)
  (altura-piso 3)
  (direccion "e Muntaner  Barrio Sant Gervasi - La Bonanova   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.405698)
  (longitud 2.1376822)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4699] of piso

  (precio 1300.0)
  (superficie 130)
  (habitaciones 5)
  (altura-piso 3)
  (direccion "uro de Monterols, 9  Barrio Sant Gervasi - Galvany   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.402226)
  (longitud 2.1425809)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4700] of piso

  (precio 1350.0)
  (superficie 70)
  (habitaciones 3)
  (altura-piso 5)
  (direccion "e Manuel Girona, 52  Barrio Pedralbes   Distrito Les Corts   Bar")
  (latitud 41.391258)
  (longitud 2.1251823)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4701] of piso

  (precio 1750.0)
  (superficie 85)
  (habitaciones 2)
  (altura-piso 6)
  (direccion " Diagonal  Barrio El Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.406542)
  (longitud 2.2029076)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4702] of piso

  (precio 1050.0)
  (superficie 80)
  (habitaciones 2)
  (altura-piso 2)
  (direccion "el Sol  Barrio Vila de Gracia   Distrito Gracia   Bar")
  (latitud 41.399696)
  (longitud 2.1589387)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4703] of piso

  (precio 1400.0)
  (superficie 90)
  (habitaciones 2)
  (altura-piso 3)
  (direccion "e Sepulveda  Barrio Sant Antoni   Distrito Eixample   Bar")
  (latitud 41.381236)
  (longitud 2.1595781)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4704] of piso

  (precio 1200.0)
  (superficie 87)
  (habitaciones 2)
  (direccion " Diagonal  Barrio El Fort Pienc   Distrito Eixample   Bar")
  (latitud 41.401683)
  (longitud 2.1776943)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4706] of piso

  (precio 990.0)
  (superficie 129)
  (habitaciones 3)
  (direccion "'annibal  Distrito Instituts - Ponent   Gr")
  (latitud 41.604661)
  (longitud 2.2844678)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4707] of piso

  (precio 1200.0)
  (superficie 65)
  (habitaciones 3)
  (altura-piso 3)
  (direccion "el Secretari Coloma  Barrio El Camp d'En Grassot i Gracia Nova   Distrito Gracia   Bar")
  (latitud 41.409477)
  (longitud 2.1620401)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4708] of piso

  (precio 1350.0)
  (superficie 67)
  (habitaciones 2)
  (altura-piso 3)
  (direccion "e Londres  Barrio La Nova Esquerra de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.390638)
  (longitud 2.1441727)
  (admite-mascotas TRUE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)


([onto_Class4710] of piso

  (precio 850.0)
  (superficie 84)
  (habitaciones 3)
  (altura-piso 1)
  (direccion "ito Fontetes-La Farigola   Cerdanyola del ")
  (latitud 41.493579)
  (longitud 2.1424272)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4711] of piso

  (precio 2700.0)
  (superficie 138)
  (habitaciones 2)
  (altura-piso 2)
  (direccion "e la Diputacio  Barrio L'Antiga Esquerra de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.385366)
  (longitud 2.1595806)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4713] of piso

  (precio 675.0)
  (superficie 50)
  (habitaciones 2)
  (altura-piso 4)
  (direccion ", 28  Barrio La Teixonera   Distrito Horta Guinardo   Bar")
  (latitud 41.421965)
  (longitud 2.1430312)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4714] of piso

  (precio 1800.0)
  (superficie 60)
  (habitaciones 2)
  (altura-piso 3)
  (direccion "e Balmes  Barrio La Dreta de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.393734)
  (longitud 2.1573578)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4715] of piso

  (precio 1550.0)
  (superficie 85)
  (habitaciones 2)
  (altura-piso 2)
  (direccion "'En Bot  Barrio El Gotic   Distrito Ciutat Vella   Bar")
  (latitud 41.382141)
  (longitud 2.1718592)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4716] of piso

  (precio 2600.0)
  (superficie 93)
  (habitaciones 2)
  (altura-piso 1)
  (direccion "e Garcia Faria  Barrio Diagonal Mar i el Front Maritim del Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.407378)
  (longitud 2.2200397)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)


([onto_Class4718] of piso

  (precio 1190.0)
  (superficie 80)
  (habitaciones 2)
  (altura-piso 3)
  (direccion "els Cotoners  Barrio Sant Pere - Santa Caterina i la Ribera   Distrito Ciutat Vella   Bar")
  (latitud 41.385006)
  (longitud 2.1781296)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4719] of piso

  (precio 950.0)
  (superficie 65)
  (habitaciones 1)
  (altura-piso 3)
  (direccion "e Pontevedra  Barrio La Barceloneta   Distrito Ciutat Vella   Bar")
  (latitud 41.376108)
  (longitud 2.1906906)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4720] of piso

  (precio 1600.0)
  (superficie 98)
  (habitaciones 3)
  (altura-piso 2)
  (direccion "e Sant Antoni  Barrio El Raval   Distrito Ciutat Vella   Bar")
  (latitud 41.382335)
  (longitud 2.1651469)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4721] of piso

  (precio 995.0)
  (superficie 124)
  (habitaciones 4)
  (altura-piso 2)
  (direccion "oler i Carbonell  Distrito Centre Vila - La Geltru   Vil")
  (latitud 41.223768)
  (longitud 1.7223373)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4722] of piso

  (precio 1200.0)
  (superficie 145)
  (habitaciones 4)
  (altura-piso 3)
  (direccion "e Pi i Gibert  Sant A")
  (latitud 41.427066)
  (longitud 2.2251133)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4724] of piso

  (precio 1100.0)
  (superficie 54)
  (habitaciones 2)
  (altura-piso 2)
  (direccion "ca  Barrio L'Antiga Esquerra de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.390857)
  (longitud 2.1540069)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4725] of piso

  (precio 1790.0)
  (superficie 70)
  (habitaciones 1)
  (altura-piso 1)
  (direccion "el Fenix  Barrio La Bordeta   Distrito Sants-Montjuïc   Bar")
  (latitud 41.369046)
  (longitud 2.1335089)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4726] of piso

  (precio 950.0)
  (superficie 75)
  (habitaciones 3)
  (altura-piso 1)
  (direccion "ona  Barrio El Gotic   Distrito Ciutat Vella   Bar")
  (latitud 41.375188)
  (longitud 2.1848877)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4727] of piso

  (precio 1250.0)
  (superficie 60)
  (habitaciones 2)
  (altura-piso 1)
  (direccion "e roger de flor  Barrio El Fort Pienc   Distrito Eixample   Bar")
  (latitud 41.393038)
  (longitud 2.1802748)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4728] of piso

  (precio 850.0)
  (superficie 75)
  (habitaciones 3)
  (altura-piso 2)
  (direccion "e Valldoreix  Barrio La Salut   Distrito Gracia   Bar")
  (latitud 41.410610)
  (longitud 2.1501948)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4729] of piso

  (precio 850.0)
  (superficie 60)
  (habitaciones 3)
  (altura-piso 1)
  (direccion "e vallseca  Barrio Can Baro   Distrito Horta Guinardo   Bar")
  (latitud 41.415550)
  (longitud 2.1631791)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4730] of piso

  (precio 2000.0)
  (superficie 120)
  (habitaciones 3)
  (altura-piso 2)
  (direccion "o El Raval   Distrito Ciutat Vella   Bar")
  (latitud 41.381516)
  (longitud 2.1705416)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4731] of piso

  (precio 950.0)
  (superficie 68)
  (habitaciones 2)
  (altura-piso 5)
  (direccion " vallcarca, 56  Barrio Vallcarca i els Penitents   Distrito Gracia   Bar")
  (latitud 41.409934)
  (longitud 2.1475595)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4732] of piso

  (precio 1380.0)
  (superficie 80)
  (habitaciones 3)
  (altura-piso 3)
  (direccion "roel  Barrio L'Antiga Esquerra de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.388103)
  (longitud 2.1526557)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4733] of piso

  (precio 1650.0)
  (superficie 65)
  (habitaciones 3)
  (altura-piso 3)
  (direccion "'Arago  Barrio L'Antiga Esquerra de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.390478)
  (longitud 2.1600991)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4734] of piso

  (precio 2050.0)
  (superficie 80)
  (habitaciones 4)
  (altura-piso 3)
  (direccion "el Pou de l'Estany  Barrio Sant Pere - Santa Caterina i la Ribera   Distrito Ciutat Vella   Bar")
  (latitud 41.381911)
  (longitud 2.1837392)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4735] of piso

  (precio 1150.0)
  (superficie 69)
  (habitaciones 2)
  (altura-piso 4)
  (direccion "de Badal, 153  Barrio Sants - Badal   Distrito Sants-Montjuïc   Bar")
  (latitud 41.374189)
  (longitud 2.1307339)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4736] of piso

  (precio 1400.0)
  (superficie 70)
  (habitaciones 2)
  (altura-piso 2)
  (direccion "els Escudellers  Barrio El Gotic   Distrito Ciutat Vella   Bar")
  (latitud 41.380600)
  (longitud 2.1773436)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4737] of piso

  (precio 1550.0)
  (superficie 67)
  (habitaciones 3)
  (altura-piso 1)
  (direccion " Barrio Vila de Gracia   Distrito Gracia   Bar")
  (latitud 41.409293)
  (longitud 2.1562208)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4738] of piso

  (precio 1600.0)
  (superficie 64)
  (habitaciones 2)
  (altura-piso 4)
  (direccion "blenou  Barrio El Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.397605)
  (longitud 2.2041426)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4739] of piso

  (precio 1100.0)
  (superficie 57)
  (habitaciones 2)
  (altura-piso 2)
  (direccion "el Lledoner  Barrio Sant Genis Dels Agudells - Montbau   Distrito Horta Guinardo   Bar")
  (latitud 41.425604)
  (longitud 2.1372325)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4741] of piso

  (precio 1400.0)
  (superficie 98)
  (habitaciones 3)
  (altura-piso 2)
  (direccion "e Viladomat  Barrio La Nova Esquerra de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.386347)
  (longitud 2.1487184)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-piso TRUE)
)

([onto_Class4742] of piso

  (precio 2000.0)
  (superficie 73)
  (habitaciones 3)
  (altura-piso 4)
  (direccion "er  Barrio L'Antiga Esquerra de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.386924)
  (longitud 2.1588074)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4743] of piso

  (precio 2100.0)
  (superficie 85)
  (habitaciones 4)
  (altura-piso 2)
  (direccion "e Rera Palau  Barrio Sant Pere - Santa Caterina i la Ribera   Distrito Ciutat Vella   Bar")
  (latitud 41.382070)
  (longitud 2.1823947)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4744] of piso

  (precio 800.0)
  (superficie 52)
  (habitaciones 1)
  (altura-piso 1)
  (direccion "el doctor trueta  Barrio El Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.393979)
  (longitud 2.1999577)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4746] of piso

  (precio 1190.0)
  (superficie 100)
  (habitaciones 3)
  (altura-piso 3)
  (direccion "e Santa Coloma, 37  Barrio Sant Andreu   Distrito Sant Andreu   Bar")
  (latitud 41.445630)
  (longitud 2.1910766)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)

([onto_Class4747] of piso

  (precio 1450.0)
  (superficie 87)
  (habitaciones 3)
  (altura-piso 6)
  (direccion "r  Barrio Les Corts   Distrito Les Corts   Bar")
  (latitud 41.385915)
  (longitud 2.1404929)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-piso TRUE)
)


([onto_Class30003] of duplex

  (precio 1300.0)
  (superficie 86)
  (habitaciones 1)
  (altura-duplex 1)
  (direccion "el Correu Vell  Barrio El Gotic   Distrito Ciutat Vella   Bar")
  (latitud 41.382355)
  (longitud 2.1777069)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30007] of duplex

  (precio 1700.0)
  (superficie 92)
  (habitaciones 1)
  (altura-duplex 3)
  (direccion "omtal, 8  Barrio El Gotic   Distrito Ciutat Vella   Bar")
  (latitud 41.386317)
  (longitud 2.1728426)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)


([onto_Class30009] of duplex

  (precio 750.0)
  (superficie 102)
  (habitaciones 4)
  (altura-duplex 3)
  (direccion "el Doctor Zamenhof  Distrito Sant Joan - L'Aiguacuit   Vil")
  (latitud 41.220097)
  (longitud 1.7093898)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30010] of duplex

  (precio 4500.0)
  (superficie 230)
  (habitaciones 5)
  (altura-duplex 6)
  (direccion "o Les Tres Torres   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.404354)
  (longitud 2.1243012)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)


([onto_Class30012] of duplex

  (precio 1050.0)
  (superficie 60)
  (habitaciones 2)
  (altura-duplex 6)
  (direccion "els Carders, 27  Barrio Sant Pere - Santa Caterina i la Ribera   Distrito Ciutat Vella   Bar")
  (latitud 41.386830)
  (longitud 2.1802329)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30013] of duplex

  (precio 1000.0)
  (superficie 75)
  (habitaciones 2)
  (altura-duplex 1)
  (direccion "o Sant Josep   Distrito Centre   Hospitalet")
  (latitud 41.364719)
  (longitud 2.1060884)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30014] of duplex

  (precio 875.0)
  (superficie 71)
  (habitaciones 2)
  (altura-duplex 2)
  (direccion "nat  Distrito Centre   Sa")
  (latitud 41.543344)
  (longitud 2.1196435)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30015] of duplex

  (precio 1500.0)
  (superficie 110)
  (habitaciones 4)
  (altura-duplex 2)
  (direccion "arc  Urb. Pla de M")
  (latitud 41.471575)
  (longitud 2.2871869)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)

([onto_Class30016] of duplex

  (precio 790.0)
  (superficie 103)
  (habitaciones 3)
  (altura-duplex 3)
  (direccion "e sant marc, 31  Urb. 33205-0001   Barrio Ca n'Aurell   Distrito Ponent   Te")
  (latitud 41.562955)
  (longitud 2.0015555)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30017] of duplex

  (precio 990.0)
  (superficie 110)
  (habitaciones 3)
  (altura-duplex 1)
  (direccion "ito Terra Nostra - Font Pudenta   Montcada i ")
  (latitud 41.483372)
  (longitud 2.1706671)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)



([onto_Class30020] of duplex

  (precio 650.0)
  (superficie 80)
  (habitaciones 1)
  (altura-duplex 2)
  (direccion "")
  (latitud 41.441996)
  (longitud 1.8658129)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30022] of duplex

  (precio 1600.0)
  (superficie 115)
  (habitaciones 2)
  (altura-duplex 5)
  (direccion "o Provencals del Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.406901)
  (longitud 2.2064868)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30024] of duplex

  (precio 4500.0)
  (superficie 213)
  (habitaciones 5)
  (altura-duplex 6)
  (direccion "e la Bonanova  Barrio Les Tres Torres   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.401154)
  (longitud 2.1269012)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30025] of duplex

  (precio 1600.0)
  (superficie 132)
  (habitaciones 3)
  (altura-duplex 2)
  (direccion "ntro  Urb. Centro   Distrito Centre   Gr")
  (latitud 41.607592)
  (longitud 2.2850889)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30026] of duplex

  (precio 1750.0)
  (superficie 113)
  (habitaciones 3)
  (altura-duplex 1)
  (direccion "e Londres, 37  Barrio La Nova Esquerra de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.389771)
  (longitud 2.1456779)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30027] of duplex

  (precio 1300.0)
  (superficie 80)
  (habitaciones 3)
  (altura-duplex 3)
  (direccion "ou  Urb. VINYET   Distrito Vinyet-Terramar-Can Pei-Ca")
  (latitud 41.233157)
  (longitud 1.7946849)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)


([onto_Class30030] of duplex

  (precio 3400.0)
  (superficie 174)
  (habitaciones 3)
  (altura-duplex 5)
  (direccion "o El Gotic   Distrito Ciutat Vella   Bar")
  (latitud 41.386266)
  (longitud 2.1713673)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)


([onto_Class30032] of duplex

  (precio 996.0)
  (superficie 171)
  (habitaciones 3)
  (altura-duplex 3)
  (direccion "rat de la Riba, 15  Parets de")
  (latitud 41.559977)
  (longitud 2.2296253)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30033] of duplex

  (precio 1000.9999999999999)
  (superficie 172)
  (habitaciones 5)
  (altura-duplex 2)
  (direccion "rat de la Riba, 15  Parets de")
  (latitud 41.559977)
  (longitud 2.2296253)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30034] of duplex

  (precio 900.0)
  (superficie 95)
  (habitaciones 4)
  (altura-duplex 1)
  (direccion "ito Montcada Centro   Montcada i ")
  (latitud 41.483765)
  (longitud 2.1865085)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)


([onto_Class30036] of duplex

  (precio 7500.0)
  (superficie 250)
  (habitaciones 3)
  (altura-duplex 5)
  (direccion "el General Mitre  Barrio Sant Gervasi - La Bonanova   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.397935)
  (longitud 2.1329809)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30039] of duplex

  (precio 2950.0)
  (superficie 256)
  (habitaciones 3)
  (altura-duplex 3)
  (direccion "ge dels Pins Melis  Les B")
  (latitud 41.267000)
  (longitud 1.9322596)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)


([onto_Class30041] of duplex

  (precio 1700.0)
  (superficie 92)
  (habitaciones 1)
  (altura-duplex 3)
  (direccion "omtal, 10  Barrio El Gotic   Distrito Ciutat Vella   Bar")
  (latitud 41.386298)
  (longitud 2.1729216)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)


([onto_Class30043] of duplex

  (precio 4500.0)
  (superficie 201)
  (habitaciones 5)
  (altura-duplex 6)
  (direccion "va, 77  Barrio Les Tres Torres   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.402131)
  (longitud 2.1261812)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)


([onto_Class30045] of duplex

  (precio 890.0)
  (superficie 88)
  (habitaciones 4)
  (altura-duplex 4)
  (direccion "ei en Jaume  Barrio Sant Feliu   Distrito Centre   Hospitalet")
  (latitud 41.364557)
  (longitud 2.0916695)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30046] of duplex

  (precio 1250.0)
  (superficie 103)
  (habitaciones 2)
  (altura-duplex 5)
  (direccion " de Barcelona, 111  Sant Joan ")
  (latitud 41.370936)
  (longitud 2.0759644)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30047] of duplex

  (precio 700.0)
  (superficie 120)
  (habitaciones 4)
  (altura-duplex 3)
  (direccion "a Pontons  Torre")
  (latitud 41.388407)
  (longitud 1.5692046)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30048] of duplex

  (precio 1700.0)
  (superficie 185)
  (habitaciones 4)
  (altura-duplex 3)
  (direccion "ito Mirasol   Sant Cugat del ")
  (latitud 41.473696)
  (longitud 2.0418594)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)


([onto_Class30050] of duplex

  (precio 1400.0)
  (superficie 100)
  (habitaciones 3)
  (altura-duplex 1)
  (direccion "ito Cent")
  (latitud 41.448860)
  (longitud 2.2478778)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex FALSE)
)

([onto_Class30051] of duplex

  (precio 7500.0)
  (superficie 150)
  (habitaciones 3)
  (altura-duplex 2)
  (direccion "o Diagonal Mar i el Front Maritim del Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.405607)
  (longitud 2.2143772)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)

([onto_Class30052] of duplex

  (precio 1600.0)
  (superficie 100)
  (habitaciones 2)
  (altura-duplex 2)
  (direccion "ito Gava Mar  ")
  (latitud 41.270346)
  (longitud 2.0278613)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)

([onto_Class30053] of duplex

  (precio 750.0)
  (superficie 110)
  (habitaciones 3)
  (altura-duplex 1)
  (direccion " PRAT DE LA RIBA, 134  Distrito Bigues   Bigues ")
  (latitud 41.677129)
  (longitud 2.2102735)
  (admite-mascotas TRUE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30054] of duplex

  (precio 650.0)
  (superficie 60)
  (habitaciones 1)
  (altura-duplex 1)
  (direccion "e Mitja Galta  Distrito")
  (latitud 41.543022)
  (longitud 2.4486802)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)


([onto_Class30056] of duplex

  (precio 1500.0)
  (superficie 70)
  (habitaciones 1)
  (altura-duplex 2)
  (direccion "eria  Barrio Sant Pere - Santa Caterina i la Ribera   Distrito Ciutat Vella   Bar")
  (latitud 41.386374)
  (longitud 2.1824099)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30057] of duplex

  (precio 1550.0)
  (superficie 250)
  (habitaciones 4)
  (altura-duplex 2)
  (direccion " Urb. Port Llavaneres   Distrito Playa   Sant Andr")
  (latitud 41.560537)
  (longitud 2.5001212)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)

([onto_Class30059] of duplex

  (precio 1600.0)
  (superficie 90)
  (habitaciones 2)
  (altura-duplex 6)
  (direccion "o El Camp d'En Grassot i Gracia Nova   Distrito Gracia   Bar")
  (latitud 41.408090)
  (longitud 2.1653766)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30060] of duplex

  (precio 900.0)
  (superficie 40)
  (habitaciones 2)
  (altura-duplex 8)
  (direccion " de la Republica Argentina  Barrio Vallcarca i els Penitents   Distrito Gracia   Bar")
  (latitud 41.411349)
  (longitud 2.1403502)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30061] of duplex

  (precio 4500.0)
  (superficie 200)
  (habitaciones 5)
  (altura-duplex 6)
  (direccion "el Doctor Roux  Barrio Les Tres Torres   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.401082)
  (longitud 2.1243185)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)


([onto_Class30063] of duplex

  (precio 1685.0)
  (superficie 110)
  (habitaciones 4)
  (altura-duplex 6)
  (direccion " del Santuari de Sant Josep de la Muntanya, 6  Barrio La Salut   Distrito Gracia   Bar")
  (latitud 41.410053)
  (longitud 2.1546894)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)


([onto_Class30065] of duplex

  (precio 1850.0)
  (superficie 150)
  (habitaciones 3)
  (altura-duplex 1)
  (direccion "ito La Pineda   Castelld")
  (latitud 41.267617)
  (longitud 1.9985654)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)


([onto_Class30067] of duplex

  (precio 1000.0)
  (superficie 42)
  (habitaciones 1)
  (altura-duplex 1)
  (direccion "el Brosoli  Barrio Sant Pere - Santa Caterina i la Ribera   Distrito Ciutat Vella   Bar")
  (latitud 41.385992)
  (longitud 2.1815671)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30068] of duplex

  (precio 1025.0)
  (superficie 191)
  (habitaciones 3)
  (altura-duplex 2)
  (direccion "ito Zona Nord - Can 'Oriol - Can Bertran ")
  (latitud 41.499297)
  (longitud 2.0367695)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)


([onto_Class30070] of duplex

  (precio 2000.0)
  (superficie 160)
  (habitaciones 3)
  (altura-duplex 1)
  (direccion "ge Vallseca  Barrio Can Baro   Distrito Horta Guinardo   Bar")
  (latitud 41.413122)
  (longitud 2.1617279)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30071] of duplex

  (precio 1600.0)
  (superficie 115)
  (habitaciones 2)
  (altura-duplex 5)
  (direccion " Diagonal  Barrio Provencals del Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.407201)
  (longitud 2.2071868)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30073] of duplex

  (precio 2450.0)
  (superficie 156)
  (habitaciones 4)
  (altura-duplex 1)
  (direccion "e tortella  Barrio El Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.401814)
  (longitud 2.2073393)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)

([onto_Class30075] of duplex

  (precio 2200.0)
  (superficie 80)
  (habitaciones 4)
  (altura-duplex 1)
  (direccion "ge de Planell  Barrio Les Corts   Distrito Les Corts   Bar")
  (latitud 41.385981)
  (longitud 2.1412155)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30076] of duplex

  (precio 8000.0)
  (superficie 170)
  (habitaciones 4)
  (altura-duplex 2)
  (direccion "el taulat  Urb. Illa del Llac   Barrio Diagonal Mar i el Front Maritim del Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.409641)
  (longitud 2.2160435)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)



([onto_Class30080] of duplex

  (precio 1780.0)
  (superficie 84)
  (habitaciones 2)
  (altura-duplex 2)
  (direccion "e la Duquessa d'Orleans  Barrio Sarria   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.398354)
  (longitud 2.1200671)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30081] of duplex

  (precio 2800.0)
  (superficie 170)
  (habitaciones 3)
  (altura-duplex 1)
  (direccion "'Homer  Barrio El Putxet i el Farro   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.409612)
  (longitud 2.1441004)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30082] of duplex

  (precio 3395.0)
  (superficie 181)
  (habitaciones 4)
  (altura-duplex 3)
  (direccion "e Gomis  Barrio Vallcarca i els Penitents   Distrito Gracia   Bar")
  (latitud 41.412215)
  (longitud 2.1420881)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)

([onto_Class30083] of duplex

  (precio 1150.0)
  (superficie 70)
  (habitaciones 1)
  (altura-duplex 5)
  (direccion " de la Mare de Deu de Montserrat  Barrio Can Baro   Distrito Horta Guinardo   Bar")
  (latitud 41.413305)
  (longitud 2.1619594)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)


([onto_Class30085] of duplex

  (precio 8000.0)
  (superficie 150)
  (habitaciones 4)
  (altura-duplex 2)
  (direccion "el taulat  Barrio Diagonal Mar i el Front Maritim del Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.410236)
  (longitud 2.2161516)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)

([onto_Class30086] of duplex

  (precio 8000.0)
  (superficie 248)
  (habitaciones 4)
  (altura-duplex 2)
  (direccion "o Diagonal Mar i el Front Maritim del Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.409685)
  (longitud 2.2173192)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)

([onto_Class30087] of duplex

  (precio 2000.0)
  (superficie 104)
  (habitaciones 1)
  (altura-duplex 9)
  (direccion "el Torrent de l'Olla  Barrio Vila de Gracia   Distrito Gracia   Bar")
  (latitud 41.405364)
  (longitud 2.1496892)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30088] of duplex

  (precio 575.0)
  (superficie 120)
  (habitaciones 3)
  (altura-duplex 2)
  (direccion "ant Pere  El Pla")
  (latitud 41.416425)
  (longitud 1.7120876)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30089] of duplex

  (precio 1000.0)
  (superficie 120)
  (habitaciones 2)
  (altura-duplex 1)
  (direccion "ntoni, 35  Distrito")
  (latitud 41.537097)
  (longitud 2.4470575)
  (admite-mascotas TRUE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)


([onto_Class30092] of duplex

  (precio 2500.0)
  (superficie 80)
  (habitaciones 2)
  (altura-duplex 1)
  (direccion "y Cajal, 69  Barrio Vila de Gracia   Distrito Gracia   Bar")
  (latitud 41.404143)
  (longitud 2.1610074)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)



([onto_Class30096] of duplex

  (precio 1200.0)
  (superficie 70)
  (habitaciones 2)
  (altura-duplex 1)
  (direccion "e Venero, 11  Barrio El Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.401470)
  (longitud 2.2017289)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)


([onto_Class30098] of duplex

  (precio 895.0)
  (superficie 60)
  (habitaciones 1)
  (altura-duplex 5)
  (direccion "e la Riereta  Barrio El Raval   Distrito Ciutat Vella   Bar")
  (latitud 41.379624)
  (longitud 2.1704514)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)



([onto_Class30101] of duplex

  (precio 1200.0)
  (superficie 151)
  (habitaciones 4)
  (altura-duplex 1)
  (direccion "e sant cristofol  Barrio Centro - Catedral   Distrito Centre   Te")
  (latitud 41.563144)
  (longitud 2.0143791)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)




([onto_Class30105] of duplex

  (precio 1600.0)
  (superficie 130)
  (habitaciones 3)
  (altura-duplex 4)
  (direccion "e Ramon Otero Pedrayo  Distrito Els Canyars   Castelld")
  (latitud 41.291729)
  (longitud 1.9823662)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)


([onto_Class30107] of duplex

  (precio 1600.0)
  (superficie 115)
  (habitaciones 2)
  (altura-duplex 5)
  (direccion " Diagonal  Barrio Diagonal Mar i el Front Maritim del Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.408027)
  (longitud 2.2029515)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30108] of duplex

  (precio 1600.0)
  (superficie 60)
  (habitaciones 2)
  (altura-duplex 2)
  (direccion "el Comte de Santa Clara  Barrio La Barceloneta   Distrito Ciutat Vella   Bar")
  (latitud 41.378878)
  (longitud 2.1907715)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30109] of duplex

  (precio 5000.0)
  (superficie 300)
  (habitaciones 5)
  (altura-duplex 1)
  (direccion "e Rubio i Lluch, 4  Barrio Pedralbes   Distrito Les Corts   Bar")
  (latitud 41.388350)
  (longitud 2.1240469)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)

([onto_Class30111] of duplex

  (precio 950.0)
  (superficie 197)
  (habitaciones 4)
  (altura-duplex 4)
  (direccion "elip ii  Distrito L'Hostal - Lledoner   Gr")
  (latitud 41.614290)
  (longitud 2.2951962)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30112] of duplex

  (precio 1150.0)
  (superficie 112)
  (habitaciones 4)
  (altura-duplex 1)
  (direccion "erge de M")
  (latitud 41.473359)
  (longitud 2.2729713)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)

([onto_Class30113] of duplex

  (precio 1600.0)
  (superficie 110)
  (habitaciones 3)
  (altura-duplex 4)
  (direccion "e Ramon Otero Pedrayo  Distrito Vistalegre   Castelld")
  (latitud 41.290071)
  (longitud 1.9841013)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)

([onto_Class30114] of duplex

  (precio 2500.0)
  (superficie 130)
  (habitaciones 3)
  (altura-duplex 3)
  (direccion "sidor Cartro Robert  Distrito Ametllers-P")
  (latitud 41.237399)
  (longitud 1.8006922)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)

([onto_Class30115] of duplex

  (precio 800.0)
  (superficie 115)
  (habitaciones 2)
  (altura-duplex 2)
  (direccion "metllers s/n  Coll")
  (latitud 41.566454)
  (longitud 1.8280346)
  (admite-mascotas TRUE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30116] of duplex

  (precio 849.0)
  (superficie 110)
  (habitaciones 4)
  (altura-duplex 3)
  (direccion "gara, 9  Barrio Sant Pere   Distrito Nord-Oest   Te")
  (latitud 41.569864)
  (longitud 2.0175012)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30117] of duplex

  (precio 1500.0)
  (superficie 62)
  (habitaciones 2)
  (altura-duplex 3)
  (direccion "el Dos de Maig  Urb. CENTRE   Distrito Centre - Estacio   Sant Cugat del ")
  (latitud 41.468964)
  (longitud 2.0847536)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)


([onto_Class30119] of duplex

  (precio 1600.0)
  (superficie 180)
  (habitaciones 4)
  (altura-duplex 5)
  (direccion "Joan ")
  (latitud 41.364767)
  (longitud 2.0515841)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)


([onto_Class30122] of duplex

  (precio 2400.0)
  (superficie 170)
  (habitaciones 3)
  (altura-duplex 7)
  (direccion "o Sant Gervasi - La Bonanova   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.406260)
  (longitud 2.1353816)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)


([onto_Class30124] of duplex

  (precio 10000.0)
  (superficie 160)
  (habitaciones 3)
  (altura-duplex 2)
  (direccion "e Garcia Faria  Barrio Diagonal Mar i el Front Maritim del Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.403664)
  (longitud 2.2177561)
  (admite-mascotas TRUE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)



([onto_Class30127] of duplex

  (precio 1800.0)
  (superficie 140)
  (habitaciones 3)
  (altura-duplex 2)
  (direccion "txet s/n  Urb. Barcelona   Barrio El Putxet i el Farro   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.405979)
  (longitud 2.1434008)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)



([onto_Class30130] of duplex

  (precio 2500.0)
  (superficie 80)
  (habitaciones 2)
  (altura-duplex 1)
  (direccion "e Ramon y Cajal, 69  Barrio Vila de Gracia   Distrito Gracia   Bar")
  (latitud 41.404143)
  (longitud 2.1610074)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)

([onto_Class30131] of duplex

  (precio 2000.0)
  (superficie 85)
  (habitaciones 3)
  (altura-duplex 1)
  (direccion "e Ballester, 46  Barrio El Putxet i el Farro   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.407704)
  (longitud 2.1469227)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)


([onto_Class30133] of duplex

  (precio 1950.0)
  (superficie 110)
  (habitaciones 3)
  (altura-duplex 9)
  (direccion "oger de Lluria, 110  Barrio La Dreta de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.396269)
  (longitud 2.1648627)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30135] of duplex

  (precio 900.0)
  (superficie 140)
  (habitaciones 4)
  (altura-duplex 2)
  (direccion "octor Klein, 169  ")
  (latitud 41.639522)
  (longitud 2.3630935)
  (admite-mascotas TRUE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30136] of duplex

  (precio 1600.0)
  (superficie 170)
  (habitaciones 2)
  (altura-duplex 1)
  (direccion "e Joan Salvat Papasseit, 34  Urb. Aiguadolc   Distrito San Sebastian-A")
  (latitud 41.235445)
  (longitud 1.8217318)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)

([onto_Class30137] of duplex

  (precio 1000.0)
  (superficie 120)
  (habitaciones 3)
  (altura-duplex 3)
  (direccion "Barrio Ca n'Aurell   Distrito Ponent   Te")
  (latitud 41.558871)
  (longitud 2.0083036)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30138] of duplex

  (precio 700.0)
  (superficie 100)
  (habitaciones 2)
  (altura-duplex 2)
  (direccion "ito")
  (latitud 41.535795)
  (longitud 2.4442994)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30139] of duplex

  (precio 2200.0)
  (superficie 90)
  (habitaciones 1)
  (altura-duplex 8)
  (direccion "o El Parc i la Llacuna del Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.394352)
  (longitud 2.1870608)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)




([onto_Class30144] of duplex

  (precio 1175.0)
  (superficie 91)
  (habitaciones 2)
  (altura-duplex 4)
  (direccion "el Marques de Barbera  Barrio El Raval   Distrito Ciutat Vella   Bar")
  (latitud 41.377678)
  (longitud 2.1745814)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30145] of duplex

  (precio 4500.0)
  (superficie 220)
  (habitaciones 5)
  (altura-duplex 6)
  (direccion "el Doctor Roux, 116  Urb. Zona Bonanova - Dr Roux - Sarria   Barrio Les Tres Torres   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.401882)
  (longitud 2.1263185)
  (admite-mascotas TRUE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30146] of duplex

  (precio 6500.0)
  (superficie 265)
  (habitaciones 6)
  (altura-duplex 5)
  (direccion "el Marques de Mulhacen  Barrio Pedralbes   Distrito Les Corts   Bar")
  (latitud 41.391745)
  (longitud 2.1201797)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)



([onto_Class30149] of duplex

  (precio 7500.0)
  (superficie 180)
  (habitaciones 3)
  (altura-duplex 2)
  (direccion "e Garcia Faria s/n  Barrio Diagonal Mar i el Front Maritim del Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.405664)
  (longitud 2.2166561)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30150] of duplex

  (precio 1695.0)
  (superficie 117)
  (habitaciones 2)
  (altura-duplex 6)
  (direccion "e Ganduxer, 5  Barrio Sant Gervasi - Galvany   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.392287)
  (longitud 2.1388096)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30151] of duplex

  (precio 2350.0)
  (superficie 130)
  (habitaciones 2)
  (altura-duplex 6)
  (direccion "o Sant Gervasi - Galvany   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.398874)
  (longitud 2.1448066)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30152] of duplex

  (precio 1600.0)
  (superficie 160)
  (habitaciones 3)
  (altura-duplex 3)
  (direccion "morera  Urb. OASIS   Distrit")
  (latitud 41.236596)
  (longitud 1.8034418)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)


([onto_Class30155] of duplex

  (precio 1900.0)
  (superficie 118)
  (habitaciones 3)
  (altura-duplex 2)
  (direccion "e domenec oristrell  Distrito Parc Central - Colomer   Sant Cugat del ")
  (latitud 41.473869)
  (longitud 2.0727701)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)

([onto_Class30156] of duplex

  (precio 2800.0)
  (superficie 197)
  (habitaciones 3)
  (altura-duplex 1)
  (direccion "o El Putxet i el Farro   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.406412)
  (longitud 2.1436005)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)



([onto_Class30161] of duplex

  (precio 950.0)
  (superficie 150)
  (habitaciones 4)
  (altura-duplex 4)
  (direccion "rraquer, 7  Sant Sad")
  (latitud 41.426547)
  (longitud 1.7875059)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)


([onto_Class30163] of duplex

  (precio 2100.0)
  (superficie 85)
  (habitaciones 2)
  (altura-duplex 3)
  (direccion "e Gracia  Barrio La Dreta de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.387107)
  (longitud 2.1673814)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30164] of duplex

  (precio 1400.0)
  (superficie 58)
  (habitaciones 2)
  (altura-duplex 2)
  (direccion "icasso  Barrio Sant Pere - Santa Caterina i la Ribera   Distrito Ciutat Vella   Bar")
  (latitud 41.386790)
  (longitud 2.1828867)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)



([onto_Class30167] of duplex

  (precio 8000.0)
  (superficie 165)
  (habitaciones 4)
  (altura-duplex 2)
  (direccion "asseig del Taulat  Barrio Diagonal Mar i el Front Maritim del Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.407424)
  (longitud 2.2125801)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)

([onto_Class30168] of duplex

  (precio 1600.0)
  (superficie 219)
  (habitaciones 3)
  (altura-duplex 6)
  (direccion "AR DE ESPAÑA  Sant Joan ")
  (latitud 41.366167)
  (longitud 2.0526841)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)

([onto_Class30169] of duplex

  (precio 1500.0)
  (superficie 92)
  (habitaciones 1)
  (altura-duplex 3)
  (direccion "omtal  Barrio El Gotic   Distrito Ciutat Vella   Bar")
  (latitud 41.387682)
  (longitud 2.1723885)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)



([onto_Class30172] of duplex

  (precio 7500.0)
  (superficie 150)
  (habitaciones 3)
  (altura-duplex 2)
  (direccion "e garcia faria  Barrio Diagonal Mar i el Front Maritim del Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.406943)
  (longitud 2.2139967)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)

([onto_Class30173] of duplex

  (precio 2000.0)
  (superficie 170)
  (habitaciones 4)
  (altura-duplex 2)
  (direccion "laba, 49  Barrio Torresana - Montserrat   Distrito Llevant   Te")
  (latitud 41.566124)
  (longitud 2.0414809)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30174] of duplex

  (precio 2000.0)
  (superficie 120)
  (habitaciones 1)
  (altura-duplex 1)
  (direccion "vinyo, 34  Barrio El Gotic   Distrito Ciutat Vella   Bar")
  (latitud 41.380748)
  (longitud 2.1776616)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)


([onto_Class30177] of duplex

  (precio 4500.0)
  (superficie 203)
  (habitaciones 5)
  (altura-duplex 6)
  (direccion "e la Bonanova, 77  Barrio Les Tres Torres   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.402354)
  (longitud 2.1260012)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)



([onto_Class30181] of duplex

  (precio 2650.0)
  (superficie 156)
  (habitaciones 4)
  (altura-duplex 1)
  (direccion "ati de Vela, 2  Distrito Gorg - Pep Ventu")
  (latitud 41.440060)
  (longitud 2.2448252)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)

([onto_Class30182] of duplex

  (precio 900.0)
  (superficie 140)
  (habitaciones 4)
  (altura-duplex 2)
  (direccion "octor Klein, 169  ")
  (latitud 41.639522)
  (longitud 2.3630935)
  (admite-mascotas TRUE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30183] of duplex

  (precio 1500.0)
  (superficie 75)
  (habitaciones 1)
  (altura-duplex 3)
  (direccion " Gran Via de les Corts Catalanes  Barrio La Dreta de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.391768)
  (longitud 2.1694863)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30184] of duplex

  (precio 1750.0)
  (superficie 80)
  (habitaciones 2)
  (altura-duplex 3)
  (direccion "e Seneca  Barrio Vila de Gracia   Distrito Gracia   Bar")
  (latitud 41.397601)
  (longitud 2.1568024)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)


([onto_Class30186] of duplex

  (precio 10000.0)
  (superficie 160)
  (habitaciones 3)
  (altura-duplex 2)
  (direccion "e garcia faria  Barrio Diagonal Mar i el Front Maritim del Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.403497)
  (longitud 2.2146642)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)

([onto_Class30187] of duplex

  (precio 470.0)
  (superficie 60)
  (habitaciones 1)
  (altura-duplex 3)
  (direccion "ito Centre - Passeig i ")
  (latitud 41.727160)
  (longitud 1.8241402)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)



([onto_Class30191] of duplex

  (precio 6500.0)
  (superficie 160)
  (habitaciones 3)
  (altura-duplex 2)
  (direccion "o Diagonal Mar i el Front Maritim del Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.407097)
  (longitud 2.2181642)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)

([onto_Class30192] of duplex

  (precio 2900.0)
  (superficie 180)
  (habitaciones 3)
  (altura-duplex 2)
  (direccion "o Sant Gervasi - La Bonanova   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.402756)
  (longitud 2.1337701)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)


([onto_Class30194] of duplex

  (precio 1500.0)
  (superficie 170)
  (habitaciones 4)
  (altura-duplex 3)
  (direccion "")
  (latitud 41.478328)
  (longitud 2.3139193)
  (admite-mascotas TRUE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30197] of duplex

  (precio 13500.0)
  (superficie 600)
  (habitaciones 4)
  (altura-duplex 7)
  (direccion "o Sant Gervasi - Galvany   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.396109)
  (longitud 2.1399409)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30198] of duplex

  (precio 3500.0)
  (superficie 140)
  (habitaciones 4)
  (altura-duplex 4)
  (direccion "Pla de la Torreta  Urb. Marinada   Distrito Playa   Sant Andr")
  (latitud 41.552429)
  (longitud 2.4901778)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)


([onto_Class30200] of duplex

  (precio 600.0)
  (superficie 100)
  (habitaciones 3)
  (altura-duplex 2)
  (direccion "o Peña  Sant V")
  (latitud 41.671846)
  (longitud 1.8602883)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30201] of duplex

  (precio 550.0)
  (superficie 98)
  (habitaciones 2)
  (altura-duplex 4)
  (direccion "l ")
  (latitud 42.040509)
  (longitud 2.2715813)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30202] of duplex

  (precio 600.0)
  (superficie 73)
  (habitaciones 3)
  (altura-duplex 2)
  (direccion "'enric granados, 27  Urb. 78186-0002   Sant V")
  (latitud 41.673803)
  (longitud 1.8622019)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30203] of duplex

  (precio 500.0)
  (superficie 71)
  (habitaciones 2)
  (altura-duplex 3)
  (direccion "oixero  Urb. Duplex en Guardiola de Bergueda   Guardi")
  (latitud 42.236657)
  (longitud 1.8754064)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30204] of duplex

  (precio 1800.0)
  (superficie 120)
  (habitaciones 3)
  (altura-duplex 3)
  (direccion "iutat de Balaguer, 30  Barrio Sant Gervasi - La Bonanova   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.403523)
  (longitud 2.1351667)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)


([onto_Class30207] of duplex

  (precio 1900.0)
  (superficie 80)
  (habitaciones 2)
  (altura-duplex 3)
  (direccion "e Prats de Mollo, 18  Barrio Sant Gervasi - Galvany   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.401301)
  (longitud 2.1381583)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)



([onto_Class30210] of duplex

  (precio 1120.0)
  (superficie 78)
  (habitaciones 3)
  (altura-duplex 3)
  (direccion "ito Gracia   Sa")
  (latitud 41.541392)
  (longitud 2.1011674)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)


([onto_Class30212] of duplex

  (precio 1395.0)
  (superficie 85)
  (habitaciones 3)
  (altura-duplex 2)
  (direccion "avia  Barrio Sants - Badal   Distrito Sants-Montjuïc   Bar")
  (latitud 41.372869)
  (longitud 2.1296804)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)


([onto_Class30215] of duplex

  (precio 1400.0)
  (superficie 75)
  (habitaciones 2)
  (altura-duplex 3)
  (direccion " de les Trompetes  Barrio Sant Pere - Santa Caterina i la Ribera   Distrito Ciutat Vella   Bar")
  (latitud 41.380854)
  (longitud 2.1840573)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30216] of duplex

  (precio 1780.0)
  (superficie 84)
  (habitaciones 2)
  (altura-duplex 2)
  (direccion "e la Duquessa d'Orleans  Barrio Sarria   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.397254)
  (longitud 2.1218671)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30217] of duplex

  (precio 1600.0)
  (superficie 111)
  (habitaciones 2)
  (altura-duplex 5)
  (direccion " Diagonal  Barrio Provencals del Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.406401)
  (longitud 2.2065868)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)


([onto_Class30219] of duplex

  (precio 1300.0)
  (superficie 66)
  (habitaciones 1)
  (altura-duplex 1)
  (direccion "e llull  Barrio Diagonal Mar i el Front Maritim del Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.407276)
  (longitud 2.2101845)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)



([onto_Class30222] of duplex

  (precio 590.0)
  (superficie 90)
  (habitaciones 3)
  (altura-duplex 1)
  (direccion "ito Le")
  (latitud 41.590884)
  (longitud 1.6121161)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30223] of duplex

  (precio 1195.0)
  (superficie 149)
  (habitaciones 3)
  (altura-duplex 3)
  (direccion "o Sant Pere   Distrito Nord-Oest   Te")
  (latitud 41.568993)
  (longitud 2.0102755)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)


([onto_Class30227] of duplex

  (precio 1800.0)
  (superficie 100)
  (habitaciones 3)
  (altura-duplex 3)
  (direccion "iudad de Balaguer  Urb. Barcelona   Barrio Sant Gervasi - La Bonanova   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.403417)
  (longitud 2.1335248)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)








([onto_Class30235] of duplex

  (precio 1900.0)
  (superficie 80)
  (habitaciones 2)
  (altura-duplex 3)
  (direccion "rats de Mollo  Urb. Barcelona   Barrio Sant Gervasi - Galvany   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.403317)
  (longitud 2.1378945)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30236] of duplex

  (precio 2500.0)
  (superficie 80)
  (habitaciones 2)
  (altura-duplex 1)
  (direccion "amon y Cajal  Urb. Barcelona   Barrio Vila de Gracia   Distrito Gracia   Bar")
  (latitud 41.404514)
  (longitud 2.1629349)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)





([onto_Class30241] of duplex

  (precio 2500.0)
  (superficie 171)
  (habitaciones 4)
  (altura-duplex 2)
  (direccion "o Sant Gervasi - La Bonanova   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.417307)
  (longitud 2.1309181)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
  (ascensor-duplex TRUE)
)

([onto_Class30242] of duplex

  (precio 1900.0)
  (superficie 90)
  (habitaciones 3)
  (altura-duplex 2)
  (direccion "icia  Barrio Vallcarca i els Penitents   Distrito Gracia   Bar")
  (latitud 41.415948)
  (longitud 2.1357312)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)




([onto_Class30246] of duplex

  (precio 3000.0)
  (superficie 160)
  (habitaciones 3)
  (altura-duplex 4)
  (direccion "e Casanova, 115  Barrio L'Antiga Esquerra de l'Eixample   Distrito Eixample   Bar")
  (latitud 41.387724)
  (longitud 2.1554994)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30247] of duplex

  (precio 1600.0)
  (superficie 75)
  (habitaciones 2)
  (altura-duplex 3)
  (direccion "el Comte de Santa Clara, 24  Barrio La Barceloneta   Distrito Ciutat Vella   Bar")
  (latitud 41.378644)
  (longitud 2.1890849)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30248] of duplex

  (precio 1000.0)
  (superficie 130)
  (habitaciones 4)
  (altura-duplex 2)
  (direccion " del Torrent Gornal  Barrio Collblanc   Distrito Collblanc   Hospitalet")
  (latitud 41.372508)
  (longitud 2.1161858)
  (admite-mascotas TRUE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30249] of duplex

  (precio 475.0)
  (superficie 70)
  (habitaciones 3)
  (altura-duplex 2)
  (direccion "reus  Sant Feliu de")
  (latitud 41.688828)
  (longitud 2.1647777)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)





([onto_Class30254] of duplex

  (precio 4500.0)
  (superficie 300)
  (habitaciones 5)
  (altura-duplex 6)
  (direccion "va  Barrio Les Tres Torres   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.404354)
  (longitud 2.1242012)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)


([onto_Class30256] of duplex

  (precio 1750.0)
  (superficie 120)
  (habitaciones 3)
  (altura-duplex 2)
  (direccion "el Clos de Sant Francesc, 26  Barrio Sarria   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.398505)
  (longitud 2.1214953)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)



([onto_Class30260] of duplex

  (precio 1700.0)
  (superficie 150)
  (habitaciones 4)
  (altura-duplex 2)
  (direccion " XA")
  (latitud 41.497369)
  (longitud 2.2913691)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30261] of duplex

  (precio 810.0)
  (superficie 91)
  (habitaciones 4)
  (altura-duplex 4)
  (direccion "e jaume i, 9  Urb. Jaime I   Distri")
  (latitud 41.725018)
  (longitud 1.8231048)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)





([onto_Class30266] of duplex

  (precio 2300.0)
  (superficie 80)
  (habitaciones 2)
  (altura-duplex 1)
  (direccion "e ramon y cajal, 69  Urb. Barcelona   Barrio Vila de Gracia   Distrito Gracia   Bar")
  (latitud 41.404143)
  (longitud 2.1610074)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)


([onto_Class30268] of duplex

  (precio 1800.0)
  (superficie 85)
  (habitaciones 3)
  (altura-duplex 1)
  (direccion "e ballester s/n  Barrio El Putxet i el Farro   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.407161)
  (longitud 2.1461466)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)



([onto_Class30271] of duplex

  (precio 2000.0)
  (superficie 85)
  (habitaciones 3)
  (altura-duplex 2)
  (direccion "e ballester s/n  Barrio El Putxet i el Farro   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.407161)
  (longitud 2.1461466)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)

([onto_Class30272] of duplex

  (precio 1900.0)
  (superficie 80)
  (habitaciones 3)
  (altura-duplex 3)
  (direccion "de mollo 18, local s/n  Barrio El Gotic   Distrito Ciutat Vella   Bar")
  (latitud 41.385063)
  (longitud 2.1734035)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)




([onto_Class30276] of duplex

  (precio 2500.0)
  (superficie 120)
  (habitaciones 3)
  (altura-duplex 3)
  (direccion "e la ciutat de balaguer s/n  Barrio Sant Gervasi - La Bonanova   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.403523)
  (longitud 2.1351667)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
  (ascensor-duplex TRUE)
)



([onto_Class80000] of unifamiliar

  (precio 840.0)
  (superficie 57)
  (habitaciones 2)
  (direccion "e la Camelia, 26  Urb. Urbanizacion   Distrito Mas Trader-Corral D’En Tort-Corral D’E")
  (latitud 41.226550)
  (longitud 1.6601166)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80001] of unifamiliar

  (precio 1210.0)
  (superficie 190)
  (habitaciones 4)
  (direccion "ant Iscle, 3  Distrito Montessol-Can Carreras")
  (latitud 41.638249)
  (longitud 2.6582795)
  (admite-mascotas TRUE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80002] of unifamiliar

  (precio 1900.0)
  (superficie 75)
  (habitaciones 3)
  (direccion "ito La Pineda   Castelld")
  (latitud 41.267264)
  (longitud 1.9845477)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80003] of unifamiliar

  (precio 1490.0)
  (superficie 225)
  (habitaciones 4)
  (direccion "oc")
  (latitud 41.491104)
  (longitud 2.2938711)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80005] of unifamiliar

  (precio 1200.0)
  (superficie 160)
  (habitaciones 4)
  (direccion "NA CENTRO  Urb. ZONA CENTRO   El Pa")
  (latitud 41.436717)
  (longitud 2.0100404)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80006] of unifamiliar

  (precio 3850.0)
  (superficie 140)
  (habitaciones 3)
  (direccion "o El Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.398773)
  (longitud 2.2071622)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina TRUE)
)

([onto_Class80007] of unifamiliar

  (precio 800.0)
  (superficie 100)
  (habitaciones 3)
  (direccion "S ROSERS  Urb. ELS ROSERS   Cast")
  (latitud 41.226696)
  (longitud 1.6291973)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80008] of unifamiliar

  (precio 750.0)
  (superficie 260)
  (habitaciones 6)
  (direccion "lcir s/n  Urb. La Rour")
  (latitud 41.770357)
  (longitud 2.1548473)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80009] of unifamiliar

  (precio 2500.0)
  (superficie 279)
  (habitaciones 3)
  (direccion "ranada  Distrito Can Bou   Castelld")
  (latitud 41.271230)
  (longitud 1.9793178)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80010] of unifamiliar

  (precio 1400.0)
  (superficie 320)
  (habitaciones 5)
  (direccion "")
  (latitud 41.594237)
  (longitud 2.5733902)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80011] of unifamiliar

  (precio 1200.0)
  (superficie 45)
  (habitaciones 2)
  (direccion "e Pujades  Barrio El Poblenou   Distrito Sant Marti   Bar")
  (latitud 41.401733)
  (longitud 2.1986557)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80012] of unifamiliar

  (precio 2900.0)
  (superficie 380)
  (habitaciones 5)
  (direccion "SSEDA  Barrio Vallvidrera - El Tibidabo i les Planes   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.415642)
  (longitud 2.1096688)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80013] of unifamiliar

  (precio 2000.0)
  (superficie 181)
  (habitaciones 4)
  (direccion "a de Solanell  Barrio El Coll   Distrito Gracia   Bar")
  (latitud 41.415122)
  (longitud 2.1470372)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80014] of unifamiliar

  (precio 3800.0)
  (superficie 476)
  (habitaciones 5)
  (direccion "ito Valldoreix   Sant Cugat del ")
  (latitud 41.456465)
  (longitud 2.0463382)
  (admite-mascotas TRUE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80015] of unifamiliar

  (precio 2300.0)
  (superficie 200)
  (habitaciones 3)
  (direccion "ito Parc Central - Colomer   Sant Cugat del ")
  (latitud 41.470773)
  (longitud 2.0725581)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80016] of unifamiliar

  (precio 2557.0)
  (superficie 285)
  (habitaciones 4)
  (direccion "an Calella  Sant V")
  (latitud 41.569348)
  (longitud 2.5143506)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80017] of unifamiliar

  (precio 1400.0)
  (superficie 105)
  (habitaciones 4)
  (direccion "e Mari  Barrio La Font d'En Fargues   Distrito Horta Guinardo   Bar")
  (latitud 41.422717)
  (longitud 2.1596109)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80018] of unifamiliar

  (precio 850.0)
  (superficie 145)
  (habitaciones 4)
  (direccion " TEMPLE  Urb. EL TEMPLE   Santa Maria de Pala")
  (latitud 41.681901)
  (longitud 2.4757566)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80019] of unifamiliar

  (precio 1600.0)
  (superficie 379)
  (habitaciones 4)
  (direccion "e l'esglesia  Sant ")
  (latitud 41.921256)
  (longitud 2.3254205)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80020] of unifamiliar

  (precio 2400.0)
  (superficie 140)
  (habitaciones 4)
  (direccion "uminetes  Urb. Lluminetes   Distrito Lluminetes   Castelld")
  (latitud 41.264272)
  (longitud 1.9722396)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80021] of unifamiliar

  (precio 2400.0)
  (superficie 187)
  (habitaciones 3)
  (direccion "el Perill  Barrio Vila de Gracia   Distrito Gracia   Bar")
  (latitud 41.400569)
  (longitud 2.1615493)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80023] of unifamiliar

  (precio 890.0)
  (superficie 50)
  (habitaciones 1)
  (direccion " Barrio La Torrasa   Distrito La Torrasa   Hospitalet")
  (latitud 41.370647)
  (longitud 2.1207396)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80026] of unifamiliar

  (precio 1120.0)
  (superficie 128)
  (habitaciones 4)
  (direccion "e jacint verdaguer  Ca")
  (latitud 41.625193)
  (longitud 2.2769929)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80027] of unifamiliar

  (precio 1750.0)
  (superficie 225)
  (habitaciones 4)
  (direccion "oc")
  (latitud 41.489504)
  (longitud 2.2916711)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80028] of unifamiliar

  (precio 2400.0)
  (superficie 430)
  (habitaciones 5)
  (direccion "")
  (latitud 41.536125)
  (longitud 2.2971272)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80029] of unifamiliar

  (precio 1400.0)
  (superficie 102)
  (habitaciones 3)
  (direccion "e Felip II  Barrio Navas   Distrito Sant Andreu   Bar")
  (latitud 41.422064)
  (longitud 2.1859087)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80030] of unifamiliar

  (precio 950.0)
  (superficie 170)
  (habitaciones 4)
  (direccion "ito Estadi-Horta Vermell")
  (latitud 41.921582)
  (longitud 2.2589583)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80032] of unifamiliar

  (precio 700.0)
  (superficie 150)
  (habitaciones 5)
  (direccion "ito Les Roquetes   S")
  (latitud 41.230340)
  (longitud 1.7489365)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80033] of unifamiliar

  (precio 2400.0)
  (superficie 300)
  (habitaciones 5)
  (direccion "cres s/n  Urb. La L")
  (latitud 41.532922)
  (longitud 2.3706341)
  (admite-mascotas TRUE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80034] of unifamiliar

  (precio 1290.0)
  (superficie 300)
  (habitaciones 7)
  (direccion "osep Lluis Sert i Roca  Urb. Les Ginest")
  (latitud 41.568871)
  (longitud 2.3791316)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80036] of unifamiliar

  (precio 1450.0)
  (superficie 300)
  (habitaciones 4)
  (direccion "el General Boadella  Urb. CENTRO   Barrio Poble - Casc Antic   Distrito Centre   Castellar del ")
  (latitud 41.619242)
  (longitud 2.0887612)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80037] of unifamiliar

  (precio 1900.0)
  (superficie 298)
  (habitaciones 4)
  (direccion "'Artemis  Distrito Canyet - Bonavis")
  (latitud 41.463638)
  (longitud 2.2327474)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80038] of unifamiliar

  (precio 1400.0)
  (superficie 105)
  (habitaciones 4)
  (direccion "el Marques de Foronda  Barrio La Font d'En Fargues   Distrito Horta Guinardo   Bar")
  (latitud 41.423996)
  (longitud 2.1607425)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80040] of unifamiliar

  (precio 1800.0)
  (superficie 100)
  (habitaciones 4)
  (direccion "ito Lluminetes   Castelld")
  (latitud 41.267787)
  (longitud 1.9780643)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80041] of unifamiliar

  (precio 800.0)
  (superficie 75)
  (habitaciones 2)
  (direccion "e Lol")
  (latitud 41.481448)
  (longitud 2.2682766)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80042] of unifamiliar

  (precio 3800.0)
  (superficie 269)
  (habitaciones 4)
  (direccion "aires, 56  Distrito Gava Mar  ")
  (latitud 41.268169)
  (longitud 2.0248112)
  (admite-mascotas TRUE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80043] of unifamiliar

  (precio 890.0)
  (superficie 52)
  (habitaciones 1)
  (direccion "el Progres  Barrio La Torrasa   Distrito La Torrasa   Hospitalet")
  (latitud 41.370722)
  (longitud 2.1212154)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80044] of unifamiliar

  (precio 2000.0)
  (superficie 584)
  (habitaciones 4)
  (direccion "ito Levantina-Montgavina-")
  (latitud 41.249441)
  (longitud 1.8239443)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80045] of unifamiliar

  (precio 950.0)
  (superficie 250)
  (habitaciones 4)
  (direccion "s Mila  Urb. Mas Mila   Distrito Ma")
  (latitud 41.293723)
  (longitud 1.7779718)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80046] of unifamiliar

  (precio 1300.0)
  (superficie 150)
  (habitaciones 3)
  (direccion "e Sant")
  (latitud 41.494657)
  (longitud 2.2940893)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80047] of unifamiliar

  (precio 700.0)
  (superficie 180)
  (habitaciones 5)
  (direccion "e")
  (latitud 41.511338)
  (longitud 1.7327367)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80048] of unifamiliar

  (precio 2500.0)
  (superficie 421)
  (habitaciones 5)
  (direccion "NTRO  Urb. CENTRO ")
  (latitud 41.508148)
  (longitud 2.3506203)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80049] of unifamiliar

  (precio 1210.0)
  (superficie 190)
  (habitaciones 4)
  (direccion "ito Montessol-Can Carreras")
  (latitud 41.639649)
  (longitud 2.6588795)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina TRUE)
)

([onto_Class80050] of unifamiliar

  (precio 1475.0)
  (superficie 380)
  (habitaciones 4)
  (direccion " de Sant Andreu  Distrito El Moli - Canafort - El Punto   Sant Andr")
  (latitud 41.569563)
  (longitud 2.4822141)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)


([onto_Class80052] of unifamiliar

  (precio 1400.0)
  (superficie 450)
  (habitaciones 5)
  (direccion "ulnes, 25  Urb. alta maresma")
  (latitud 41.639818)
  (longitud 2.7196849)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80053] of unifamiliar

  (precio 485.0)
  (superficie 120)
  (habitaciones 4)
  (direccion "")
  (latitud 41.813736)
  (longitud 2.0990284)
  (admite-mascotas TRUE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80054] of unifamiliar

  (precio 2250.0)
  (superficie 150)
  (habitaciones 4)
  (direccion "e la gleva  Barrio El Putxet i el Farro   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.402590)
  (longitud 2.1450667)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80055] of unifamiliar

  (precio 1800.0)
  (superficie 180)
  (habitaciones 5)
  (direccion "ito El Poal   Castelld")
  (latitud 41.268763)
  (longitud 1.9458711)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)


([onto_Class80057] of unifamiliar

  (precio 6000.0)
  (superficie 578)
  (habitaciones 6)
  (direccion "ito Golf   Sant Cugat del ")
  (latitud 41.459573)
  (longitud 2.0776622)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80058] of unifamiliar

  (precio 4950.0)
  (superficie 500)
  (habitaciones 4)
  (direccion "e Can Sagrera  Sant Just De")
  (latitud 41.385526)
  (longitud 2.0694258)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80059] of unifamiliar

  (precio 1500.0)
  (superficie 299)
  (habitaciones 4)
  (direccion "")
  (latitud 41.385448)
  (longitud 1.9284384)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80061] of unifamiliar

  (precio 1500.0)
  (superficie 35)
  (habitaciones 1)
  (direccion " del Llac, 8  Distrito La Floresta - Les Planes   Sant Cugat del ")
  (latitud 41.438529)
  (longitud 2.1042579)
  (admite-mascotas TRUE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80063] of unifamiliar

  (precio 2400.0)
  (superficie 140)
  (habitaciones 4)
  (direccion " Font  Barrio Sant Gervasi - La Bonanova   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.419807)
  (longitud 2.1309181)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80064] of unifamiliar

  (precio 3850.0)
  (superficie 380)
  (habitaciones 6)
  (direccion "")
  (latitud 41.511847)
  (longitud 2.3606691)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80065] of unifamiliar

  (precio 2150.0)
  (superficie 180)
  (habitaciones 4)
  (direccion "ito Baixador   Castelld")
  (latitud 41.263241)
  (longitud 1.9421594)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80067] of unifamiliar

  (precio 3800.0)
  (superficie 325)
  (habitaciones 4)
  (direccion "ito Gava Mar  ")
  (latitud 41.270502)
  (longitud 2.0263755)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80068] of unifamiliar

  (precio 3000.0)
  (superficie 300)
  (habitaciones 5)
  (direccion "el Ginebre  Distrito Mas R")
  (latitud 41.478101)
  (longitud 2.2505042)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80069] of unifamiliar

  (precio 2800.0)
  (superficie 264)
  (habitaciones 4)
  (direccion "arena  Distrito Valldoreix   Sant Cugat del ")
  (latitud 41.456219)
  (longitud 2.0366629)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80070] of unifamiliar

  (precio 2500.0)
  (superficie 260)
  (habitaciones 4)
  (direccion "oscoll  Urb. QUINT MAR   Distrito Levantina-Montgavina-")
  (latitud 41.251726)
  (longitud 1.8270731)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina TRUE)
)


([onto_Class80072] of unifamiliar

  (precio 1400.0)
  (superficie 107)
  (habitaciones 3)
  (direccion "el Marques de Foronda  Barrio La Font d'En Fargues   Distrito Horta Guinardo   Bar")
  (latitud 41.424307)
  (longitud 2.1605872)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)


([onto_Class80074] of unifamiliar

  (precio 2000.0)
  (superficie 244)
  (habitaciones 4)
  (direccion "arca, 11  Sant Joan ")
  (latitud 41.363856)
  (longitud 2.0571701)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80075] of unifamiliar

  (precio 2500.0)
  (superficie 650)
  (habitaciones 6)
  (direccion "ito El Poal   Castelld")
  (latitud 41.275416)
  (longitud 1.9469547)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80076] of unifamiliar

  (precio 2300.0)
  (superficie 180)
  (habitaciones 5)
  (direccion "de can gatxet  Distrito Arxius   Sant Cugat del ")
  (latitud 41.467475)
  (longitud 2.0762818)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80077] of unifamiliar

  (precio 1500.0)
  (superficie 247)
  (habitaciones 5)
  (direccion "")
  (latitud 41.495088)
  (longitud 2.3172898)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80078] of unifamiliar

  (precio 2400.0)
  (superficie 200)
  (habitaciones 4)
  (direccion "e Granollers s/n  Urb. Parc Central   Distrito Parc Central - Colomer   Sant Cugat del ")
  (latitud 41.476399)
  (longitud 2.0725126)
  (admite-mascotas TRUE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80079] of unifamiliar

  (precio 4000.0)
  (superficie 280)
  (habitaciones 3)
  (direccion "ra de les aigües  Barrio Vallcarca i els Penitents   Distrito Gracia   Bar")
  (latitud 41.422236)
  (longitud 2.1323047)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80080] of unifamiliar

  (precio 1500.0)
  (superficie 220)
  (habitaciones 4)
  (direccion "oan Coromines ")
  (latitud 41.605111)
  (longitud 2.6218343)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80081] of unifamiliar

  (precio 2400.0)
  (superficie 171)
  (habitaciones 4)
  (direccion "de Sant Cugat  Urb. Tibidabo a Vallvidriera   Barrio Vallvidrera - El Tibidabo i les Planes   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.422684)
  (longitud 2.1189333)
  (admite-mascotas TRUE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)


([onto_Class80084] of unifamiliar

  (precio 1400.0)
  (superficie 200)
  (habitaciones 4)
  (direccion "an forns  L'Ametlla de")
  (latitud 41.669465)
  (longitud 2.2472535)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80085] of unifamiliar

  (precio 1400.0)
  (superficie 210)
  (habitaciones 3)
  (direccion "e l'Havana, 20  Distrito Centre Vila - La Geltru   Vil")
  (latitud 41.221508)
  (longitud 1.7243819)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80086] of unifamiliar

  (precio 2557.0)
  (superficie 182)
  (habitaciones 4)
  (direccion "an Calella  Sant V")
  (latitud 41.566948)
  (longitud 2.5126506)
  (admite-mascotas TRUE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)


([onto_Class80088] of unifamiliar

  (precio 750.0)
  (superficie 112)
  (habitaciones 3)
  (direccion "octor Fleming  La ")
  (latitud 41.535866)
  (longitud 1.6625732)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80089] of unifamiliar

  (precio 900.0)
  (superficie 120)
  (habitaciones 2)
  (direccion ".garcia lorca  Distrito Centre - Colomeres - Ramblas  ")
  (latitud 41.311096)
  (longitud 1.9958517)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80090] of unifamiliar

  (precio 1400.0)
  (superficie 105)
  (habitaciones 3)
  (direccion "el Marques de Foronda  Barrio La Font d'En Fargues   Distrito Horta Guinardo   Bar")
  (latitud 41.426735)
  (longitud 2.1619267)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)



([onto_Class80093] of unifamiliar

  (precio 2800.0)
  (superficie 290)
  (habitaciones 5)
  (direccion "ito Valldoreix   Sant Cugat del ")
  (latitud 41.459119)
  (longitud 2.0342629)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80094] of unifamiliar

  (precio 1280.0)
  (superficie 227)
  (habitaciones 4)
  (direccion "")
  (latitud 41.491264)
  (longitud 2.3651862)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80095] of unifamiliar

  (precio 2800.0)
  (superficie 325)
  (habitaciones 4)
  (direccion "nmany  Urb. Monmany   Distrito Valldoreix   Sant Cugat del ")
  (latitud 41.459030)
  (longitud 2.0335228)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80096] of unifamiliar

  (precio 1500.0)
  (superficie 150)
  (habitaciones 3)
  (direccion "A SANTIAGO RUSIÑOL  Urb. VALLPINEDA   Distrito Vallpineda-Santa")
  (latitud 41.248311)
  (longitud 1.7977302)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina TRUE)
)

([onto_Class80098] of unifamiliar

  (precio 3200.0)
  (superficie 256)
  (habitaciones 3)
  (direccion "el Francoli  Barrio El Putxet i el Farro   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.405414)
  (longitud 2.1495681)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)


([onto_Class80100] of unifamiliar

  (precio 5000.0)
  (superficie 500)
  (habitaciones 6)
  (direccion " sofia  Urb. VINYET   Distrito Vinyet-Terramar-Can Pei-Ca")
  (latitud 41.232986)
  (longitud 1.8019495)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80101] of unifamiliar

  (precio 5000.0)
  (superficie 600)
  (habitaciones 8)
  (direccion "ito Centre - Estacio   Sant Cugat del ")
  (latitud 41.475585)
  (longitud 2.0846123)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80102] of unifamiliar

  (precio 1400.0)
  (superficie 200)
  (habitaciones 5)
  (direccion "Vell del Maltemps  Distrito Urbanitzacions")
  (latitud 41.585191)
  (longitud 2.5582253)
  (admite-mascotas TRUE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80103] of unifamiliar

  (precio 2350.0)
  (superficie 150)
  (habitaciones 2)
  (direccion "  Barrio Vila de Gracia   Distrito Gracia   Bar")
  (latitud 41.400296)
  (longitud 2.1636477)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80104] of unifamiliar

  (precio 1600.0)
  (superficie 165)
  (habitaciones 3)
  (direccion " de la Creu  Urb. centro sant Just zona Milenari   Sant Just De")
  (latitud 41.384111)
  (longitud 2.0777788)
  (admite-mascotas TRUE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80105] of unifamiliar

  (precio 2500.0)
  (superficie 150)
  (habitaciones 2)
  (direccion "ntoni Caballe s/n  Distrito Golf   Sant Cugat del ")
  (latitud 41.456277)
  (longitud 2.0696039)
  (admite-mascotas TRUE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina TRUE)
)

([onto_Class80106] of unifamiliar

  (precio 3500.0)
  (superficie 403)
  (habitaciones 4)
  (direccion " de Guix Borrull, 8  Distrito La Floresta - Les Planes   Sant Cugat del ")
  (latitud 41.439689)
  (longitud 2.0783299)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80107] of unifamiliar

  (precio 2900.0)
  (superficie 380)
  (habitaciones 5)
  (direccion "e Can Basseda  Barrio Vallvidrera - El Tibidabo i les Planes   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.414942)
  (longitud 2.1081688)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80108] of unifamiliar

  (precio 1350.0)
  (superficie 217)
  (habitaciones 4)
  (direccion "els Batlle")
  (latitud 41.490964)
  (longitud 2.3653862)
  (admite-mascotas TRUE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80109] of unifamiliar

  (precio 1200.0)
  (superficie 250)
  (habitaciones 4)
  (direccion "e washington, 66  Distrito Riera Alta - Llati   Santa Colom")
  (latitud 41.457531)
  (longitud 2.2129417)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80110] of unifamiliar

  (precio 2600.0)
  (superficie 363)
  (habitaciones 5)
  (direccion "")
  (latitud 41.512016)
  (longitud 2.3571544)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80111] of unifamiliar

  (precio 3300.0)
  (superficie 355)
  (habitaciones 5)
  (direccion "os de maig  Sant Just De")
  (latitud 41.384200)
  (longitud 2.0812762)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80112] of unifamiliar

  (precio 1300.0)
  (superficie 145)
  (habitaciones 3)
  (direccion "ito La Creu Alta   Sa")
  (latitud 41.554596)
  (longitud 2.1080523)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80113] of unifamiliar

  (precio 7795.0)
  (superficie 390)
  (habitaciones 4)
  (direccion "e Gaziel  Barrio Sant Gervasi - La Bonanova   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.412449)
  (longitud 2.1222589)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80114] of unifamiliar

  (precio 700.0)
  (superficie 100)
  (habitaciones 3)
  (direccion "")
  (latitud 41.525964)
  (longitud 1.6890862)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80115] of unifamiliar

  (precio 2400.0)
  (superficie 143)
  (habitaciones 4)
  (direccion "'August Font  Barrio Sant Gervasi - La Bonanova   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.416507)
  (longitud 2.1299181)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80116] of unifamiliar

  (precio 2950.0)
  (superficie 523)
  (habitaciones 5)
  (direccion "atagalls  Urb. Mas de Enserra   Distrito Mas d'En Serra-Els Cards   S")
  (latitud 41.232117)
  (longitud 1.7593871)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80118] of unifamiliar

  (precio 2850.0)
  (superficie 223)
  (habitaciones 5)
  (direccion "Fost de Camps")
  (latitud 41.522532)
  (longitud 2.2265758)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina TRUE)
)

([onto_Class80120] of unifamiliar

  (precio 6600.0)
  (superficie 397)
  (habitaciones 7)
  (direccion "ito El Poal   Castelld")
  (latitud 41.267345)
  (longitud 1.9481763)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80121] of unifamiliar

  (precio 1200.0)
  (superficie 200)
  (habitaciones 4)
  (direccion "ito L'Aragai - Prat de Vilanova   Vil")
  (latitud 41.202710)
  (longitud 1.6837712)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80122] of unifamiliar

  (precio 890.0)
  (superficie 125)
  (habitaciones 4)
  (direccion "EAL  Urb. Poligono 7 Parcela 9010 ESTACIO DE GUALBA. GUALBA (BARCELONA) ")
  (latitud 41.744222)
  (longitud 2.4923153)
  (admite-mascotas TRUE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)


([onto_Class80124] of unifamiliar

  (precio 2900.0)
  (superficie 350)
  (habitaciones 4)
  (direccion "live")
  (latitud 41.534064)
  (longitud 2.3733001)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80125] of unifamiliar

  (precio 1800.0)
  (superficie 240)
  (habitaciones 4)
  (direccion " de Cal Ma")
  (latitud 41.524688)
  (longitud 2.3701007)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80126] of unifamiliar

  (precio 2850.0)
  (superficie 223)
  (habitaciones 5)
  (direccion " emili monturiol  Sant Fost de Camps")
  (latitud 41.524232)
  (longitud 2.2275758)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina TRUE)
)

([onto_Class80127] of unifamiliar

  (precio 3300.0)
  (superficie 350)
  (habitaciones 4)
  (direccion "D'ARAGO  Urb. levantina   Distrito Levantina-Montgavina-")
  (latitud 41.239430)
  (longitud 1.8246899)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80129] of unifamiliar

  (precio 3600.0)
  (superficie 350)
  (habitaciones 5)
  (direccion "e bernat desclot  Distrito Arxius   Sant Cugat del ")
  (latitud 41.468674)
  (longitud 2.0679261)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina TRUE)
)


([onto_Class80131] of unifamiliar

  (precio 7700.0)
  (superficie 350)
  (habitaciones 6)
  (direccion "o Pedralbes   Distrito Les Corts   Bar")
  (latitud 41.392542)
  (longitud 2.1153933)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80132] of unifamiliar

  (precio 3500.0)
  (superficie 483)
  (habitaciones 8)
  (direccion "s, 44  Urb. VALLVIDRERA   Barrio Vallvidrera - El Tibidabo i les Planes   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.414760)
  (longitud 2.1084237)
  (admite-mascotas TRUE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80133] of unifamiliar

  (precio 2300.0)
  (superficie 352)
  (habitaciones 4)
  (direccion " pere planas  Distrito La Floresta - Les Planes   Sant Cugat del ")
  (latitud 41.442159)
  (longitud 2.0794255)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina TRUE)
)

([onto_Class80134] of unifamiliar

  (precio 1300.0)
  (superficie 75)
  (habitaciones 3)
  (direccion "e Marti Molins  Barrio La Sagrera   Distrito Sant Andreu   Bar")
  (latitud 41.425094)
  (longitud 2.1904203)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80135] of unifamiliar

  (precio 1300.0)
  (superficie 232)
  (habitaciones 3)
  (direccion "Sant Miquel de Gua")
  (latitud 41.566257)
  (longitud 1.9775726)
  (admite-mascotas TRUE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80136] of unifamiliar

  (precio 2400.0)
  (superficie 185)
  (habitaciones 3)
  (direccion "el Perill  Barrio Vila de Gracia   Distrito Gracia   Bar")
  (latitud 41.398569)
  (longitud 2.1612493)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80137] of unifamiliar

  (precio 2400.0)
  (superficie 500)
  (habitaciones 5)
  (direccion "argarida Abril G")
  (latitud 41.551360)
  (longitud 2.3933018)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80138] of unifamiliar

  (precio 2960.0)
  (superficie 185)
  (habitaciones 6)
  (direccion "Velez s/n  Barrio El Guinardo   Distrito Horta Guinardo   Bar")
  (latitud 41.415726)
  (longitud 2.1771761)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80139] of unifamiliar

  (precio 2400.0)
  (superficie 155)
  (habitaciones 3)
  (direccion "ra de Vallvidrera al Tibidabo  Urb. TIBIDABO   Barrio Vallvidrera - El Tibidabo i les Planes   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.421082)
  (longitud 2.1176704)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80140] of unifamiliar

  (precio 2000.0)
  (superficie 250)
  (habitaciones 5)
  (direccion " SANTA ISABEL  Urb. VALLPINEDA   Distrito Vallpineda-Rocamar   S")
  (latitud 41.246664)
  (longitud 1.7983311)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80141] of unifamiliar

  (precio 2500.0)
  (superficie 210)
  (habitaciones 3)
  (direccion "el Torrent de les Roses  Barrio Pedralbes   Distrito Les Corts   Bar")
  (latitud 41.387595)
  (longitud 2.1035775)
  (admite-mascotas TRUE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80142] of unifamiliar

  (precio 2600.0)
  (superficie 185)
  (habitaciones 3)
  (direccion "el Perill  Barrio Vila de Gracia   Distrito Gracia   Bar")
  (latitud 41.399069)
  (longitud 2.1638493)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80143] of unifamiliar

  (precio 4500.0)
  (superficie 288)
  (habitaciones 5)
  (direccion " Urb. TERRAMAR   Distrito Vinyet-Terramar-Can Pei-Ca")
  (latitud 41.231471)
  (longitud 1.7919773)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80144] of unifamiliar

  (precio 9000.0)
  (superficie 780)
  (habitaciones 7)
  (direccion "ondat  Urb. VALLPINEDA   Distrito Vallpineda-Santa")
  (latitud 41.244856)
  (longitud 1.8001297)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80145] of unifamiliar

  (precio 10000.0)
  (superficie 395)
  (habitaciones 6)
  (direccion " del Cami de Can Girona  Urb. CAN GIRONA   Distrito Vinyet-Terramar-Can Pei-Ca")
  (latitud 41.225743)
  (longitud 1.7686579)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80146] of unifamiliar

  (precio 6000.0)
  (superficie 313)
  (habitaciones 5)
  (direccion " de la Trinitat  Urb. Levantina   Distrito Levantina-Montgavina-")
  (latitud 41.245027)
  (longitud 1.8267325)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80149] of unifamiliar

  (precio 1800.0)
  (superficie 519)
  (habitaciones 6)
  (direccion " Sisena, 9  Urb. Can Güell   Torrelles de Llob")
  (latitud 41.359815)
  (longitud 1.9537586)
  (admite-mascotas TRUE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80150] of unifamiliar

  (precio 10000.0)
  (superficie 925)
  (habitaciones 7)
  (direccion "osep Planas i Robert  Urb. TERRAMAR   Distrito Vinyet-Terramar-Can Pei-Ca")
  (latitud 41.231968)
  (longitud 1.7889917)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80153] of unifamiliar

  (precio 2700.0)
  (superficie 204)
  (habitaciones 2)
  (direccion "ito Lluminetes   Castelld")
  (latitud 41.267888)
  (longitud 1.9754358)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina TRUE)
)

([onto_Class80154] of unifamiliar

  (precio 1450.0)
  (superficie 204)
  (habitaciones 5)
  (direccion "")
  (latitud 41.599396)
  (longitud 2.5152882)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80155] of unifamiliar

  (precio 6000.0)
  (superficie 575)
  (habitaciones 6)
  (direccion "ito Bellaterra   Cerdanyola del ")
  (latitud 41.507951)
  (longitud 2.0900399)
  (admite-mascotas TRUE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80156] of unifamiliar

  (precio 5000.0)
  (superficie 480)
  (habitaciones 7)
  (direccion " Sofia  Distrito Vinyet-Terramar-Can Pei-Ca")
  (latitud 41.236660)
  (longitud 1.8038491)
  (admite-mascotas TRUE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)


([onto_Class80158] of unifamiliar

  (precio 10000.0)
  (superficie 460)
  (habitaciones 6)
  (direccion "Torrente de les Roses  Barrio Pedralbes   Distrito Les Corts   Bar")
  (latitud 41.390184)
  (longitud 2.1027863)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)


([onto_Class80160] of unifamiliar

  (precio 3300.0)
  (superficie 241)
  (habitaciones 4)
  (direccion "e Sant Francesc Xavier  Distrito Eixample   Sant Cugat del ")
  (latitud 41.462270)
  (longitud 2.0832557)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80161] of unifamiliar

  (precio 2400.0)
  (superficie 171)
  (habitaciones 3)
  (direccion "Cami de Sant Cugat  Urb. Tibidabo a Vallvidriera   Barrio Vallvidrera - El Tibidabo i les Planes   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.424707)
  (longitud 2.1160508)
  (admite-mascotas TRUE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80162] of unifamiliar

  (precio 1300.0)
  (superficie 125)
  (habitaciones 4)
  (direccion "el Roure  Distrito Mas R")
  (latitud 41.478640)
  (longitud 2.2547387)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80163] of unifamiliar

  (precio 10000.0)
  (superficie 300)
  (habitaciones 4)
  (direccion "e Gaziel  Barrio Sarria   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.405969)
  (longitud 2.1201501)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina TRUE)
)

([onto_Class80164] of unifamiliar

  (precio 2600.0)
  (superficie 437)
  (habitaciones 7)
  (direccion "enorca  Distrito Mirasol   Sant Cugat del ")
  (latitud 41.467076)
  (longitud 2.0467088)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina TRUE)
)

([onto_Class80165] of unifamiliar

  (precio 7700.0)
  (superficie 400)
  (habitaciones 6)
  (direccion "els til·lers  Barrio Pedralbes   Distrito Les Corts   Bar")
  (latitud 41.392717)
  (longitud 2.1187407)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80166] of unifamiliar

  (precio 2200.0)
  (superficie 231)
  (habitaciones 5)
  (direccion "")
  (latitud 41.476238)
  (longitud 2.2690619)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina FALSE)
)


([onto_Class80168] of unifamiliar

  (precio 1495.0)
  (superficie 225)
  (habitaciones 3)
  (direccion " ")
  (latitud 41.489884)
  (longitud 2.2935512)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80170] of unifamiliar

  (precio 8000.0)
  (superficie 415)
  (habitaciones 5)
  (direccion "ant Pere Claver, 24  Barrio Sarria   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.407445)
  (longitud 2.1167343)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80171] of unifamiliar

  (precio 12000.0)
  (superficie 430)
  (habitaciones 8)
  (direccion "e les monges  Distrito Centro   Sant Andr")
  (latitud 41.576154)
  (longitud 2.4851181)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina TRUE)
)

([onto_Class80172] of unifamiliar

  (precio 1300.0)
  (superficie 100)
  (habitaciones 3)
  (direccion "ito La Creu Alta   Sa")
  (latitud 41.556396)
  (longitud 2.1080523)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80173] of unifamiliar

  (precio 1145.0)
  (superficie 143)
  (habitaciones 4)
  (direccion "erra ")
  (latitud 41.444608)
  (longitud 1.8648848)
  (admite-mascotas TRUE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80174] of unifamiliar

  (precio 4950.0)
  (superficie 310)
  (habitaciones 4)
  (direccion "e saldes  Barrio Sant Genis Dels Agudells - Montbau   Distrito Horta Guinardo   Bar")
  (latitud 41.429708)
  (longitud 2.1341989)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina FALSE)
)


([onto_Class80176] of unifamiliar

  (precio 4000.0)
  (superficie 330)
  (habitaciones 3)
  (direccion "ntmany  Urb. Montmany   Distrito Valldoreix   Sant Cugat del ")
  (latitud 41.454815)
  (longitud 2.0383566)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80177] of unifamiliar

  (precio 1700.0)
  (superficie 100)
  (habitaciones 3)
  (direccion "ito Mirasol   Sant Cugat del ")
  (latitud 41.463764)
  (longitud 2.0441509)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80178] of unifamiliar

  (precio 1300.0)
  (superficie 100)
  (habitaciones 3)
  (direccion "NEDA  Urb. Vallpineda   Distrito Vallpineda-Rocamar   S")
  (latitud 41.247575)
  (longitud 1.7952871)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80182] of unifamiliar

  (precio 2200.0)
  (superficie 500)
  (habitaciones 6)
  (direccion "s Zorreras  Urb. Las Zorreras   Distrito Urbaniz")
  (latitud 41.563642)
  (longitud 2.4329055)
  (admite-mascotas TRUE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80184] of unifamiliar

  (precio 3800.0)
  (superficie 303)
  (habitaciones 6)
  (direccion "el valles  Distrito Sant Francesc-El Coll   Sant Cugat del ")
  (latitud 41.474594)
  (longitud 2.0891748)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80185] of unifamiliar

  (precio 2400.0)
  (superficie 240)
  (habitaciones 5)
  (direccion "ANT ALBERT  Urb. VALLPINEDA   Distrito Vallpineda-Santa")
  (latitud 41.248305)
  (longitud 1.7981647)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80186] of unifamiliar

  (precio 2950.0)
  (superficie 523)
  (habitaciones 5)
  (direccion "els encantats  Distrito Mas d'En Serra-Els Cards   S")
  (latitud 41.230978)
  (longitud 1.7584685)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80187] of unifamiliar

  (precio 3500.0)
  (superficie 384)
  (habitaciones 4)
  (direccion "el maduixer  Barrio Vallcarca i els Penitents   Distrito Gracia   Bar")
  (latitud 41.420552)
  (longitud 2.1318532)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80188] of unifamiliar

  (precio 2500.0)
  (superficie 210)
  (habitaciones 4)
  (direccion "el Torrent de les Roses  Barrio Pedralbes   Distrito Les Corts   Bar")
  (latitud 41.390043)
  (longitud 2.1035822)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80189] of unifamiliar

  (precio 2950.0)
  (superficie 400)
  (habitaciones 5)
  (direccion "N SERRA  Urb. MAS DEN SERRA   Distrito Les Roquetes   S")
  (latitud 41.231709)
  (longitud 1.7474839)
  (admite-mascotas TRUE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80191] of unifamiliar

  (precio 1500.0)
  (superficie 275)
  (habitaciones 5)
  (direccion "e jovellanos  Distrito Centre   Sa")
  (latitud 41.550151)
  (longitud 2.1039452)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80192] of unifamiliar

  (precio 2300.0)
  (superficie 380)
  (habitaciones 4)
  (direccion "  Urb. MONTGAVINA   Distrito Levantina-Montgavina-")
  (latitud 41.239006)
  (longitud 1.8383101)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80193] of unifamiliar

  (precio 2557.0)
  (superficie 182)
  (habitaciones 4)
  (direccion "an Calella  Sant V")
  (latitud 41.566963)
  (longitud 2.5145756)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80194] of unifamiliar

  (precio 1800.0)
  (superficie 297)
  (habitaciones 7)
  (direccion "ngel guimera  Marto")
  (latitud 41.479124)
  (longitud 1.9195002)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina FALSE)
)


([onto_Class80198] of unifamiliar

  (precio 3200.0)
  (superficie 317)
  (habitaciones 4)
  (direccion "o El Putxet i el Farro   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.406314)
  (longitud 2.1495681)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80199] of unifamiliar

  (precio 10000.0)
  (superficie 450)
  (habitaciones 6)
  (direccion " CAN GIRONA  Urb. CAN GIRONA   Distrito Vinyet-Terramar-Can Pei-Ca")
  (latitud 41.228920)
  (longitud 1.7719629)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80200] of unifamiliar

  (precio 7700.0)
  (superficie 390)
  (habitaciones 5)
  (direccion "els til lers  Barrio Pedralbes   Distrito Les Corts   Bar")
  (latitud 41.389717)
  (longitud 2.1189407)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80202] of unifamiliar

  (precio 5500.0)
  (superficie 720)
  (habitaciones 5)
  (direccion "e mexic  Distrito Golf   Sant Cugat del ")
  (latitud 41.456912)
  (longitud 2.0768331)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80203] of unifamiliar

  (precio 3800.0)
  (superficie 800)
  (habitaciones 5)
  (direccion "alvador  Urb. AVINYONET   Distrito Centre Vila   Vilafranca")
  (latitud 41.344926)
  (longitud 1.6989398)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80204] of unifamiliar

  (precio 2300.0)
  (superficie 350)
  (habitaciones 3)
  (direccion "les  Urb. cany")
  (latitud 41.282833)
  (longitud 1.7250553)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80205] of unifamiliar

  (precio 500.0)
  (superficie 70)
  (habitaciones 3)
  (direccion "a Tossa  Urb. Les Pinedes de L´Armengol   La ")
  (latitud 41.514357)
  (longitud 1.6293183)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)



([onto_Class80208] of unifamiliar

  (precio 2500.0)
  (superficie 150)
  (habitaciones 3)
  (direccion "e la davallada  Distrit")
  (latitud 41.237326)
  (longitud 1.8112183)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
)




([onto_Class80213] of unifamiliar

  (precio 1150.0)
  (superficie 169)
  (habitaciones 4)
  (direccion "uer Beltran  Marto")
  (latitud 41.483108)
  (longitud 1.9152092)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80214] of unifamiliar

  (precio 1200.0)
  (superficie 180)
  (habitaciones 3)
  (direccion "arques de M")
  (latitud 41.470919)
  (longitud 2.2875823)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80215] of unifamiliar

  (precio 2500.0)
  (superficie 144)
  (habitaciones 3)
  (direccion "o Sant Gervasi - La Bonanova   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.416907)
  (longitud 2.1308181)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80216] of unifamiliar

  (precio 2800.0)
  (superficie 283)
  (habitaciones 4)
  (direccion "ito Valldoreix   Sant Cugat del ")
  (latitud 41.457719)
  (longitud 2.0332629)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80218] of unifamiliar

  (precio 3000.0)
  (superficie 400)
  (habitaciones 4)
  (direccion "RRAMAR  Urb. TERRAMAR   Distrito Vinyet-Terramar-Can Pei-Ca")
  (latitud 41.231665)
  (longitud 1.7895274)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)


([onto_Class80220] of unifamiliar

  (precio 1300.0)
  (superficie 113)
  (habitaciones 4)
  (direccion "AGAI  Urb. ARAGAI   Distrito L'Aragai - Prat de Vilanova   Vil")
  (latitud 41.219006)
  (longitud 1.7039368)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80222] of unifamiliar

  (precio 4500.0)
  (superficie 350)
  (habitaciones 4)
  (direccion " 340  Distrito Montmar   Castelld")
  (latitud 41.274542)
  (longitud 1.9724162)
  (admite-mascotas TRUE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80225] of unifamiliar

  (precio 1400.0)
  (superficie 80)
  (habitaciones 3)
  (direccion " Barrio Sant Gervasi - Galvany   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.400155)
  (longitud 2.1486665)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80226] of unifamiliar

  (precio 2200.0)
  (superficie 120)
  (habitaciones 4)
  (direccion "e Planella  Barrio Sant Gervasi - La Bonanova   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.406132)
  (longitud 2.1243682)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80227] of unifamiliar

  (precio 2850.0)
  (superficie 228)
  (habitaciones 5)
  (direccion "Pagesia  Distrito Zona Nord - Can 'Oriol - Can Bertran ")
  (latitud 41.509263)
  (longitud 2.0384643)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80228] of unifamiliar

  (precio 12000.0)
  (superficie 350)
  (habitaciones 6)
  (direccion "Immaculada  Barrio Sant Gervasi - La Bonanova   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.407561)
  (longitud 2.1231768)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80229] of unifamiliar

  (precio 3500.0)
  (superficie 200)
  (habitaciones 5)
  (direccion "ge sant jordi")
  (latitud 41.498355)
  (longitud 2.3344814)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80231] of unifamiliar

  (precio 2480.0)
  (superficie 100)
  (habitaciones 2)
  (direccion "e Casal Sant Jordi  Esplugues de Llob")
  (latitud 41.384986)
  (longitud 2.0994895)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina TRUE)
)

([onto_Class80232] of unifamiliar

  (precio 1530.0)
  (superficie 262)
  (habitaciones 5)
  (direccion "Cerv")
  (latitud 41.408297)
  (longitud 1.9312677)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80233] of unifamiliar

  (precio 2250.0)
  (superficie 140)
  (habitaciones 4)
  (direccion " Barrio El Putxet i el Farro   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.404990)
  (longitud 2.1462667)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80234] of unifamiliar

  (precio 1600.0)
  (superficie 280)
  (habitaciones 4)
  (direccion "'Afrodita s/n  Distrito Canyet - Bonavis")
  (latitud 41.465123)
  (longitud 2.2336847)
  (admite-mascotas TRUE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80235] of unifamiliar

  (precio 1000.0)
  (superficie 190)
  (habitaciones 4)
  (direccion "e la bellavista  Castell")
  (latitud 41.471622)
  (longitud 1.9803091)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80236] of unifamiliar

  (precio 1800.0)
  (superficie 100)
  (habitaciones 3)
  (direccion "el Bosc  Distrito Montmar   Castelld")
  (latitud 41.278106)
  (longitud 1.9592534)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80237] of unifamiliar

  (precio 4500.0)
  (superficie 384)
  (habitaciones 4)
  (direccion "ito Montmar   Castelld")
  (latitud 41.273342)
  (longitud 1.9705162)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80238] of unifamiliar

  (precio 1400.0)
  (superficie 290)
  (habitaciones 4)
  (direccion "e Tomas Vives  Sant Andreu de la B")
  (latitud 41.446070)
  (longitud 1.9679429)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80239] of unifamiliar

  (precio 1300.0)
  (superficie 320)
  (habitaciones 5)
  (direccion "e Baldomer Sola  Distrito Gorg - Pep Ventu")
  (latitud 41.446005)
  (longitud 2.2393828)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80240] of unifamiliar

  (precio 1295.0)
  (superficie 207)
  (habitaciones 3)
  (direccion "ataro  Ab")
  (latitud 41.522441)
  (longitud 1.9081881)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80241] of unifamiliar

  (precio 2500.0)
  (superficie 250)
  (habitaciones 4)
  (direccion " la guineu, 40  Distrito Levantina-Montgavina-")
  (latitud 41.241438)
  (longitud 1.8270089)
  (admite-mascotas TRUE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80242] of unifamiliar

  (precio 3500.0)
  (superficie 490)
  (habitaciones 4)
  (direccion "ito Valldoreix   Sant Cugat del ")
  (latitud 41.455959)
  (longitud 2.0327264)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina TRUE)
)


([onto_Class80244] of unifamiliar

  (precio 1500.0)
  (superficie 160)
  (habitaciones 3)
  (direccion "ONOLL  Urb. QUINT MAR   Distrito Levantina-Montgavina-")
  (latitud 41.249025)
  (longitud 1.8241306)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)


([onto_Class80246] of unifamiliar

  (precio 2350.0)
  (superficie 155)
  (habitaciones 2)
  (direccion "erill  Barrio Vila de Gracia   Distrito Gracia   Bar")
  (latitud 41.402356)
  (longitud 2.1645086)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80247] of unifamiliar

  (precio 1200.0)
  (superficie 127)
  (habitaciones 4)
  (direccion "  Urb. CAN FATJo   Distrito Can Alzamora - Les Torres - Can Fatjo ")
  (latitud 41.484883)
  (longitud 2.0235436)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80248] of unifamiliar

  (precio 1700.0)
  (superficie 120)
  (habitaciones 3)
  (direccion "ito Mirasol   Sant Cugat del ")
  (latitud 41.467264)
  (longitud 2.0437509)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80249] of unifamiliar

  (precio 1900.0)
  (superficie 200)
  (habitaciones 4)
  (direccion " de Santiago Rusinyol  Urb. Vallpineda   Distrito Vallpineda-Santa")
  (latitud 41.246028)
  (longitud 1.7969955)
  (admite-mascotas TRUE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80250] of unifamiliar

  (precio 930.0)
  (superficie 130)
  (habitaciones 4)
  (direccion "ossen Jacint Verdaguer, 69  Urb. La L")
  (latitud 41.533840)
  (longitud 2.3681096)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza FALSE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80251] of unifamiliar

  (precio 2200.0)
  (superficie 200)
  (habitaciones 3)
  (direccion "el roca  Sant V")
  (latitud 41.585897)
  (longitud 2.5099361)
  (admite-mascotas FALSE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80253] of unifamiliar

  (precio 500.0)
  (superficie 112)
  (habitaciones 4)
  (direccion "e San")
  (latitud 41.812755)
  (longitud 2.0914283)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80254] of unifamiliar

  (precio 800.0)
  (superficie 120)
  (habitaciones 2)
  (direccion "HUGUET  Barrio El Besos   Distrito Sant Marti   Bar")
  (latitud 41.417180)
  (longitud 2.2164857)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80255] of unifamiliar

  (precio 1550.0)
  (superficie 190)
  (habitaciones 3)
  (direccion "")
  (latitud 41.488404)
  (longitud 2.2947711)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80256] of unifamiliar

  (precio 2399.0)
  (superficie 185)
  (habitaciones 3)
  (direccion "erill  Barrio Vila de Gracia   Distrito Gracia   Bar")
  (latitud 41.398396)
  (longitud 2.1608477)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80257] of unifamiliar

  (precio 3300.0)
  (superficie 355)
  (habitaciones 5)
  (direccion "Just De")
  (latitud 41.386799)
  (longitud 2.0818258)
  (admite-mascotas TRUE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80258] of unifamiliar

  (precio 2750.0)
  (superficie 185)
  (habitaciones 6)
  (direccion "o El Guinardo   Distrito Horta Guinardo   Bar")
  (latitud 41.417572)
  (longitud 2.1783377)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80260] of unifamiliar

  (precio 2000.0)
  (superficie 180)
  (habitaciones 4)
  (direccion "a de Solanell, 9  Barrio El Coll   Distrito Gracia   Bar")
  (latitud 41.416998)
  (longitud 2.1478167)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80261] of unifamiliar

  (precio 2500.0)
  (superficie 280)
  (habitaciones 4)
  (direccion "s Luhi  Urb. Mas Luhi   Sant Feliu de Llob")
  (latitud 41.384652)
  (longitud 2.0570148)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80262] of unifamiliar

  (precio 2600.0)
  (superficie 363)
  (habitaciones 5)
  (direccion "")
  (latitud 41.511616)
  (longitud 2.3569544)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)

([onto_Class80263] of unifamiliar

  (precio 2990.0)
  (superficie 325)
  (habitaciones 4)
  (direccion "ito Parc Central - Colomer   Sant Cugat del ")
  (latitud 41.470394)
  (longitud 2.0762584)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80264] of unifamiliar

  (precio 1500.0)
  (superficie 223)
  (habitaciones 4)
  (direccion "s Colinas  Urb. Las Colinas   Distrito Ma")
  (latitud 41.307153)
  (longitud 1.7672299)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)


([onto_Class80266] of unifamiliar

  (precio 7000.0)
  (superficie 450)
  (habitaciones 6)
  (direccion "")
  (latitud 41.505773)
  (longitud 2.2841621)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80267] of unifamiliar

  (precio 5000.0)
  (superficie 580)
  (habitaciones 7)
  (direccion "nyet  Urb. Vinyet   Distrito Vinyet-Terramar-Can Pei-Ca")
  (latitud 41.235360)
  (longitud 1.8014491)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)


([onto_Class80269] of unifamiliar

  (precio 3000.0)
  (superficie 500)
  (habitaciones 4)
  (direccion "d'Agell s/n")
  (latitud 41.530517)
  (longitud 2.3998229)
  (admite-mascotas FALSE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina TRUE)
)

([onto_Class80271] of unifamiliar

  (precio 2500.0)
  (superficie 360)
  (habitaciones 4)
  (direccion "egre  Urb. MAS MESTRE   Distrito Mas ")
  (latitud 41.286376)
  (longitud 1.7522018)
  (admite-mascotas TRUE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina TRUE)
)

([onto_Class80273] of unifamiliar

  (precio 750.0)
  (superficie 74)
  (habitaciones 2)
  (direccion "aus  La Roca de")
  (latitud 41.603475)
  (longitud 2.3425659)
  (admite-mascotas TRUE)
  (amueblada FALSE)
  (terraza TRUE)
  (garaje TRUE)
  (piscina FALSE)
)


([onto_Class80276] of unifamiliar

  (precio 2500.0)
  (superficie 222)
  (habitaciones 6)
  (direccion "")
  (latitud 41.503208)
  (longitud 2.2794738)
  (admite-mascotas TRUE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina TRUE)
)

([onto_Class80277] of unifamiliar

  (precio 1000.0)
  (superficie 110)
  (habitaciones 4)
  (direccion "E LA PINEDA s/n  Distrito Riells del Fai   Bigues ")
  (latitud 41.696581)
  (longitud 2.1945519)
  (admite-mascotas FALSE)
  (amueblada TRUE)
  (terraza TRUE)
  (garaje FALSE)
  (piscina FALSE)
)

([onto_Class80278] of unifamiliar

  (precio 3500.0)
  (superficie 480)
  (habitaciones 8)
  (direccion "e les alberes, 44  Barrio Vallvidrera - El Tibidabo i les Planes   Distrito Sarria-Sant Gervasi   Bar")
  (latitud 41.414662)
  (longitud 2.1085659)
  (admite-mascotas FALSE)
  (terraza FALSE)
  (garaje FALSE)
  (piscina TRUE)
)


([onto_Class20001] of  parada_metro

    (nombre "linea 1 Can Serra")
    (x 41.367707)
    (y 2.103443))

([onto_Class20002] of  parada_metro

    (nombre "linea 1 Catalunya")
    (x 41.387081)
    (y 2.170109))

([onto_Class20003] of  parada_metro

    (nombre "linea 1 Clot")
    (x 41.410336)
    (y 2.186803))

([onto_Class20004] of  parada_metro

    (nombre "linea 1 Espanya")
    (x 41.374518)
    (y 2.148468))

([onto_Class20005] of  parada_metro

    (nombre "linea 1 Fabra i Puig")
    (x 41.429634)
    (y 2.183661))

([onto_Class20006] of  parada_metro

    (nombre "linea 1 Florida")
    (x 41.368316)
    (y 2.110001))

([onto_Class20007] of  parada_metro

    (nombre "linea 1 Fondo")
    (x 41.451444)
    (y 2.218435))

([onto_Class20008] of  parada_metro

    (nombre "linea 1 Glories")
    (x 41.405484)
    (y 2.191486))

([onto_Class20009] of  parada_metro

    (nombre "linea 1 Hospital de Bellvitge")
    (x 41.344677)
    (y 2.107242))

([onto_Class200010] of  parada_metro

    (nombre "linea 1 Hostafrancs")
    (x 41.375246)
    (y 2.144756))

([onto_Class200011] of  parada_metro

    (nombre "linea 1 Marina")
    (x 41.394315)
    (y 2.187561))

([onto_Class200012] of  parada_metro

    (nombre "linea 1 Mercat Nou")
    (x 41.372551)
    (y 2.133274))

([onto_Class200013] of  parada_metro

    (nombre "linea 1 Navas")
    (x 41.416187)
    (y 2.187057))

([onto_Class200014] of  parada_metro

    (nombre "linea 1 Placa de Sants")
    (x 41.375729)
    (y 2.135159))

([onto_Class200015] of  parada_metro

    (nombre "linea 1 Rambla Just Oliveras")
    (x 41.363823)
    (y 2.099992))

([onto_Class200016] of  parada_metro

    (nombre "linea 1 Rocafort")
    (x 41.379232)
    (y 2.154562))

([onto_Class200017] of  parada_metro

    (nombre "linea 1 Sagrera")
    (x 41.421458)
    (y 2.186937))

([onto_Class200018] of  parada_metro

    (nombre "linea 1 Sant Andreu")
    (x 41.436610)
    (y 2.191188))

([onto_Class200019] of  parada_metro

    (nombre "linea 1 Santa Coloma")
    (x 41.451067)
    (y 2.207969))

([onto_Class200020] of  parada_metro

    (nombre "linea 1 Santa Eulalia")
    (x 41.368816)
    (y 2.128617))

([onto_Class200021] of  parada_metro

    (nombre "linea 1 Torras i Bages")
    (x 41.442749)
    (y 2.190888))

([onto_Class200022] of  parada_metro

    (nombre "linea 1 Torrassa")
    (x 41.368329)
    (y 2.116643))

([onto_Class200023] of  parada_metro

    (nombre "linea 1 Trinitat Vella")
    (x 41.448765)
    (y 2.193964))

([onto_Class200024] of  parada_metro

    (nombre "linea 1 Universitat")
    (x 41.387065)
    (y 2.164855))

([onto_Class200025] of  parada_metro

    (nombre "linea 1 Urgell")
    (x 41.382487)
    (y 2.158891))

([onto_Class200026] of  parada_metro

    (nombre "linea 1 Urquinaona")
    (x 41.388787)
    (y 2.172799))

([onto_Class200027] of  parada_metro

    (nombre "linea 2 Arigues | Sant Adria")
    (x 41.433414)
    (y 2.217224))

([onto_Class200028] of  parada_metro

    (nombre "linea 2 Bac de Roda")
    (x 41.414944)
    (y 2.195216))

([onto_Class200029] of  parada_metro

    (nombre "linea 2 Badalona Pompeu Fabra")
    (x 41.449194)
    (y 2.243605))

([onto_Class200030] of  parada_metro

    (nombre "linea 2 Clot")
    (x 41.410336)
    (y 2.186803))

([onto_Class200031] of  parada_metro

    (nombre "linea 2 Encants")
    (x 41.406844)
    (y 2.182546))

([onto_Class200032] of  parada_metro

    (nombre "linea 2 Gorg")
    (x 41.440142)
    (y 2.233793))

([onto_Class200033] of  parada_metro

    (nombre "linea 2 La Pau")
    (x 41.423289)
    (y 2.205965))

([onto_Class200034] of  parada_metro

    (nombre "linea 2 Monumental")
    (x 41.398684)
    (y 2.180486))

([onto_Class200035] of  parada_metro

    (nombre "linea 2 Paral-lel")
    (x 41.374827)
    (y 2.170025))

([onto_Class200036] of  parada_metro

    (nombre "linea 2 Passeig de Gracia")
    (x 41.393170)
    (y 2.163960))

([onto_Class200037] of  parada_metro

    (nombre "linea 2 Pep Ventura")
    (x 41.443905)
    (y 2.237959))

([onto_Class200038] of  parada_metro

    (nombre "linea 2 Sagrada Familia")
    (x 41.404307)
    (y 2.174905))

([onto_Class200039] of  parada_metro

    (nombre "linea 2 Sant Antoni")
    (x 41.379833)
    (y 2.163241))

([onto_Class200040] of  parada_metro

    (nombre "linea 2 Sant Marti")
    (x 41.419064)
    (y 2.200724))

([onto_Class200041] of  parada_metro

    (nombre "linea 2 Sant Roc")
    (x 41.435842)
    (y 2.228616))

([onto_Class200042] of  parada_metro

    (nombre "linea 2 Tetuan")
    (x 41.393776)
    (y 2.173943))

([onto_Class200043] of  parada_metro

    (nombre "linea 2 Universitat")
    (x 41.387065)
    (y 2.164855))

([onto_Class200044] of  parada_metro

    (nombre "linea 2 Vernada")
    (x 41.391917)
    (y 2.138618))

([onto_Class200045] of  parada_metro

    (nombre "linea 3 Canyelles")
    (x 41.441753)
    (y 2.166388))

([onto_Class200046] of  parada_metro

    (nombre "linea 3 Catalunya")
    (x 41.387081)
    (y 2.170109))

([onto_Class200047] of  parada_metro

    (nombre "linea 3 Diagonal")
    (x 41.392814)
    (y 2.158031))

([onto_Class200048] of  parada_metro

    (nombre "linea 3 Drassenes")
    (x 41.376538)
    (y 2.175664))

([onto_Class200049] of  parada_metro

    (nombre "linea 3 Espanya")
    (x 41.374518)
    (y 2.148468))

([onto_Class200050] of  parada_metro

    (nombre "linea 3 Fontana")
    (x 41.402626)
    (y 2.152730))

([onto_Class200051] of  parada_metro

    (nombre "linea 3 Les Corts")
    (x 41.384113)
    (y 2.130701))

([onto_Class200052] of  parada_metro

    (nombre "linea 3 Lesseps")
    (x 41.405738)
    (y 2.150407))

([onto_Class200053] of  parada_metro

    (nombre "linea 3 Liceu")
    (x 41.380939)
    (y 2.173388))

([onto_Class200054] of  parada_metro

    (nombre "linea 3 Maria Cristina")
    (x 41.387816)
    (y 2.125791))

([onto_Class200055] of  parada_metro

    (nombre "linea 3 Montbau")
    (x 41.430273)
    (y 2.145218))

([onto_Class200056] of  parada_metro

    (nombre "linea 3 Mundet")
    (x 41.434820)
    (y 2.148365))

([onto_Class200057] of  parada_metro

    (nombre "linea 3 Palau Reial")
    (x 41.385919)
    (y 2.118461))

([onto_Class200058] of  parada_metro

    (nombre "linea 3 Paral.lel")
    (x 41.374827)
    (y 2.170025))

([onto_Class200059] of  parada_metro

    (nombre "linea 3 Passeig de Gracia")
    (x 41.389731)
    (y 2.168852))

([onto_Class200060] of  parada_metro

    (nombre "linea 3 Penitents")
    (x 41.417448)
    (y 2.140595))

([onto_Class200061] of  parada_metro

    (nombre "linea 3 Placa de Sants")
    (x 41.375729)
    (y 2.135159))

([onto_Class200062] of  parada_metro

    (nombre "linea 3 Poble Sec")
    (x 41.374996)
    (y 2.160245))

([onto_Class200063] of  parada_metro

    (nombre "linea 3 Roquetes")
    (x 41.448124)
    (y 2.177171))

([onto_Class200064] of  parada_metro

    (nombre "linea 3 Sants Estacio")
    (x 41.379203)
    (y 2.140309))

([onto_Class200065] of  parada_metro

    (nombre "linea 3 Tarragona")
    (x 41.378223)
    (y 2.146040))

([onto_Class200066] of  parada_metro

    (nombre "linea 3 Trinitat Nova")
    (x 41.449018)
    (y 2.182160))

([onto_Class200067] of  parada_metro

    (nombre "linea 3 Vall d´Hebron")
    (x 41.425326)
    (y 2.142137))

([onto_Class200068] of  parada_metro

    (nombre "linea 3 Vallcarca")
    (x 41.411975)
    (y 2.144337))

([onto_Class200069] of  parada_metro

    (nombre "linea 3 Valldaura")
    (x 41.437996)
    (y 2.156878))

([onto_Class200070] of  parada_metro

    (nombre "linea 3 Zona Universitaria")
    (x 41.384329)
    (y 2.111612))

([onto_Class200071] of  parada_metro

    (nombre "linea 4 Alfons X")
    (x 41.412376)
    (y 2.166382))

([onto_Class200072] of  parada_metro

    (nombre "linea 4 Barceloneta")
    (x 41.382161)
    (y 2.185299))

([onto_Class200073] of  parada_metro

    (nombre "linea 4 Besos")
    (x 41.419918)
    (y 2.210213))

([onto_Class200074] of  parada_metro

    (nombre "linea 4 Besos Mar")
    (x 41.415099)
    (y 2.216082))

([onto_Class200075] of  parada_metro

    (nombre "linea 4 Bogatell")
    (x 41.394891)
    (y 2.191571))

([onto_Class200076] of  parada_metro

    (nombre "linea 4 Ciudadella | Villa Olimpica")
    (x 41.387824)
    (y 2.193370))

([onto_Class200077] of  parada_metro

    (nombre "linea 4 El Maresme | Forum")
    (x 41.412186)
    (y 2.217561))

([onto_Class200078] of  parada_metro

    (nombre "linea 4 Girona")
    (x 41.394920)
    (y 2.170825))

([onto_Class200079] of  parada_metro

    (nombre "linea 4 Guinardo | Hospital de Sant Pau")
    (x 41.415962)
    (y 2.173989))

([onto_Class200080] of  parada_metro

    (nombre "linea 4 Jaume I")
    (x 41.383849)
    (y 2.178418))

([onto_Class200081] of  parada_metro

    (nombre "linea 4 Joanic")
    (x 41.405975)
    (y 2.162861))

([onto_Class200082] of  parada_metro

    (nombre "linea 4 La Pau")
    (x 41.423289)
    (y 2.205965))

([onto_Class200083] of  parada_metro

    (nombre "linea 4 Llacuna")
    (x 41.399425)
    (y 2.197712))

([onto_Class200084] of  parada_metro

    (nombre "linea 4 Llucmajor")
    (x 41.436990)
    (y 2.173341))

([onto_Class200085] of  parada_metro

    (nombre "linea 4 Maragall")
    (x 41.424656)
    (y 2.176766))

([onto_Class200086] of  parada_metro

    (nombre "linea 4 Passeig de Gracia")
    (x 41.389731)
    (y 2.168852))

([onto_Class200087] of  parada_metro

    (nombre "linea 4 Poblenou")
    (x 41.403716)
    (y 2.203371))

([onto_Class200088] of  parada_metro

    (nombre "linea 4 Selva de Mar")
    (x 41.408001)
    (y 2.209122))

([onto_Class200089] of  parada_metro

    (nombre "linea 4 Trinitat Nova")
    (x 41.449018)
    (y 2.182160))

([onto_Class200090] of  parada_metro

    (nombre "linea 4 Urquinaona")
    (x 41.388787)
    (y 2.172799))

([onto_Class200091] of  parada_metro

    (nombre "linea 4 Verdaguer")
    (x 41.399944)
    (y 2.168672))

([onto_Class200092] of  parada_metro

    (nombre "linea 4 Via Julia")
    (x 41.443729)
    (y 2.178550))

([onto_Class200093] of  parada_metro

    (nombre "linea 5 Badal")
    (x 41.375580)
    (y 2.127424))

([onto_Class200094] of  parada_metro

    (nombre "linea 5 Camp de l´Arpa")
    (x 41.415318)
    (y 2.181744))

([onto_Class200095] of  parada_metro

    (nombre "linea 5 Can Boixeres")
    (x 41.366571)
    (y 2.091506))

([onto_Class200096] of  parada_metro

    (nombre "linea 5 Can Videlet")
    (x 41.371281)
    (y 2.099445))

([onto_Class200097] of  parada_metro

    (nombre "linea 5 Collblanc")
    (x 41.375854)
    (y 2.118150))

([onto_Class200098] of  parada_metro

    (nombre "linea 5 Congres")
    (x 41.423439)
    (y 2.181186))

([onto_Class200099] of  parada_metro

    (nombre "linea 5 Cornella")
    (x 41.351694)
    (y 2.090564))

([onto_Class2000100] of  parada_metro

    (nombre "linea 5 Centre Diagonal")
    (x 41.392814)
    (y 2.158031))

([onto_Class2000101] of  parada_metro

    (nombre "linea 5 El Carmel")
    (x 41.423570)
    (y 2.154880))

([onto_Class2000102] of  parada_metro

    (nombre "linea 5 Entenca")
    (x 41.384467)
    (y 2.145619))

([onto_Class2000103] of  parada_metro

    (nombre "linea 5 Gavarra")
    (x 41.357925)
    (y 2.079058))

([onto_Class2000104] of  parada_metro

    (nombre "linea 5 Horta")
    (x 41.429701)
    (y 2.158816))

([onto_Class2000105] of  parada_metro

    (nombre "linea 5 Hospital Clinic")
    (x 41.388669)
    (y 2.151018))

([onto_Class2000106] of  parada_metro

    (nombre "linea 5 La Sagrera")
    (x 41.421458)
    (y 2.186937))

([onto_Class2000107] of  parada_metro

    (nombre "linea 5 La Teixonera")
    (x 41.420934)
    (y 2.150274))

([onto_Class2000108] of  parada_metro

    (nombre "linea 5 Maragall")
    (x 41.424334)
    (y 2.177221))

([onto_Class2000109] of  parada_metro

    (nombre "linea 5 Placa de Sants")
    (x 41.375729)
    (y 2.135159))

([onto_Class2000110] of  parada_metro

    (nombre "linea 5 Pubila Cases")
    (x 41.373838)
    (y 2.107209))

([onto_Class2000111] of  parada_metro

    (nombre "linea 5 Sagrada Familia")
    (x 41.404307)
    (y 2.174905))

([onto_Class2000112] of  parada_metro

    (nombre "linea 5 Sant Ildefons Sant Pau | Dos de Maig")
    (x 41.410751)
    (y 2.176016))

([onto_Class2000113] of  parada_metro

    (nombre "linea 5 Sats Estacio")
    (x 41.379203)
    (y 2.140309))

([onto_Class2000114] of  parada_metro

    (nombre "linea 5 Vall d´Hebron")
    (x 41.425326)
    (y 2.142137))

([onto_Class2000115] of  parada_metro

    (nombre "linea 5 Verdaguer")
    (x 41.399944)
    (y 2.168672))

([onto_Class2000116] of  parada_metro

    (nombre "linea 5 Vilapicina")
    (x 41.430465)
    (y 2.167619))

([onto_Class2000117] of  parada_metro

    (nombre "linea 5 Virrei Amat")
    (x 41.430235)
    (y 2.174749))




([onto_Class2000118] of  hipermercado

    (nombre "ALCAMPO C.C Diagonal Mar    Barcelona ")
    (x 41.409356)
    (y 2.216574))

([onto_Class2000119] of  hipermercado

    (nombre "CONSUM BASIC    C/ Valencia, 478    Barcelona")
    (x 41.403746)
    (y 2.178247))

([onto_Class2000120] of  hipermercado

    (nombre "NA   C/ Sant Pere d’Abanto, 12-14    Barcelona")
    (x 41.371740)
    (y 2.143251))

([onto_Class2000121] of  hipermercado

    (nombre "OCONSUM BASIC    C/ Buenos Aires, 30 Barcelona")
    (x 41.391920)
    (y 2.146750))

([onto_Class2000122] of  hipermercado

    (nombre "ERCADCONSUM BASIC    C/ Numancia, 77 Barcelona")
    (x 41.384626)
    (y 2.137388))

([onto_Class2000123] of  hipermercado

    (nombre "MCONSUM  Av Madrid, 46-48    Barcelona")
    (x 41.377240)
    (y 2.125616))

([onto_Class2000124] of  hipermercado

    (nombre "MERCADONA   C.C Las Arenas  Barcelona")
    (x 41.376027)
    (y 2.149688))

([onto_Class2000125] of  hipermercado

    (nombre "MERCADONA   C/ Sants, 204   Barcelona")
    (x 41.375600)
    (y 2.131461))

([onto_Class2000126] of  hipermercado

    (nombre "MERCADONA   Av Roma, 22 Barcelona")
    (x 41.381136)
    (y 2.145580))

([onto_Class2000127] of  hipermercado

    (nombre "CONSUM  C/ Lluca, 19    Barcelona")
    (x 41.382611)
    (y 2.130516))

([onto_Class2000128] of  hipermercado

    (nombre "CADONA   C/ Anden Estacion, 48-54    Barcelona")
    (x 41.437665)
    (y 2.192566))

([onto_Class2000129] of  hipermercado

    (nombre " G Via Corts Catalanes, 1.107-1.109  Barcelona")
    (x 41.417570)
    (y 2.204940))

([onto_Class2000130] of  hipermercado

    (nombre "EA   CONSUM BASIC    C/ Llull, 465   Barcelona")
    (x 41.413062)
    (y 2.217578))

([onto_Class2000131] of  hipermercado

    (nombre "ON ARDIA MARKET  C/ Rocafort, 244    Barcelona")
    (x 41.385107)
    (y 2.148894))

([onto_Class2000132] of  hipermercado

    (nombre "MERBDIA MARKET  C/ Rocafort, 39 Barcelona ")
    (x 41.385107)
    (y 2.148894))

([onto_Class2000133] of  hipermercado

    (nombre "CARREFOUR   C.C Les Glories Barcelona ")
    (x 41.405879)
    (y 2.191670))

([onto_Class2000134] of  hipermercado

    (nombre "CARREFOUR   C.C La Maquinista   Barcelona")
    (x 41.441989)
    (y 2.198576))

([onto_Class2000135] of  hipermercado

    (nombre "DIA MARKET  C/ Provenca, 106    Barcelona")
    (x 41.385107)
    (y 2.148894))

([onto_Class2000136] of  hipermercado

    (nombre "CONSUM  Av Icaria, 160  Barcelona")
    (x 41.390416)
    (y 2.198541))

([onto_Class2000137] of  hipermercado

    (nombre " MARKET  C/ Gran de Sant Andreu, 275 Barcelona")
    (x 41.435900)
    (y 2.189650))

([onto_Class2000138] of  hipermercado

    (nombre "REFOUR MARKET    C/ Berlin, 50-54    Barcelona")
    (x 41.383581)
    (y 2.139987))

([onto_Class2000139] of  hipermercado

    (nombre "IACARDIA MARKET  C/ Casanova, 154    Barcelona")
    (x 41.385107)
    (y 2.148894))

([onto_Class2000140] of  hipermercado

    (nombre "DCAPRABO C/ Costa i Cuixart, 23  Barcelona")
    (x 41.427712)
    (y 2.173811))

([onto_Class2000141] of  hipermercado

    (nombre "ESCLAT  Pº Valldaura, 148   Barcelona")
    (x 41.439114)
    (y 2.165334))

([onto_Class2000142] of  hipermercado

    (nombre "CAPRABO C/ Consell de Cent, 180 Barcelona")
    (x 41.388744)
    (y 2.162383))

([onto_Class2000143] of  hipermercado

    (nombre " C/ Sant Antoni Maria Claret, 318    Barcelona")
    (x 41.414760)
    (y 2.179270))

([onto_Class2000144] of  hipermercado

    (nombre "PRABOCAPRABO Pº Fabra i Puig, 182    Barcelona")
    (x 41.430411)
    (y 2.179682))

([onto_Class2000145] of  hipermercado

    (nombre "RABO Av. Diagonal, 557, C.C. L’Illa  Barcelona")
    (x 41.389222)
    (y 2.133916))

([onto_Class2000146] of  hipermercado

    (nombre "PCAPRABO Av Republica Argentina, 32  Barcelona")
    (x 41.408619)
    (y 2.147287))

([onto_Class2000147] of  hipermercado

    (nombre "CACACAPRABO Pº Valldaura, 218-220   Barcelona")
    (x 41.437920)
    (y 2.173740))

([onto_Class2000148] of  hipermercado

    (nombre "CONSUM  C/ Berruguete, 106  Barcelona")
    (x 41.433747)
    (y 2.148449))

([onto_Class2000149] of  hipermercado

    (nombre "BON PREU    C/ Taquigraf Serra, 3   Barcelona")
    (x 41.386147)
    (y 2.138641))

([onto_Class2000150] of  hipermercado

    (nombre "CAPRABO Travessera de Gracia, 72    Barcelona")
    (x 41.397151)
    (y 2.151375))

([onto_Class2000151] of  hipermercado

    (nombre "BON PREU    C/ Andrade, 58  Barcelona")
    (x 41.412518)
    (y 2.196696))

([onto_Class2000152] of  hipermercado

    (nombre "BON PREU    C/ Industria, 90-92 Barcelona")
    (x 41.406572)
    (y 2.170562))

([onto_Class2000153] of  hipermercado

    (nombre "BON PREU    C/ Pi i Margall, 22-24  Barcelona")
    (x 41.406627)
    (y 2.163542))

([onto_Class2000154] of  hipermercado

    (nombre "BON PREU    C/ Provenca, 185    Barcelona")
    (x 41.391193)
    (y 2.156535))

([onto_Class2000155] of  hipermercado

    (nombre " C/ Sant Antoni Maria Claret, 264    Barcelona")
    (x 41.412521)
    (y 2.176423))

([onto_Class2000156] of  hipermercado

    (nombre "N PREU   JESPAC  C/ Muntaner, 187    Barcelona")
    (x 41.392704)
    (y 2.151182))

([onto_Class2000157] of  hipermercado

    (nombre "OBON PREU    C/ Capita Arenas, 58    Barcelona")
    (x 41.393299)
    (y 2.124978))

([onto_Class2000158] of  hipermercado

    (nombre "BCONDIS  Av Vallcarca, 40    Barcelona")
    (x 41.409422)
    (y 2.148174))

([onto_Class2000159] of  hipermercado

    (nombre "CONDIS  C/ Carles III, 49   Barcelona")
    (x 41.382594)
    (y 2.127719))

([onto_Class2000160] of  hipermercado

    (nombre "CONDIS  C/ Guipuscoa, 132   Barcelona")
    (x 41.420763)
    (y 2.203379))

([onto_Class2000161] of  hipermercado

    (nombre "KEISY   C/ Corsega, 54  Barcelona")
    (x 41.384278)
    (y 2.143318))

([onto_Class2000162] of  hipermercado

    (nombre "CORTE INGLeS Av Diagonal, 471-473    Barcelona")
    (x 41.392564)
    (y 2.146355))

([onto_Class2000163] of  hipermercado

    (nombre "EL CONDIS  Av Paral·lel, 93    Barcelona")
    (x 41.374782)
    (y 2.164517))

([onto_Class2000164] of  hipermercado

    (nombre "CONDIS  C/ Murcia, 43   Barcelona")
    (x 41.416609)
    (y 2.190209))

([onto_Class2000165] of  hipermercado

    (nombre "CONDIS  Pº Zona Franca, 210 Barcelona")
    (x 41.360029)
    (y 2.139429))

([onto_Class2000166] of  hipermercado

    (nombre "JESPAC  Av Borbo, 64-66 Barcelona")
    (x 41.428640)
    (y 2.174637))

([onto_Class2000167] of  hipermercado

    (nombre "KEISY   Rambla de Poble Nou, 145    Barcelona")
    (x 41.405901)
    (y 2.194686))

([onto_Class2000168] of  hipermercado

    (nombre "CONDIS  C/ Concepcio Arenal, 219    Barcelona")
    (x 41.430690)
    (y 2.184462))

([onto_Class2000169] of  hipermercado

    (nombre "CONDIS  C/ Joan Güell, 98-106   Barcelona")
    (x 41.381249)
    (y 2.132341))

([onto_Class2000170] of  hipermercado

    (nombre "CONDIS  Pº Valldaura, 242-244   Barcelona")
    (x 41.438632)
    (y 2.177544))

([onto_Class2000171] of  hipermercado

    (nombre "PLUS FRESC  Via Augusta, 188    Barcelona")
    (x 41.398445)
    (y 2.141767))

([onto_Class2000172] of  hipermercado

    (nombre "CORTE INGLeS Av Diagonal, 617-619    Barcelona")
    (x 41.387724)
    (y 2.128022))

([onto_Class2000173] of  hipermercado

    (nombre "EL DISGOR  Av Sarria, 27   Barcelona")
    (x 41.387856)
    (y 2.136738))

([onto_Class2000174] of  hipermercado

    (nombre "SPAR    C/ Muntaner, 175    Barcelona")
    (x 41.391872)
    (y 2.152290))

([onto_Class2000175] of  hipermercado

    (nombre "SUMA    Rda. Guinardo, 210  Barcelona")
    (x 41.421998)
    (y 2.177350))

([onto_Class2000176] of  hipermercado

    (nombre "SORLI DISCAU    C/ Gran de Sant Andreu, 104 Barcelona")
    (x 41.439526)
    (y 2.188752))

([onto_Class2000177] of  hipermercado

    (nombre "ISCAU    Mare de Deu del Port, 291   Barcelona")
    (x 41.360172)
    (y 2.141044))

([onto_Class2000178] of  hipermercado

    (nombre "SORLI DISCAU    C/ Provenca, 49 Barcelona")
    (x 41.384310)
    (y 2.147493))

([onto_Class2000179] of  hipermercado

    (nombre "OSORLI DISCAU    Pz del Porxos, 1    Barcelona")
    (x 41.413843)
    (y 2.199639))

([onto_Class2000180] of  hipermercado

    (nombre "SORLI DISCAU    Mare de Deu Montserrat, 267 Barcelona")
    (x 41.424738)
    (y 2.176210))

([onto_Class2000181] of  hipermercado

    (nombre "SORLI DISCAU    C/ Marina, 63   Barcelona")
    (x 41.391687)
    (y 2.190585))

([onto_Class90000] of  parada_bus

  (nombre "Bus linea V3  Pol. Ind. Zona Franca")
  (x 41.345494)
  (y 2.142764))

([onto_Class90001] of  parada_bus

  (nombre "Bus linea V3  Transports Metropolitans de Barcelona")
  (x 41.3464102)
  (y 2.1432353))

([onto_Class90002] of  parada_bus

  (nombre "Bus linea V3  Pg. Zona Franca-Motors")
  (x 41.351695)
  (y 2.145339))

([onto_Class90003] of  parada_bus

  (nombre "Bus linea V3  Pg. Zona Franca-Cisell")
  (x 41.354813)
  (y 2.1432056))

([onto_Class90004] of  parada_bus

  (nombre "Bus linea V3  Pg. Zona Franca-Alts Forns")
  (x 41.358062)
  (y 2.14069))

([onto_Class90005] of  parada_bus

  (nombre "Bus linea V3  Pg. Zona Franca-Foneria")
  (x 41.3601132)
  (y 2.138971))

([onto_Class90006] of  parada_bus

  (nombre "Bus linea V3  Pg. Zona Franca-Mineria")
  (x 41.362303)
  (y 2.13764))

([onto_Class90007] of  parada_bus

  (nombre "Bus linea V3  Ildefons Cerda-Rambla Badal")
  (x 41.365078)
  (y 2.13574))

([onto_Class90008] of  parada_bus

  (nombre "Bus linea V3  Badal-Av. Carrilet")
  (x 41.3666153)
  (y 2.134608))

([onto_Class90009] of  parada_bus

  (nombre "Bus linea V3  Badal-Constitucio")
  (x 41.368597)
  (y 2.132776))

([onto_Class90010] of  parada_bus

  (nombre "Bus linea V3  Carles III-Les Corts")
  (x 41.382146)
  (y 2.128466))

([onto_Class90011] of  parada_bus

  (nombre "Bus linea V3  Carles III-Mejia Lequerica")
  (x 41.38524)
  (y 2.127519))

([onto_Class90012] of  parada_bus

  (nombre "Bus linea V3  Diagonal-Joan Güell")
  (x 41.388026)
  (y 2.128122))

([onto_Class90013] of  parada_bus

  (nombre "Bus linea V3  Maria Cristina-Gran Via")
  (x 40.4218629)
  (y -3.7009052))

([onto_Class90014] of  parada_bus

  (nombre "Bus linea V3  Carles III-Numancia")
  (x 41.3917276)
  (y 2.1292777))

([onto_Class90015] of  parada_bus

  (nombre "Bus linea V3  Manuel Girona-Carles III")
  (x 41.3921686)
  (y 2.1281568))

([onto_Class90016] of  parada_bus

  (nombre "Bus linea V3  Benet i Mateu-Manuel de Falla")
  (x 41.393853)
  (y 2.12624))

([onto_Class90017] of  parada_bus

  (nombre "Bus linea V3  Santa Amelia-Capita Arenas")
  (x 41.394194)
  (y 2.124073))

([onto_Class90018] of  parada_bus

  (nombre "Bus linea V3  Santa Amelia")
  (x -23.2620277)
  (y -50.42048080000001))

([onto_Class90019] of  parada_bus

  (nombre "Bus linea V3  Av J V Foix-Pedro de la Creu")
  (x 41.39558)
  (y 2.120108))

([onto_Class90020] of  parada_bus

  (nombre "Bus linea V3  Av J V Foix-Pg Reina Elisenda")
  (x 41.398025)
  (y 2.119089))

([onto_Class90021] of  parada_bus

  (nombre "Bus linea V3  Av J V Foix-Monestir")
  (x 41.399586)
  (y 2.116707))

([onto_Class90022] of  parada_bus

  (nombre "Bus linea V3  Ronda de Dalt-Av J V Foix")
  (x 41.401461)
  (y 2.11421))

([onto_Class90023] of  parada_bus

  (nombre "Bus linea V3  Esports-Can Caralleu")
  (x 41.4016081)
  (y 2.1109972))

([onto_Class90024] of  parada_bus

  (nombre "Bus linea V3  Can Caralleu")
  (x 41.4016081)
  (y 2.1109972))

([onto_Class90025] of  parada_bus

  (nombre "Bus linea V3  Can Caralleu")
  (x 41.4016081)
  (y 2.1109972))

([onto_Class90026] of  parada_bus

  (nombre "Bus linea V3  Ronda de Dalt-Av. J.V.Foix")
  (x 41.401461)
  (y 2.11421))

([onto_Class90027] of  parada_bus

  (nombre "Bus linea V3  Av J V Foix-Monestir")
  (x 41.399586)
  (y 2.116707))

([onto_Class90028] of  parada_bus

  (nombre "Bus linea V3  Av. J.V.Foix-Pg. Reina Elisenda")
  (x 41.398025)
  (y 2.119089))

([onto_Class90029] of  parada_bus

  (nombre "Bus linea V3  Av. J.V.Foix-Pedro de la Creu")
  (x 41.39558)
  (y 2.120108))

([onto_Class90031] of  parada_bus

  (nombre "Bus linea V3  Doctor Ferran")
  (x 34.3008056)
  (y -83.8197311))

([onto_Class90032] of  parada_bus

  (nombre "Bus linea V3  Capita Arenas-Maria Cristina")
  (x 41.388963)
  (y 2.126615))

([onto_Class90033] of  parada_bus

  (nombre "Bus linea V3  Carles III-Maria Cristina")
  (x 41.3893674)
  (y 2.1280683))

([onto_Class90034] of  parada_bus

  (nombre "Bus linea V3  Carles III-Mejia Lequerica")
  (x 41.38524)
  (y 2.127519))

([onto_Class90035] of  parada_bus

  (nombre "Bus linea V3  Carles III-Les Corts")
  (x 41.382146)
  (y 2.128466))

([onto_Class90036] of  parada_bus

  (nombre "Bus linea V3  Carles III-Av. Madrid")
  (x 40.4474745)
  (y -3.7919925))

([onto_Class90037] of  parada_bus

  (nombre "Bus linea V3  Badal-Constitucio")
  (x 41.368597)
  (y 2.132776))

([onto_Class90038] of  parada_bus

  (nombre "Bus linea V3  Badal-Av. Carrilet")
  (x 41.3666153)
  (y 2.134608))

([onto_Class90039] of  parada_bus

  (nombre "Bus linea V3  Ildelfons Cerda-Ciutat de la Justicia")
  (x 41.363809)
  (y 2.133837))

([onto_Class90040] of  parada_bus

  (nombre "Bus linea V3  Gran Via-Radi")
  (x 40.4203939)
  (y -3.7035204))

([onto_Class90041] of  parada_bus

  (nombre "Bus linea V3  Pg. Zona Franca-Ctra del Prat")
  (x 41.36206079999999)
  (y 2.1375426))

([onto_Class90042] of  parada_bus

  (nombre "Bus linea V3  Pg. Zona Franca-Foneria")
  (x 41.3601132)
  (y 2.138971))

([onto_Class90043] of  parada_bus

  (nombre "Bus linea V3  Pg. Zona Franca-Foc")
  (x 41.356714)
  (y 2.14134))

([onto_Class90044] of  parada_bus

  (nombre "Bus linea V3  Pg. Zona Franca-Cisell")
  (x 41.354813)
  (y 2.1432056))

([onto_Class90045] of  parada_bus

  (nombre "Bus linea V3  Pg. Zona Franca-Motors")
  (x 41.351695)
  (y 2.145339))

([onto_Class90046] of  parada_bus

  (nombre "Bus linea V3  Pol. Ind. Zona Franca")
  (x 41.345494)
  (y 2.142764))

([onto_Class90092] of  parada_bus

  (nombre "Bus linea H6  Zona Universitaria")
  (x 41.3843292)
  (y 2.1116123))

([onto_Class90093] of  parada_bus

  (nombre "Bus linea H6  ETS d'Arquitectura")
  (x 41.38483799999999)
  (y 2.113992))

([onto_Class90094] of  parada_bus

  (nombre "Bus linea H6  Palau Reial")
  (x 41.3884966)
  (y 2.1170465))

([onto_Class90095] of  parada_bus

  (nombre "Bus linea H6  Pl. Pius XII")
  (x 41.38738)
  (y 2.1218605))

([onto_Class90096] of  parada_bus

  (nombre "Bus linea H6  Pl Reina Maria Cristina")
  (x 41.3730751)
  (y 2.1504629))

([onto_Class90097] of  parada_bus

  (nombre "Bus linea H6  Maria Cristina-Gran Via")
  (x 40.4218629)
  (y -3.7009052))

([onto_Class90098] of  parada_bus

  (nombre "Bus linea H6  Carles III-Numancia")
  (x 41.3917276)
  (y 2.1292777))

([onto_Class90099] of  parada_bus

  (nombre "Bus linea H6  Mitre-Av. Sarria")
  (x 41.393021)
  (y 2.131147))

([onto_Class90100] of  parada_bus

  (nombre "Bus linea H6  Mitre-Doctor Fleming")
  (x 41.3948235)
  (y 2.1334478))

([onto_Class90101] of  parada_bus

  (nombre "Bus linea H6  Mitre-Ganduxer")
  (x 41.398892)
  (y 2.133914))

([onto_Class90102] of  parada_bus

  (nombre "Bus linea H6  Mitre-Mandri")
  (x 41.40053959999999)
  (y 2.1356606))

([onto_Class90103] of  parada_bus

  (nombre "Bus linea H6  Mitre-Balmes")
  (x 41.40446)
  (y 2.140848))

([onto_Class90104] of  parada_bus

  (nombre "Bus linea H6  Mitre-Saragossa")
  (x 41.4053682)
  (y 2.143727))

([onto_Class90105] of  parada_bus

  (nombre "Bus linea H6  General Mitre-Berna")
  (x -34.5805219)
  (y -58.4775864))

([onto_Class90106] of  parada_bus

  (nombre "Bus linea H6  Travessera de Dalt-Verdi")
  (x 41.4089081)
  (y 2.1530419))

([onto_Class90107] of  parada_bus

  (nombre "Bus linea H6  Travessera de Dalt-La Granja")
  (x 41.40884)
  (y 2.153526))

([onto_Class90108] of  parada_bus

  (nombre "Bus linea H6  Travessera de Dalt-Torrent de les Flors")
  (x 41.4105)
  (y 2.1566644))

([onto_Class90109] of  parada_bus

  (nombre "Bus linea H6  Guinardo-Lepant")
  (x 41.411698)
  (y 2.167011))

([onto_Class90110] of  parada_bus

  (nombre "Bus linea H6  Recinte sanitari Sant Pau")
  (x 41.414517)
  (y 2.169839))

([onto_Class90111] of  parada_bus

  (nombre "Bus linea H6  Verge de Montserrat-Cartagena")
  (x 41.416051)
  (y 2.169892))

([onto_Class90112] of  parada_bus

  (nombre "Bus linea H6  Verge de Montserrat-La Bisbal")
  (x 41.417988)
  (y 2.171696))

([onto_Class90113] of  parada_bus

  (nombre "Bus linea H6  Verge de Montserrat-Periodistes")
  (x 41.421689)
  (y 2.174279))

([onto_Class90114] of  parada_bus

  (nombre "Bus linea H6  Metro Maragall")
  (x 41.4243336)
  (y 2.1772207))

([onto_Class90115] of  parada_bus

  (nombre "Bus linea H6  Ramon Albo-Alexandre Gali")
  (x 41.426691)
  (y 2.1773138))

([onto_Class90116] of  parada_bus

  (nombre "Bus linea H6  Escocia")
  (x 56.49067119999999)
  (y -4.2026458))

([onto_Class90117] of  parada_bus

  (nombre "Bus linea H6  Fabra i Puig")
  (x 41.4296341)
  (y 2.183661))

([onto_Class90118] of  parada_bus

  (nombre "Bus linea H6   Fabra i Puig")
  (x 41.4296341)
  (y 2.183661))

([onto_Class90119] of  parada_bus

  (nombre "Bus linea H6  Fabra i Puig-Meridiana")
  (x 41.430261)
  (y 2.181997))

([onto_Class90120] of  parada_bus

  (nombre "Bus linea H6  Fabra i Puig-Arnau d'Oms")
  (x 41.430324)
  (y 2.177722))

([onto_Class90122] of  parada_bus

  (nombre "Bus linea H6  Borbo-Pg Maragall")
  (x 41.4264279)
  (y 2.1758644))

([onto_Class90123] of  parada_bus

  (nombre "Bus linea H6  Metro Maragall")
  (x 41.4243336)
  (y 2.1772207))

([onto_Class90124] of  parada_bus

  (nombre "Bus linea H6  Verge de Montserrat-Periodistes")
  (x 41.421689)
  (y 2.174279))

([onto_Class90125] of  parada_bus

  (nombre "Bus linea H6  Pl del Nen de la Rutlla")
  (x 41.4187914)
  (y 2.1716755))

([onto_Class90126] of  parada_bus

  (nombre "Bus linea H6  Verge de Montserrat-Genova")
  (x 41.41654)
  (y 2.1701672))

([onto_Class90127] of  parada_bus

  (nombre "Bus linea H6  Recinte sanitari Sant Pau")
  (x 41.414517)
  (y 2.169839))

([onto_Class90128] of  parada_bus

  (nombre "Bus linea H6  Guinardo-Parc de les Aigües")
  (x 41.4131591)
  (y 2.1654891))

([onto_Class90129] of  parada_bus

  (nombre "Bus linea H6  CAP Larrard")
  (x 41.4104931)
  (y 2.1561477))

([onto_Class90130] of  parada_bus

  (nombre "Bus linea H6  Travessera de Dalt-Verdi")
  (x 41.4089081)
  (y 2.1530419))

([onto_Class90131] of  parada_bus

  (nombre "Bus linea H6  General Mitre-Padua")
  (x 41.405824)
  (y 2.146153))

([onto_Class90132] of  parada_bus

  (nombre "Bus linea H6  Mitre-Saragossa")
  (x 41.4053682)
  (y 2.143727))

([onto_Class90133] of  parada_bus

  (nombre "Bus linea H6  Rda. General Mitre, 177-181")
  (x 41.4046633)
  (y 2.1409673))

([onto_Class90134] of  parada_bus

  (nombre "Bus linea H6  Mitre-Mandri")
  (x 41.40053959999999)
  (y 2.1356606))

([onto_Class90135] of  parada_bus

  (nombre "Bus linea H6  Mitre-Ganduxer")
  (x 41.398892)
  (y 2.133914))

([onto_Class90136] of  parada_bus

  (nombre "Bus linea H6  Mitre-Doctor Fleming")
  (x 41.3948235)
  (y 2.1334478))

([onto_Class90137] of  parada_bus

  (nombre "Bus linea H6  Mitre-Prat de la Riba")
  (x 41.393274)
  (y 2.131301))

([onto_Class90138] of  parada_bus

  (nombre "Bus linea H6  Manuel Girona-Carles III")
  (x 41.3921686)
  (y 2.1281568))

([onto_Class90139] of  parada_bus

  (nombre "Bus linea H6  Capita Arenas-Maria Cristina")
  (x 41.388963)
  (y 2.126615))

([onto_Class90140] of  parada_bus

  (nombre "Bus linea H6  Pl Pius XII")
  (x 41.38738)
  (y 2.1218605))

([onto_Class90141] of  parada_bus

  (nombre "Bus linea H6  Palau Reial")
  (x 41.3884966)
  (y 2.1170465))

([onto_Class90142] of  parada_bus

  (nombre "Bus linea H6  Zona Universitaria")
  (x 41.3843292)
  (y 2.1116123))

([onto_Class90200] of  parada_bus

  (nombre "Bus linea V7  Tarragona-Pl. Espanya")
  (x 41.37592)
  (y 2.148445))

([onto_Class90201] of  parada_bus

  (nombre "Bus linea V7  Tarragona-Pl. Espanya")
  (x 41.37592)
  (y 2.148445))

([onto_Class90202] of  parada_bus

  (nombre "Bus linea V7  Metro Tarragona")
  (x 41.3782235)
  (y 2.1460395))

([onto_Class90203] of  parada_bus

  (nombre "Bus linea V7  Av. Roma-Tarragona")
  (x 41.1181282)
  (y 1.2391767))

([onto_Class90204] of  parada_bus

  (nombre "Bus linea V7  Nicaragua-Josep Tarradellas")
  (x 41.382902)
  (y 2.141836))

([onto_Class90205] of  parada_bus

  (nombre "Bus linea V7  Nicaragua-Parc Les Corts")
  (x 41.386253)
  (y 2.137775))

([onto_Class90206] of  parada_bus

  (nombre "Bus linea V7  Constanca")
  (x 47.6779496)
  (y 9.173238399999999))

([onto_Class90207] of  parada_bus

  (nombre "Bus linea V7  Av Sarria")
  (x 41.3911614)
  (y 2.1392462))

([onto_Class90208] of  parada_bus

  (nombre "Bus linea V7  Av. Sarria-Prat de la Riba")
  (x 41.392524)
  (y 2.130705))

([onto_Class90209] of  parada_bus

  (nombre "Bus linea V7  Pg Sant Joan Bosco")
  (x 41.394086)
  (y 2.1280118))

([onto_Class90210] of  parada_bus

  (nombre "Bus linea V7  Els Vergos")
  (x 41.3968895)
  (y 2.1268155))

([onto_Class90211] of  parada_bus

  (nombre "Bus linea V7  Via Augusta-Pau Alcover")
  (x 41.39847)
  (y 2.1259687))

([onto_Class90212] of  parada_bus

  (nombre "Bus linea V7  Pg Bonanova")
  (x 41.4032663)
  (y 2.1273953))

([onto_Class90213] of  parada_bus

  (nombre "Bus linea V7  CAP Sarria")
  (x 41.40187239999999)
  (y 2.1217065))

([onto_Class90214] of  parada_bus

  (nombre "Bus linea V7  Pl Borras")
  (x 39.16990819999999)
  (y -0.2585067))

([onto_Class90215] of  parada_bus

  (nombre "Bus linea V7  Sarria")
  (x 41.3993167)
  (y 2.1341754))

([onto_Class90216] of  parada_bus

  (nombre "Bus linea V7  Sarria")
  (x 41.3993167)
  (y 2.1341754))

([onto_Class90217] of  parada_bus

  (nombre "Bus linea V7  Pl Borras")
  (x 39.16990819999999)
  (y -0.2585067))

([onto_Class90218] of  parada_bus

  (nombre "Bus linea V7  Via Augusta-Margenat")
  (x 41.402008)
  (y 2.12044))

([onto_Class90219] of  parada_bus

  (nombre "Bus linea V7  Pg Bonanova")
  (x 41.4032663)
  (y 2.1273953))

([onto_Class90220] of  parada_bus

  (nombre "Bus linea V7  Via Augusta-Els Vergos")
  (x 41.39829900000001)
  (y 2.1257916))

([onto_Class90221] of  parada_bus

  (nombre "Bus linea V7  Els Vergos")
  (x 41.3968895)
  (y 2.1268155))

([onto_Class90222] of  parada_bus

  (nombre "Bus linea V7  Pg Sant Joan Bosco")
  (x 41.394086)
  (y 2.1280118))

([onto_Class90223] of  parada_bus

  (nombre "Bus linea V7  Pg. Joan Bosco-Prat de la Riba")
  (x 41.392926)
  (y 2.129376))

([onto_Class90224] of  parada_bus

  (nombre "Bus linea V7  Numancia-Av Diagonal")
  (x 41.3895538)
  (y 2.1316579))

([onto_Class90225] of  parada_bus

  (nombre "Bus linea V7  Numancia-Parc Les Corts")
  (x 41.3861744)
  (y 2.135841))

([onto_Class90226] of  parada_bus

  (nombre "Bus linea V7  Numancia-Berlin")
  (x 41.382759)
  (y 2.139451))

([onto_Class90227] of  parada_bus

  (nombre "Bus linea V7  Numancia-Av. Roma")
  (x 41.381212)
  (y 2.141343))

([onto_Class90228] of  parada_bus

  (nombre "Bus linea V7  Metro Tarragona")
  (x 41.3782235)
  (y 2.1460395))

([onto_Class90229] of  parada_bus

  (nombre "Bus linea V7  Tarragona-Pl. Espanya")
  (x 41.37592)
  (y 2.148445))

([onto_Class90230] of  parada_bus

  (nombre "Bus linea H8  Cardenal Reig-Av St Ramon Nonat")
  (x 41.378374)
  (y 2.117137))

([onto_Class90231] of  parada_bus

  (nombre "Bus linea H8  Riera Blanca-Les Corts")
  (x 41.3770144)
  (y 2.1213071))

([onto_Class90232] of  parada_bus

  (nombre "Bus linea H8  Av de Madrid-Arizala")
  (x 41.377042)
  (y 2.125151))

([onto_Class90233] of  parada_bus

  (nombre "Bus linea H8  Comandant Benitez-Av Madrid")
  (x 41.3791637)
  (y 2.1278128))

([onto_Class90234] of  parada_bus

  (nombre "Bus linea H8  Les Corts-Maternitat")
  (x 41.3821357)
  (y 2.1243323))

([onto_Class90235] of  parada_bus

  (nombre "Bus linea H8  Les Corts-Carles III")
  (x 41.382146)
  (y 2.128466))

([onto_Class90236] of  parada_bus

  (nombre "Bus linea H8  Les Corts-Vallespir")
  (x 41.385284)
  (y 2.132669))

([onto_Class90237] of  parada_bus

  (nombre "Bus linea H8  Numancia-Parc Les Corts")
  (x 41.3861744)
  (y 2.135841))

([onto_Class90238] of  parada_bus

  (nombre "Bus linea H8  Numancia-Marques de Sentmenat")
  (x 41.38464829999999)
  (y 2.1374901))

([onto_Class90239] of  parada_bus

  (nombre "Bus linea H8  Josep Tarradellas-Paris")
  (x 41.38641)
  (y 2.142976))

([onto_Class90240] of  parada_bus

  (nombre "Bus linea H8  Josep Tarradellas-Viladomat")
  (x 41.389195)
  (y 2.14356))

([onto_Class90241] of  parada_bus

  (nombre "Bus linea H8  Josep Tarradellas-Comte Borrell")
  (x 41.3915253)
  (y 2.1442609))

([onto_Class90242] of  parada_bus

  (nombre "Bus linea H8  Diagonal-Francesc Macia")
  (x 41.392697)
  (y 2.1439904))

([onto_Class90243] of  parada_bus

  (nombre "Bus linea H8  Diagonal-Muntaner")
  (x 41.3948346)
  (y 2.1485427))

([onto_Class90244] of  parada_bus

  (nombre "Bus linea H8  Diagonal-Balmes")
  (x 41.3950474)
  (y 2.1546307))

([onto_Class90245] of  parada_bus

  (nombre "Bus linea H8  Diagonal-Pau Claris")
  (x 41.396737)
  (y 2.160649))

([onto_Class90246] of  parada_bus

  (nombre "Bus linea H8  Jardinets de Gracia-Rossello")
  (x 41.3980549)
  (y 2.1637353))

([onto_Class90247] of  parada_bus

  (nombre "Bus linea H8  Rossello-Bailen")
  (x 41.3998071)
  (y 2.166024))

([onto_Class90248] of  parada_bus

  (nombre "Bus linea H8  Pg de Sant Joan-Corsega")
  (x 41.4015095)
  (y 2.1671605))

([onto_Class90249] of  parada_bus

  (nombre "Bus linea H8  Industria-Napols")
  (x 41.4048768)
  (y 2.1682783))

([onto_Class90250] of  parada_bus

  (nombre "Bus linea H8  Industria-Lepant")
  (x 41.407479)
  (y 2.171759))

([onto_Class90251] of  parada_bus

  (nombre "Bus linea H8  Av Gaudi")
  (x 41.407903)
  (y 2.1742985))

([onto_Class90252] of  parada_bus

  (nombre "Bus linea H8  Industria-Independencia")
  (x 41.41266450000001)
  (y 2.1785224))

([onto_Class90253] of  parada_bus

  (nombre "Bus linea H8  Industria-Freser")
  (x 41.415183)
  (y 2.18178))

([onto_Class90254] of  parada_bus

  (nombre "Bus linea H8  Navas de Tolosa-Industria")
  (x 41.4166509)
  (y 2.1849656))

([onto_Class90255] of  parada_bus

  (nombre "Bus linea H8  Meridiana-Josep Estivill")
  (x 41.4163256)
  (y 2.1870584))

([onto_Class90256] of  parada_bus

  (nombre "Bus linea H8  Meridiana-La Sagrera")
  (x 41.4214575)
  (y 2.1869366))

([onto_Class90257] of  parada_bus

  (nombre "Bus linea H8  Garcilaso-La Sagrera")
  (x 41.422642)
  (y 2.189182))

([onto_Class90258] of  parada_bus

  (nombre "Bus linea H8  Berenguer de Palou-Pont del Treball")
  (x 41.42329)
  (y 2.1918373))

([onto_Class90259] of  parada_bus

  (nombre "Bus linea H8  Berenguer de Palou-Pare Manyanet")
  (x 41.427496)
  (y 2.191686))

([onto_Class90260] of  parada_bus

  (nombre "Bus linea H8  Pare Manyanet-Josep Soldevila")
  (x 41.428165)
  (y 2.1947927))

([onto_Class90261] of  parada_bus

  (nombre "Bus linea H8  Segre-Borriana")
  (x 41.43091)
  (y 2.19484))

([onto_Class90262] of  parada_bus

  (nombre "Bus linea H8  Segre-Torrent Estadella")
  (x 41.43337229999999)
  (y 2.1927595))

([onto_Class90263] of  parada_bus

  (nombre "Bus linea H8  Parc de La Maquinista")
  (x 41.4367159)
  (y 2.1953359))

([onto_Class90264] of  parada_bus

  (nombre "Bus linea H8  La Maquinista")
  (x 41.44083089999999)
  (y 2.1984927))

([onto_Class90265] of  parada_bus

  (nombre "Bus linea H8  La Maquinista")
  (x 41.44083089999999)
  (y 2.1984927))

([onto_Class90266] of  parada_bus

  (nombre "Bus linea H8  Ciutat d'Asuncion-Pg de l'Havana")
  (x 41.4386711)
  (y 2.1993859))

([onto_Class90267] of  parada_bus

  (nombre "Bus linea H8  Parc de la Maquinista")
  (x 41.4367159)
  (y 2.1953359))

([onto_Class90268] of  parada_bus

  (nombre "Bus linea H8  Josep Soldevila-Torrent Estadella")
  (x 41.434116)
  (y 2.193555))

([onto_Class90269] of  parada_bus

  (nombre "Bus linea H8  Josep Soldevila-Onze de Setembre")
  (x 41.431707)
  (y 2.195377))

([onto_Class90270] of  parada_bus

  (nombre "Bus linea H8  Onze de Setembre-Virgili")
  (x 41.430162)
  (y 2.193125))

([onto_Class90271] of  parada_bus

  (nombre "Bus linea H8  Gran de Sant Andreu-Fabra i Puig")
  (x 41.429289)
  (y 2.190011))

([onto_Class90272] of  parada_bus

  (nombre "Bus linea H8  Sagrera-Parc de la Pegaso")
  (x 41.4272893)
  (y 2.1891976))

([onto_Class90273] of  parada_bus

  (nombre "Bus linea H8  Sagrera-Marti Molins")
  (x 41.424042)
  (y 2.190122))

([onto_Class90274] of  parada_bus

  (nombre "Bus linea H8  Av Meridiana-La Sagrera")
  (x 41.424854)
  (y 2.1865331))

([onto_Class90275] of  parada_bus

  (nombre "Bus linea H8  Sant Antoni Maria Claret-Felip II")
  (x 41.420146)
  (y 2.18629))

([onto_Class90276] of  parada_bus

  (nombre "Bus linea H8  Sant Antoni Maria Claret-Trinxant")
  (x 41.416811)
  (y 2.181734))

([onto_Class90277] of  parada_bus

  (nombre "Bus linea H8  Sant Antoni Maria Claret-Rambla Volart")
  (x 41.415269)
  (y 2.179668))

([onto_Class90278] of  parada_bus

  (nombre "Bus linea H8  Sant Antoni Maria Claret-Independencia")
  (x 41.413257)
  (y 2.176982))

([onto_Class90279] of  parada_bus

  (nombre "Bus linea H8  Av. Gaudi")
  (x 41.407903)
  (y 2.1742985))

([onto_Class90280] of  parada_bus

  (nombre "Bus linea H8  Sant Antoni Maria Claret-Lepant")
  (x 41.4083345)
  (y 2.1704098))

([onto_Class90281] of  parada_bus

  (nombre "Bus linea H8  Sant Antoni Maria Claret-Sardenya")
  (x 41.406622)
  (y 2.168156))

([onto_Class90282] of  parada_bus

  (nombre "Bus linea H8  Sant Antoni Maria Claret-Napols")
  (x 41.404991)
  (y 2.1659032))

([onto_Class90283] of  parada_bus

  (nombre "Bus linea H8  Pg de Sant Joan-Industria")
  (x 41.402794)
  (y 2.1647913))

([onto_Class90284] of  parada_bus

  (nombre "Bus linea H8  Corcega-Bailen")
  (x 41.4009147)
  (y 2.1650477))

([onto_Class90285] of  parada_bus

  (nombre "Bus linea H8  Jardinets de Gracia-Lluria")
  (x 41.3973235)
  (y 2.1588912))

([onto_Class90286] of  parada_bus

  (nombre "Bus linea H8  Corsega-Pg. de Gracia")
  (x 41.397099)
  (y 2.160005))

([onto_Class90287] of  parada_bus

  (nombre "Bus linea H8  Diagonal-Balmes")
  (x 41.3950474)
  (y 2.1546307))

([onto_Class90288] of  parada_bus

  (nombre "Bus linea H8  Diagonal-Muntaner")
  (x 41.3948346)
  (y 2.1485427))

([onto_Class90289] of  parada_bus

  (nombre "Bus linea H8  Diagonal-Francesc Macia")
  (x 41.392697)
  (y 2.1439904))

([onto_Class90290] of  parada_bus

  (nombre "Bus linea H8  Josep Tarradellas-Francesc Macia")
  (x 41.3916353)
  (y 2.1437497))

([onto_Class90291] of  parada_bus

  (nombre "Bus linea H8  Les Corts-Pl Dr Ignasi Barraquer")
  (x 41.390115)
  (y 2.140635))

([onto_Class90292] of  parada_bus

  (nombre "Bus linea H8  Les Corts-Numancia")
  (x 41.3861744)
  (y 2.135841))

([onto_Class90293] of  parada_bus

  (nombre "Bus linea H8  Les Corts-Vallespir")
  (x 41.385284)
  (y 2.132669))

([onto_Class90294] of  parada_bus

  (nombre "Bus linea H8  Mercat de Les Corts")
  (x 41.3839542)
  (y 2.1297776))

([onto_Class90295] of  parada_bus

  (nombre "Bus linea H8  Les Corts-Mejia Lequerica")
  (x 41.3837497)
  (y 2.1250096))

([onto_Class90296] of  parada_bus

  (nombre "Bus linea H8  Les Corts-Arizala")
  (x 41.378609)
  (y 2.1238201))

([onto_Class90297] of  parada_bus

  (nombre "Bus linea H8  Les Corts-Ctra de Collblanc")
  (x 41.3758601)
  (y 2.1135793))

([onto_Class90298] of  parada_bus

  (nombre "Bus linea H8  Collblanc-Cardenal Reig")
  (x 41.375919)
  (y 2.114639))

([onto_Class90299] of  parada_bus

  (nombre "Bus linea H8  Cardenal Reig-Av St Ramon Nonat")
  (x 41.378374)
  (y 2.117137))

([onto_Class90335] of  parada_bus

  (nombre "Bus linea H10 Av. Madrid-Carles III")
  (x 41.378931)
  (y 2.128468))

([onto_Class90336] of  parada_bus

  (nombre "Bus linea H10 Av. Madrid-Joan Güell")
  (x 41.3808341)
  (y 2.132759))

([onto_Class90337] of  parada_bus

  (nombre "Bus linea H10 Pl. del Centre")
  (x 41.381922)
  (y 2.135884))

([onto_Class90338] of  parada_bus

  (nombre "Bus linea H10 Numancia-Berlin")
  (x 41.382759)
  (y 2.139451))

([onto_Class90339] of  parada_bus

  (nombre "Bus linea H10 Av. Roma-Tarragona")
  (x 41.1181282)
  (y 1.2391767))

([onto_Class90340] of  parada_bus

  (nombre "Bus linea H10 Valencia-Entenca")
  (x 41.381048)
  (y 2.147948))

([onto_Class90341] of  parada_bus

  (nombre "Bus linea H10 Valencia-Viladomat")
  (x 41.383576)
  (y 2.151303))

([onto_Class90342] of  parada_bus

  (nombre "Bus linea H10 Valencia-Comte d'Urgell")
  (x 41.38604040000001)
  (y 2.1545608))

([onto_Class90343] of  parada_bus

  (nombre "Bus linea H10 Valencia-Villarroel")
  (x 41.386831)
  (y 2.155608))

([onto_Class90344] of  parada_bus

  (nombre "Bus linea H10 Valencia-Muntaner")
  (x 41.3885692)
  (y 2.1578872))

([onto_Class90345] of  parada_bus

  (nombre "Bus linea H10 Valencia-Balmes")
  (x 41.390198)
  (y 2.160081))

([onto_Class90346] of  parada_bus

  (nombre "Bus linea H10 Valencia-Pg de Gracia")
  (x 39.4771844)
  (y -0.3906743))

([onto_Class90347] of  parada_bus

  (nombre "Bus linea H10 Valencia-Roger de Lluria")
  (x 41.394666)
  (y 2.166028))

([onto_Class90348] of  parada_bus

  (nombre "Bus linea H10 Mercat de la Concepcio")
  (x 41.3958794)
  (y 2.16857))

([onto_Class90349] of  parada_bus

  (nombre "Bus linea H10 Valencia-Pg de Sant Joan")
  (x 41.398843)
  (y 2.170462))

([onto_Class90350] of  parada_bus

  (nombre "Bus linea H10 Valencia-Diagonal")
  (x 41.400688)
  (y 2.174058))

([onto_Class90351] of  parada_bus

  (nombre "Bus linea H10 Valencia-Lepant")
  (x 41.403208)
  (y 2.177417))

([onto_Class90352] of  parada_bus

  (nombre "Bus linea H10 Valencia-Padilla")
  (x 39.4699399)
  (y -0.37905))

([onto_Class90353] of  parada_bus

  (nombre "Bus linea H10 Valencia-Dos de Maig")
  (x 41.406488)
  (y 2.181804))

([onto_Class90354] of  parada_bus

  (nombre "Bus linea H10 Valencia-Rogent")
  (x 41.4092163)
  (y 2.1854506))

([onto_Class90356] of  parada_bus

  (nombre "Bus linea H10 Espronceda- Guipuscoa")
  (x 41.41414899999999)
  (y 2.192836))

([onto_Class90357] of  parada_bus

  (nombre "Bus linea H10 Guipuscoa-Bac de Roda")
  (x 41.415026)
  (y 2.195089))

([onto_Class90358] of  parada_bus

  (nombre "Bus linea H10 Guipuscoa-Julian Besteiro")
  (x 41.4164884)
  (y 2.1975468))

([onto_Class90359] of  parada_bus

  (nombre "Bus linea H10 Guipuscoa-Selva de Mar")
  (x 41.41746)
  (y 2.1982636))

([onto_Class90360] of  parada_bus

  (nombre "Bus linea H10 Guipuscoa-Cantabria")
  (x 41.420063)
  (y 2.201754))

([onto_Class90361] of  parada_bus

  (nombre "Bus linea H10 Guipuscoa-Rambla Prim")
  (x 41.422359)
  (y 2.204269))

([onto_Class90362] of  parada_bus

  (nombre "Bus linea H10 Guipuscoa-Extremadura")
  (x 41.4247246)
  (y 2.2085674))

([onto_Class90363] of  parada_bus

  (nombre "Bus linea H10 Guipuscoa-Biscaia")
  (x 41.426889)
  (y 2.2110928))

([onto_Class90364] of  parada_bus

  (nombre "Bus linea H10 Av. Pi i Margall-Ricart")
  (x 41.4322303)
  (y 2.2155689))

([onto_Class90365] of  parada_bus

  (nombre "Bus linea H10 Alfons XIII-Santiago")
  (x -33.5039365)
  (y -70.74187549999999))

([onto_Class90366] of  parada_bus

  (nombre "Bus linea H10 Alfons XIII-Av. del Maresme")
  (x 41.4791237)
  (y 2.3061202))

([onto_Class90367] of  parada_bus

  (nombre "Bus linea H10 Alfons XIII-Sant Lluc")
  (x 41.43949)
  (y 2.228244))

([onto_Class90368] of  parada_bus

  (nombre "Bus linea H10 Alfons XIII-Josep M de Segarra")
  (x 41.441119)
  (y 2.231639))

([onto_Class90369] of  parada_bus

  (nombre "Bus linea H10 Av. Marques de Montroig-Pl President Tarradellas")
  (x 41.440867)
  (y 2.234111))

([onto_Class90370] of  parada_bus

  (nombre "Bus linea H10 Av. Marques de Montroig-Pl President Tarradellas")
  (x 41.440867)
  (y 2.234111))

([onto_Class90371] of  parada_bus

  (nombre "Bus linea H10 Alfons XIII-Pau Claris")
  (x 41.441061)
  (y 2.231218))

([onto_Class90372] of  parada_bus

  (nombre "Bus linea H10 Alfons XIII-Sant Lluc")
  (x 41.43949)
  (y 2.228244))

([onto_Class90374] of  parada_bus

  (nombre "Bus linea H10 Alfons XIII-Aribau")
  (x 41.434588)
  (y 2.219009))

([onto_Class90375] of  parada_bus

  (nombre "Bus linea H10 Av Pi i Margall-Av Catalunya")
  (x 41.432803)
  (y 2.215877))

([onto_Class90376] of  parada_bus

  (nombre "Bus linea H10 Tibidabo-Mare de Deu de Montserrat")
  (x 41.4155964)
  (y 2.1464763))

([onto_Class90377] of  parada_bus

  (nombre "Bus linea H10 Guipuscoa-Verneda")
  (x 41.424778)
  (y 2.208037))

([onto_Class90378] of  parada_bus

  (nombre "Bus linea H10 Guipuscoa-Rambla Prim")
  (x 41.422359)
  (y 2.204269))

([onto_Class90379] of  parada_bus

  (nombre "Bus linea H10 Guipuscoa-Cantabria")
  (x 41.420063)
  (y 2.201754))

([onto_Class90380] of  parada_bus

  (nombre "Bus linea H10 Guipuscoa-Selva de Mar")
  (x 41.41746)
  (y 2.1982636))

([onto_Class90381] of  parada_bus

  (nombre "Bus linea H10 Guipuscoa-Bac de Roda")
  (x 41.415026)
  (y 2.195089))

([onto_Class90382] of  parada_bus

  (nombre "Bus linea H10 Clot-Pont de Bac de Roda")
  (x 41.414917)
  (y 2.190514))

([onto_Class90383] of  parada_bus

  (nombre "Bus linea H10 Mallorca-Meridiana")
  (x 41.412524)
  (y 2.187359))

([onto_Class90384] of  parada_bus

  (nombre "Bus linea H10 Mallorca-Rogent")
  (x 41.4103454)
  (y 2.1844504))

([onto_Class90385] of  parada_bus

  (nombre "Bus linea H10 Mallorca-Dos de Maig")
  (x 41.407311)
  (y 2.180417))

([onto_Class90386] of  parada_bus

  (nombre "Bus linea H10 Mallorca-Padilla")
  (x 41.405635)
  (y 2.1781892))

([onto_Class90387] of  parada_bus

  (nombre "Bus linea H10 Mallorca-Marina")
  (x 39.5632622)
  (y 2.6279719))

([onto_Class90388] of  parada_bus

  (nombre "Bus linea H10 Mallorca-Napols")
  (x 41.4012459)
  (y 2.1723747))

([onto_Class90389] of  parada_bus

  (nombre "Bus linea H10 Mallorca-Pg. Sant Joan")
  (x 41.399883)
  (y 2.170463))

([onto_Class90390] of  parada_bus

  (nombre "Bus linea H10 Mallorca-Girona")
  (x 41.972461)
  (y 2.8120355))

([onto_Class90391] of  parada_bus

  (nombre "Bus linea H10 Mallorca-Pau Claris")
  (x 41.394548)
  (y 2.164609))

([onto_Class90392] of  parada_bus

  (nombre "Bus linea H10 Mallorca-Pg de Gracia")
  (x 41.393562)
  (y 2.1633904))

([onto_Class90393] of  parada_bus

  (nombre "Bus linea H10 Mallorca-Balmes")
  (x 41.3911723)
  (y 2.1592153))

([onto_Class90394] of  parada_bus

  (nombre "Bus linea H10 Mallorca-Mercat del Ninot")
  (x 41.3884423)
  (y 2.1546319))

([onto_Class90395] of  parada_bus

  (nombre "Bus linea H10 Mallorca-Comte d'Urgell")
  (x 41.3868041)
  (y 2.1531264))

([onto_Class90396] of  parada_bus

  (nombre "Bus linea H10 Mallorca-Viladomat")
  (x 41.384497)
  (y 2.150058))

([onto_Class90397] of  parada_bus

  (nombre "Bus linea H10 Av. de Roma-Entenca")
  (x 41.382013)
  (y 2.146274))

([onto_Class90398] of  parada_bus

  (nombre "Bus linea H10 Pl. Països Catalans")
  (x 41.37959499999999)
  (y 2.1415167))

([onto_Class90399] of  parada_bus

  (nombre "Bus linea H10 Comtes de Bell Lloc-Viriat")
  (x 41.380449)
  (y 2.137426))

([onto_Class90400] of  parada_bus

  (nombre "Bus linea H10 Pl. del Centre")
  (x 41.381922)
  (y 2.135884))

([onto_Class90401] of  parada_bus

  (nombre "Bus linea H10 Av. de Madrid-Joan Güell")
  (x 41.3808341)
  (y 2.132759))

([onto_Class90402] of  parada_bus

  (nombre "Bus linea H10 Av.Madrid-Carles III")
  (x 41.378931)
  (y 2.128468))

([onto_Class90403] of  parada_bus

  (nombre "Bus linea H10 Av. Madrid-Benavent")
  (x 42.1055664)
  (y -5.736113599999999))

([onto_Class90404] of  parada_bus

  (nombre "Bus linea H10 Arizala-Les Corts")
  (x 41.378609)
  (y 2.1238201))

([onto_Class90405] of  parada_bus

  (nombre "Bus linea H10 Av. Madrid-Arizala")
  (x 41.376134)
  (y 2.123659))

([onto_Class90491] of  parada_bus

  (nombre "Bus linea H12 Gornal")
  (x 18.0777287)
  (y 77.3697672))

([onto_Class90492] of  parada_bus

  (nombre "Bus linea H12 Av Granvia-Miguel Hernandez")
  (x 41.352763)
  (y 2.120214))

([onto_Class90493] of  parada_bus

  (nombre "Bus linea H12 Amadeu Torner")
  (x 41.3604404)
  (y 2.1225093))

([onto_Class90494] of  parada_bus

  (nombre "Bus linea H12 Pl Europa")
  (x 51.7736577)
  (y 19.4676453))

([onto_Class90495] of  parada_bus

  (nombre "Bus linea H12 Av Granvia-Fisica")
  (x 41.3593025)
  (y 2.128855))

([onto_Class90497] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Radi")
  (x 40.4203939)
  (y -3.7035204))

([onto_Class90498] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Quimica")
  (x 41.36590349999999)
  (y 2.1373494))

([onto_Class90500] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Mandoni")
  (x 41.36955)
  (y 2.141984))

([onto_Class90501] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Santa Dorotea")
  (x 41.3718002)
  (y 2.1449802))

([onto_Class90502] of  parada_bus

  (nombre "Bus linea H12 Pl Espanya-FGC")
  (x 41.3745179)
  (y 2.1484676))

([onto_Class90503] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Pl. Espanya")
  (x 41.375671)
  (y 2.149598))

([onto_Class90504] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Entenca")
  (x 41.378385)
  (y 2.153719))

([onto_Class90505] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Viladomat")
  (x 41.3801358)
  (y 2.1559884))

([onto_Class90506] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Urgell")
  (x 41.382487)
  (y 2.158891))

([onto_Class90507] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Muntaner")
  (x 41.384442)
  (y 2.1617136))

([onto_Class90508] of  parada_bus

  (nombre "Bus linea H12 Pl Universitat")
  (x 41.3855048)
  (y 2.1640917))

([onto_Class90509] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Balmes")
  (x 41.387854)
  (y 2.165804))

([onto_Class90510] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Pau Claris")
  (x 41.390946)
  (y 2.1693787))

([onto_Class90512] of  parada_bus

  (nombre "Bus linea H12 Metro Tetuan")
  (x 40.4605557)
  (y -3.6982513))

([onto_Class90513] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Roger de Flor")
  (x 41.39524)
  (y 2.177435))

([onto_Class90514] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Marina")
  (x 41.39861)
  (y 2.180649))

([onto_Class90515] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Padilla")
  (x 41.400555)
  (y 2.1832187))

([onto_Class90516] of  parada_bus

  (nombre "Bus linea H12 Pl. Glories")
  (x 41.4034004)
  (y 2.1868695))

([onto_Class90517] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Llacuna")
  (x 41.406343)
  (y 2.191204))

([onto_Class90518] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Bilbao")
  (x 43.2632503)
  (y -2.9360137))

([onto_Class90519] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Bac de Roda")
  (x 41.411085)
  (y 2.197428))

([onto_Class90520] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Selva de Mar")
  (x 41.41370999999999)
  (y 2.2009182))

([onto_Class90521] of  parada_bus

  (nombre "Bus linea H12 Gran Via-St Marti Provencals")
  (x 41.415806)
  (y 2.203737))

([onto_Class90522] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Maresme")
  (x 41.418658)
  (y 2.206966))

([onto_Class90524] of  parada_bus

  (nombre "Bus linea H12 Besos-Verneda")
  (x 41.422294)
  (y 2.210391))

([onto_Class90525] of  parada_bus

  (nombre "Bus linea H12 Besos-Verneda")
  (x 41.422294)
  (y 2.210391))

([onto_Class90526] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Maresme")
  (x 41.418658)
  (y 2.206966))

([onto_Class90527] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Cantabria")
  (x 32.9601727)
  (y -96.815236))

([onto_Class90528] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Pl dels Porxos")
  (x 41.413905)
  (y 2.200618))

([onto_Class90529] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Espronceda")
  (x 41.4116771)
  (y 2.1977152))

([onto_Class90532] of  parada_bus

  (nombre "Bus linea H12 Glories Catalanes, 8-15")
  (x 41.403747)
  (y 2.1865155))

([onto_Class90533] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Padilla")
  (x 41.400555)
  (y 2.1832187))

([onto_Class90534] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Sardenya")
  (x 41.398739)
  (y 2.180295000000001))

([onto_Class90535] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Roger de Flor")
  (x 41.39524)
  (y 2.177435))

([onto_Class90536] of  parada_bus

  (nombre "Bus linea H12 Metro Tetuan")
  (x 40.4605557)
  (y -3.6982513))

([onto_Class90537] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Lluria")
  (x 41.39187)
  (y 2.171675))

([onto_Class90538] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Pau Claris")
  (x 41.390946)
  (y 2.1693787))

([onto_Class90539] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Balmes")
  (x 41.387854)
  (y 2.165804))

([onto_Class90540] of  parada_bus

  (nombre "Bus linea H12 Pl Universitat")
  (x 41.3855048)
  (y 2.1640917))

([onto_Class90541] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Casanova")
  (x 41.38425)
  (y 2.160999))

([onto_Class90542] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Villarroel")
  (x 41.3833324)
  (y 2.1592347))

([onto_Class90543] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Calabria")
  (x 41.3801333)
  (y 2.1554887))

([onto_Class90544] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Vilamari")
  (x 41.37741700000001)
  (y 2.151919))

([onto_Class90545] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Pl. Espanya")
  (x 41.375671)
  (y 2.149598))

([onto_Class90546] of  parada_bus

  (nombre "Bus linea H12 Pl Espanya-FGC")
  (x 41.3745179)
  (y 2.1484676))

([onto_Class90547] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Farell")
  (x 41.371993)
  (y 2.14495))

([onto_Class90548] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Moianes")
  (x 41.3702405)
  (y 2.1425724))

([onto_Class90549] of  parada_bus

  (nombre "Bus linea H12 La Campana")
  (x 38.1039931)
  (y -121.2705447))

([onto_Class90550] of  parada_bus

  (nombre "Bus linea H12 Gran Via-Ildefons Cerda")
  (x 41.365781)
  (y 2.136445))

([onto_Class90551] of  parada_bus

  (nombre "Bus linea H12 Ildelfons Cerda-Ciutat de la Justicia")
  (x 41.363809)
  (y 2.133837))

([onto_Class90552] of  parada_bus

  (nombre "Bus linea H12 Av Granvia-Alhambra")
  (x 41.3619041)
  (y 2.1313021))

([onto_Class90553] of  parada_bus

  (nombre "Bus linea H12 Av Granvia-Natzaret")
  (x 41.3597788)
  (y 2.1284443))

([onto_Class90554] of  parada_bus

  (nombre "Bus linea H12 Pl Europa")
  (x 51.7736577)
  (y 19.4676453))

([onto_Class90555] of  parada_bus

  (nombre "Bus linea H12 Amadeu Torner")
  (x 41.3604404)
  (y 2.1225093))

([onto_Class90556] of  parada_bus

  (nombre "Bus linea H12 Av Granvia-Can Tries")
  (x 41.354971)
  (y 2.122077))

([onto_Class90557] of  parada_bus

  (nombre "Bus linea H12 Gornal")
  (x 18.0777287)
  (y 77.3697672))

([onto_Class90649] of  parada_bus

  (nombre "Bus linea H14 Paral·lel")
  (x 41.3749416)
  (y 2.1628722))

([onto_Class90650] of  parada_bus

  (nombre "Bus linea H14 Paral·lel-Drassanes")
  (x 41.376538)
  (y 2.175664))

([onto_Class90651] of  parada_bus

  (nombre "Bus linea H14 Les Drassanes")
  (x 41.3752357)
  (y 2.1759475))

([onto_Class90652] of  parada_bus

  (nombre "Bus linea H14 Portal de la Pau")
  (x 41.3762045)
  (y 2.1770625))

([onto_Class90653] of  parada_bus

  (nombre "Bus linea H14 Moll de la Fusta")
  (x 41.3806835)
  (y 2.1820856))

([onto_Class90654] of  parada_bus

  (nombre "Bus linea H14 Pg. Colom-Via Laietana")
  (x 41.3805142)
  (y 2.1816157))

([onto_Class90655] of  parada_bus

  (nombre "Bus linea H14 Pg. Isabel II-Pla de Palau")
  (x 41.38268)
  (y 2.183672))

([onto_Class90656] of  parada_bus

  (nombre "Bus linea H14 Estacio de Franca")
  (x 41.3839642)
  (y 2.1864909))

([onto_Class90657] of  parada_bus

  (nombre "Bus linea H14 Parc de la Ciutadella-Princesa")
  (x 41.388123)
  (y 2.1860152))

([onto_Class90658] of  parada_bus

  (nombre "Bus linea H14 Pg Pujades-Parc de la Ciutadella")
  (x 41.388123)
  (y 2.1860152))

([onto_Class90659] of  parada_bus

  (nombre "Bus linea H14 Buenaventura Muñoz-Sardenya")
  (x 41.3935852)
  (y 2.187546))

([onto_Class90660] of  parada_bus

  (nombre "Bus linea H14 Pallars-Joan d'austria")
  (x 41.4161483)
  (y 2.1991142))

([onto_Class90661] of  parada_bus

  (nombre "Bus linea H14 Pallars-Pere IV")
  (x 41.3987082)
  (y 2.194629))

([onto_Class90662] of  parada_bus

  (nombre "Bus linea H14 Ciutat de Granada-Pallars")
  (x 41.4003365)
  (y 2.1968081))

([onto_Class90663] of  parada_bus

  (nombre "Bus linea H14 Llull-Llacuna")
  (x 41.400066)
  (y 2.20102))

([onto_Class90664] of  parada_bus

  (nombre "Bus linea H14 Llull-Bilbao")
  (x 41.401985)
  (y 2.203556))

([onto_Class90665] of  parada_bus

  (nombre "Bus linea H14 Llull-Jardins Gandhi")
  (x 41.40446499999999)
  (y 2.206774))

([onto_Class90666] of  parada_bus

  (nombre "Bus linea H14 Llull-Selva de Mar")
  (x 41.408127)
  (y 2.209802))

([onto_Class90667] of  parada_bus

  (nombre "Bus linea H14 Llull-Diagonal Mar")
  (x 41.40914799999999)
  (y 2.213034))

([onto_Class90668] of  parada_bus

  (nombre "Bus linea H14 Metro Maresme/Forum")
  (x 41.4121856)
  (y 2.217561))

([onto_Class90669] of  parada_bus

  (nombre "Bus linea H14 Sant Ramon de Penyafort-Manuel Fernandez Marquez")
  (x 41.415813)
  (y 2.219383))

([onto_Class90670] of  parada_bus

  (nombre "Bus linea H14 Cristobal de Moura-Parc del Besos")
  (x 41.42043)
  (y 2.218757))

([onto_Class90671] of  parada_bus

  (nombre "Bus linea H14 Cristobal de Moura-La Catalana")
  (x 41.4226684)
  (y 2.2217424))

([onto_Class90672] of  parada_bus

  (nombre "Bus linea H14 Torrassa-Mare de Deu del Carme")
  (x 41.42633240000001)
  (y 2.2252364))

([onto_Class90673] of  parada_bus

  (nombre "Bus linea H14 Av. de la Platja-Estacio Sant Adria")
  (x 41.423953)
  (y 2.23055))

([onto_Class90674] of  parada_bus

  (nombre "Bus linea H14 Av. de la Platja-Pont d'Eduard Maristany")
  (x 41.425232)
  (y 2.230018))

([onto_Class90675] of  parada_bus

  (nombre "Bus linea H14Av. de la Platja-Pont d'Eduard Maristany")
  (x 41.425232)
  (y 2.230018))

([onto_Class90676] of  parada_bus

  (nombre "Bus linea H14 Torrassa-Mare de Deu del Carme")
  (x 41.42633240000001)
  (y 2.2252364))

([onto_Class90677] of  parada_bus

  (nombre "Bus linea H14 Cristobal de Moura-La Catalana")
  (x 41.4226684)
  (y 2.2217424))

([onto_Class90678] of  parada_bus

  (nombre "Bus linea H14 Cristobal de Moura-Parc del Besos")
  (x 41.42043)
  (y 2.218757))

([onto_Class90679] of  parada_bus

  (nombre "Bus linea H14 Sant Ramon de Penyafort-Ferrer i Bassa")
  (x 41.41835)
  (y 2.217301))

([onto_Class90680] of  parada_bus

  (nombre "Bus linea H14 St. Ramon de Penyafort-Manuel Fernandez Maquez")
  (x 41.415012)
  (y 2.219692))

([onto_Class90681] of  parada_bus

  (nombre "Bus linea H14 Metro Maresme/Forum")
  (x 41.4121856)
  (y 2.217561))

([onto_Class90682] of  parada_bus

  (nombre "Bus linea H14 Av Diagonal-Josep Pla")
  (x 41.4100449)
  (y 2.2129318))

([onto_Class90683] of  parada_bus

  (nombre "Bus linea H14 Av Diagonal-Selva de Mar")
  (x 41.4091858)
  (y 2.2095659))

([onto_Class90684] of  parada_bus

  (nombre "Bus linea H14 Pujades-Provencals")
  (x 41.407749)
  (y 2.208708))

([onto_Class90685] of  parada_bus

  (nombre "Bus linea H14 Pujades-Espronceda")
  (x 41.405273)
  (y 2.2052686))

([onto_Class90686] of  parada_bus

  (nombre "Bus linea H14 Pujades-Bilbao")
  (x 41.403014)
  (y 2.20236))

([onto_Class90687] of  parada_bus

  (nombre "Bus linea H14 Llacuna-Pallars")
  (x 41.401318)
  (y 2.198833))

([onto_Class90688] of  parada_bus

  (nombre "Bus linea H14 Almogavers-Ciutat de Granada")
  (x 41.3986092)
  (y 2.1975823))

([onto_Class90689] of  parada_bus

  (nombre "Bus linea H14 Almogavers-alaba")
  (x 41.39862249999999)
  (y 2.1919343))

([onto_Class90691] of  parada_bus

  (nombre "Bus linea H14 Av. Meridiana-Almogavers")
  (x 41.394137)
  (y 2.186762))

([onto_Class90692] of  parada_bus

  (nombre "Bus linea H14 Pg. Pujades-Parc de la Ciutadella")
  (x 41.388123)
  (y 2.1860152))

([onto_Class90694] of  parada_bus

  (nombre "Bus linea H14 Pla de Palau-Av. Marques de l'Argentera")
  (x 41.383472)
  (y 2.183816))

([onto_Class90695] of  parada_bus

  (nombre "Bus linea H14 Pg. Colom-Via Laietana")
  (x 41.3805142)
  (y 2.1816157))

([onto_Class90696] of  parada_bus

  (nombre "Bus linea H14 Moll de la Fusta")
  (x 41.3806835)
  (y 2.1820856))

([onto_Class90697] of  parada_bus

  (nombre "Bus linea H14 Portal de la Pau")
  (x 41.3762045)
  (y 2.1770625))

([onto_Class90698] of  parada_bus

  (nombre "Bus linea H14 Les Drassanes")
  (x 41.3752357)
  (y 2.1759475))

([onto_Class90699] of  parada_bus

  (nombre "Bus linea H14 Paral·lel-Drassanes")
  (x 41.376538)
  (y 2.175664))

([onto_Class90700] of  parada_bus

  (nombre "Bus linea H14 Metro Paral·lel")
  (x 41.3748271)
  (y 2.1700247))

([onto_Class90701] of  parada_bus

  (nombre "Bus linea H14 Aldana, 5-11")
  (x 19.4715469)
  (y -99.1624198))

([onto_Class90702] of  parada_bus

  (nombre "Bus linea H14 Paral·lel")
  (x 41.3749416)
  (y 2.1628722))

([onto_Class90703] of  parada_bus

  (nombre "Bus linea V15 Pl. Rosa dels Vents")
  (x 41.368316)
  (y 2.18779))

([onto_Class90704] of  parada_bus

  (nombre "Bus linea V15 Pg. Joan de Borbo-Moll de Catalunya")
  (x 41.370684)
  (y 2.187572))

([onto_Class90705] of  parada_bus

  (nombre "Bus linea V15 Pg Joan de Borbo-Pl del Mar")
  (x 41.374332)
  (y 2.188567))

([onto_Class90706] of  parada_bus

  (nombre "Bus linea V15 Pg Joan de Borbo-Judici")
  (x 41.376099)
  (y 2.188942))

([onto_Class90707] of  parada_bus

  (nombre "Bus linea V15 Pg. Joan de Borbo-Almirall Aixada")
  (x 41.377649)
  (y 2.188362))

([onto_Class90708] of  parada_bus

  (nombre "Bus linea V15 Pg. Joan de Borbo")
  (x 41.38003)
  (y 2.187419))

([onto_Class90709] of  parada_bus

  (nombre "Bus linea V15 Pla de Palau-Pl. Pau Vila")
  (x 41.3825493)
  (y 2.185005))

([onto_Class90710] of  parada_bus

  (nombre "Bus linea V15 Via Laietana-Jutjats")
  (x 41.382862)
  (y 2.1804025))

([onto_Class90711] of  parada_bus

  (nombre "Bus linea V15 Via Laietana-Pl. Ramon Berenguer")
  (x 41.3850053)
  (y 2.1773116))

([onto_Class90712] of  parada_bus

  (nombre "Bus linea V15 Via Laietana-Carrer Comtal")
  (x 41.387104)
  (y 2.1748235))

([onto_Class90713] of  parada_bus

  (nombre "Bus linea V15 Metro Urquinaona")
  (x 41.3887867)
  (y 2.172799))

([onto_Class90714] of  parada_bus

  (nombre "Bus linea V15 Pg de Gracia-Casp")
  (x 41.38895)
  (y 2.1692863))

([onto_Class90715] of  parada_bus

  (nombre "Bus linea V15 Pg de Gracia-Gran Via")
  (x 41.3901396)
  (y 2.1696699))

([onto_Class90716] of  parada_bus

  (nombre "Bus linea V15 Pg de Gracia-Arago")
  (x 41.3918021)
  (y 2.1655205))

([onto_Class90717] of  parada_bus

  (nombre "Bus linea V15 Pg de Gracia-Mallorca")
  (x 41.393562)
  (y 2.1633904))

([onto_Class90718] of  parada_bus

  (nombre "Bus linea V15 Pg de Gracia-Av Diagonal")
  (x 41.3931702)
  (y 2.1639602))

([onto_Class90719] of  parada_bus

  (nombre "Bus linea V15 Via Augusta-Av. Diagonal")
  (x 41.3977882)
  (y 2.1355655))

([onto_Class90720] of  parada_bus

  (nombre "Bus linea V15 Pl. Gal·la Placidia")
  (x 41.399673)
  (y 2.1522584))

([onto_Class90723] of  parada_bus

  (nombre "Bus linea V15 Balmes-Mitre")
  (x 41.40453249999999)
  (y 2.1406045))

([onto_Class90724] of  parada_bus

  (nombre "Bus linea V15 Balmes-Joaquim Folguera")
  (x 41.40678399999999)
  (y 2.138735))

([onto_Class90725] of  parada_bus

  (nombre "Bus linea V15 Balmes-Pl. John F. Kennedy")
  (x 41.409492)
  (y 2.1374025))

([onto_Class90726] of  parada_bus

  (nombre "Bus linea V15 Pg. Sant Gervasi-Mas Yebra")
  (x 41.412144)
  (y 2.1382291))

([onto_Class90727] of  parada_bus

  (nombre "Bus linea V15 Pl Alfonso Comin-Av Republica Argentina")
  (x 41.414496)
  (y 2.138262))

([onto_Class90728] of  parada_bus

  (nombre "Bus linea V15 Pl. Alfonso Comin-Pg. Vall d'Hebron")
  (x 41.41625)
  (y 2.1382585))

([onto_Class90729] of  parada_bus

  (nombre "Bus linea V15 Pg. Vall d'Hebron-Av. de Vallcarca")
  (x 41.419343)
  (y 2.139821))

([onto_Class90730] of  parada_bus

  (nombre "Bus linea V15 Pg Vall d'Hebron-Trueba")
  (x 41.423414)
  (y 2.141806))

([onto_Class90731] of  parada_bus

  (nombre "Bus linea V15 Pg Vall d'Hebron-Trueba   ")
  (x 41.423414)
  (y 2.141806))

([onto_Class90732] of  parada_bus

  (nombre "Bus linea V15 Coll i Alentorn-Basses d'Horta")
  (x 41.42438200000001)
  (y 2.143261))

([onto_Class90733] of  parada_bus

  (nombre "Bus linea V15 Av Marti Codolar-Coll i Alentorn")
  (x 41.424709)
  (y 2.14784))

([onto_Class90734] of  parada_bus

  (nombre "Bus linea V15 Granja Vella-Av Marti i Codolar")
  (x 41.426859)
  (y 2.147947))

([onto_Class90736] of  parada_bus

  (nombre "Bus linea V15 Ciutat Sanitaria de la Vall d'Hebron")
  (x 41.426892)
  (y 2.142992))

([onto_Class90737] of  parada_bus

  (nombre "Bus linea V15 Pg. Vall d'Hebron-Av. Jorda")
  (x 41.424328)
  (y 2.14172))

([onto_Class90738] of  parada_bus

  (nombre "Bus linea V15 Pg. Vall d'Hebron-Natzaret")
  (x 41.42157599999999)
  (y 2.140501))

([onto_Class90739] of  parada_bus

  (nombre "Bus linea V15 Pg. Vall d'Hebron-L'Arrabassada")
  (x 41.418623)
  (y 2.139062))

([onto_Class90740] of  parada_bus

  (nombre "Bus linea V15 Pl. Alfonso Comin-Isaac Newton")
  (x 41.4151914)
  (y 2.1361746))

([onto_Class90741] of  parada_bus

  (nombre "Bus linea V15 Av Republica Argentina-Pl Alfonso Comin")
  (x 41.414496)
  (y 2.138262))

([onto_Class90742] of  parada_bus

  (nombre "Bus linea V15 Craywinckel-Hurtado")
  (x 41.411747)
  (y 2.138929))

([onto_Class90743] of  parada_bus

  (nombre "Bus linea V15 Balmes-Pl. John F. Kennedy")
  (x 41.409492)
  (y 2.1374025))

([onto_Class90744] of  parada_bus

  (nombre "Bus linea V15 Balmes-Pl Joaquim Folguera")
  (x 41.40544)
  (y 2.139226))

([onto_Class90745] of  parada_bus

  (nombre "Bus linea V15 Balmes-Mitre")
  (x 41.40453249999999)
  (y 2.1406045))

([onto_Class90746] of  parada_bus

  (nombre "Bus linea V15 Balmes-Copernic")
  (x 41.40256670000001)
  (y 2.1450169))

([onto_Class90747] of  parada_bus

  (nombre "Bus linea V15 Balmes-Via Augusta")
  (x 41.4008759)
  (y 2.1479603))

([onto_Class90748] of  parada_bus

  (nombre "Bus linea V15 Balmes-Travessera de Gracia")
  (x 41.398323)
  (y 2.1511636))

([onto_Class90749] of  parada_bus

  (nombre "Bus linea V15 Balmes-Av. Diagonal")
  (x 41.3950474)
  (y 2.1546307))

([onto_Class90750] of  parada_bus

  (nombre "Bus linea V15 Balmes-Rossello")
  (x 41.3930772)
  (y 2.1574419))

([onto_Class90751] of  parada_bus

  (nombre "Bus linea V15 Balmes-Valencia")
  (x 41.3911734)
  (y 2.160118))

([onto_Class90752] of  parada_bus

  (nombre "Bus linea V15 Balmes-Arago")
  (x 41.38951429999999)
  (y 2.1623085))

([onto_Class90753] of  parada_bus

  (nombre "Bus linea V15 Balmes-Gran Via")
  (x 41.38761299999999)
  (y 2.164736))

([onto_Class90756] of  parada_bus

  (nombre "Bus linea V15 Via Laietana-Pl Ramon Berenguer")
  (x 41.3850053)
  (y 2.1773116))

([onto_Class90757] of  parada_bus

  (nombre "Bus linea V15 Via Laietana-Correus")
  (x 41.3820787)
  (y 2.1804146))

([onto_Class90758] of  parada_bus

  (nombre "Bus linea V15 Pas Sota Muralla-Pl. Pau Vila")
  (x 41.3812064)
  (y 2.1844683))

([onto_Class90759] of  parada_bus

  (nombre "Bus linea V15 Pg Joan de Borbo")
  (x 41.38003)
  (y 2.187419))

([onto_Class90760] of  parada_bus

  (nombre "Bus linea V15 Pg Joan de Borbo-Almirall Aixada")
  (x 41.377649)
  (y 2.188362))

([onto_Class90761] of  parada_bus

  (nombre "Bus linea V15 Pg. Joan de Borbo-Pl. del Mar")
  (x 41.374332)
  (y 2.188567))

([onto_Class90762] of  parada_bus

  (nombre "Bus linea V15 Pg Joan de Borbo-Moll de Catalunya")
  (x 41.370684)
  (y 2.187572))

([onto_Class90763] of  parada_bus

  (nombre "Bus linea V15 Pl. Rosa dels Vents")
  (x 41.368316)
  (y 2.18779))

([onto_Class90801] of  parada_bus

  (nombre "Bus linea H16Pl del Nou")
  (x 21.3629302)
  (y -158.0826558))

([onto_Class90802] of  parada_bus

  (nombre "Bus linea H16 Pg. Zona Franca-Cisell")
  (x 41.354813)
  (y 2.1432056))

([onto_Class90803] of  parada_bus

  (nombre "Bus linea H16 Pg. Zona Franca-Alts Forns")
  (x 41.358062)
  (y 2.14069))

([onto_Class90804] of  parada_bus

  (nombre "Bus linea H16 Pg. Zona Franca-Foneria")
  (x 41.3601132)
  (y 2.138971))

([onto_Class90805] of  parada_bus

  (nombre "Bus linea H16 Pg. Zona Franca-Mineria")
  (x 41.362303)
  (y 2.13764))

([onto_Class90806] of  parada_bus

  (nombre "Bus linea H16 Gran Via-Quimica")
  (x 41.36590349999999)
  (y 2.1373494))

([onto_Class90807] of  parada_bus

  (nombre "Bus linea H16 La Campana")
  (x 38.1039931)
  (y -121.2705447))

([onto_Class90808] of  parada_bus

  (nombre "Bus linea H16 Gran Via-Mandoni")
  (x 41.36955)
  (y 2.141984))

([onto_Class90809] of  parada_bus

  (nombre "Bus linea H16 Gran Via-Santa Dorotea")
  (x 41.3718002)
  (y 2.1449802))

([onto_Class90810] of  parada_bus

  (nombre "Bus linea H16 Pl Espanya-FGC")
  (x 41.3745179)
  (y 2.1484676))

([onto_Class90811] of  parada_bus

  (nombre "Bus linea H16 Gran Via-Llanca")
  (x 41.376261)
  (y 2.151148))

([onto_Class90812] of  parada_bus

  (nombre "Bus linea H16 Floridablanca-Av. Mistral")
  (x 41.376577)
  (y 2.156056))

([onto_Class90813] of  parada_bus

  (nombre "Bus linea H16 Floridablanca-Viladomat")
  (x 41.37817099999999)
  (y 2.158158))

([onto_Class90814] of  parada_bus

  (nombre "Bus linea H16 Floridablanca-Comte d'Urgell")
  (x 41.3806889)
  (y 2.1614885))

([onto_Class90815] of  parada_bus

  (nombre "Bus linea H16 Ronda Sant Antoni-Pl Goya")
  (x 41.3830846)
  (y 2.1637946))

([onto_Class90816] of  parada_bus

  (nombre "Bus linea H16 Pl Universitat-Pelai")
  (x 41.3856684)
  (y 2.164474))

([onto_Class90817] of  parada_bus

  (nombre "Bus linea H16 Pl. Catalunya-Portal de l'angel")
  (x 41.386592)
  (y 2.171069))

([onto_Class90818] of  parada_bus

  (nombre "Bus linea H16 Pl. Urquinaona")
  (x 41.3893381)
  (y 2.1729284))

([onto_Class90819] of  parada_bus

  (nombre "Bus linea H16 Trafalgar-Bruc")
  (x 41.389715)
  (y 2.177001))

([onto_Class90820] of  parada_bus

  (nombre "Bus linea H16 Pg. Lluis Companys-Arc de Triomf")
  (x 41.3910524)
  (y 2.1806449))

([onto_Class90821] of  parada_bus

  (nombre "Bus linea H16 Pg. Lluis Companys-Jutjats")
  (x 41.388887)
  (y 2.1827453))

([onto_Class90822] of  parada_bus

  (nombre "Bus linea H16 Pg Pujades-Parc de la Ciutadella")
  (x 41.388123)
  (y 2.1860152))

([onto_Class90823] of  parada_bus

  (nombre "Bus linea H16 Sardenya-Pg. Pujades")
  (x 41.391813)
  (y 2.188536))

([onto_Class90824] of  parada_bus

  (nombre "Bus linea H16 Ramon Trias Fargas-Doctor Trueta")
  (x 41.390161)
  (y 2.190742))

([onto_Class90825] of  parada_bus

  (nombre "Bus linea H16 Icaria-Parc Carles I")
  (x 41.3884854)
  (y 2.1934892))

([onto_Class90826] of  parada_bus

  (nombre "Bus linea H16 Av Icaria-Joan Miro")
  (x 41.390328)
  (y 2.195773))

([onto_Class90827] of  parada_bus

  (nombre "Bus linea H16 Av. Icaria-alaba")
  (x 41.393104)
  (y 2.198985))

([onto_Class90828] of  parada_bus

  (nombre "Bus linea H16 Cementiri de l'Est")
  (x 41.3958991)
  (y 2.2030233))

([onto_Class90829] of  parada_bus

  (nombre "Bus linea H16 Pg Calvell-Rambla del Poblenou")
  (x 41.397033)
  (y 2.205785))

([onto_Class90830] of  parada_bus

  (nombre "Bus linea H16 Jonquera-Taulat")
  (x 41.4000089)
  (y 2.2075558))

([onto_Class90831] of  parada_bus

  (nombre "Bus linea H16 Pg. Taulat-Bac de Roda")
  (x 41.402743)
  (y 2.209682))

([onto_Class90832] of  parada_bus

  (nombre "Bus linea H16 Pg. Taulat-Provencals")
  (x 41.404938)
  (y 2.212377))

([onto_Class90833] of  parada_bus

  (nombre "Bus linea H16 Pg. Taulat-Diagonal Mar")
  (x 41.406876)
  (y 2.21542))

([onto_Class90835] of  parada_bus

  (nombre "Bus linea H16 Forum")
  (x 36.1817412)
  (y -93.7149141))

([onto_Class90836] of  parada_bus

  (nombre "Bus linea H16Forum")
  (x 40.2968979)
  (y -111.6946475))

([onto_Class90837] of  parada_bus

  (nombre "Bus linea H16 Diagonal Mar")
  (x 41.40996579999999)
  (y 2.2166369))

([onto_Class90838] of  parada_bus

  (nombre "Bus linea H16 Pg. Taulat-Diagonal Mar")
  (x 41.406876)
  (y 2.21542))

([onto_Class90839] of  parada_bus

  (nombre "Bus linea H16 Pg. Taulat-Provencals")
  (x 41.404938)
  (y 2.212377))

([onto_Class90840] of  parada_bus

  (nombre "Bus linea H16 Pg. Taulat-Bac de Roda")
  (x 41.402743)
  (y 2.209682))

([onto_Class90841] of  parada_bus

  (nombre "Bus linea H16 Jonquera-Pg. Taulat")
  (x 41.399851)
  (y 2.207531))

([onto_Class90842] of  parada_bus

  (nombre "Bus linea H16 Pg Calvell-Rambla del Poblenou")
  (x 41.397033)
  (y 2.205785))

([onto_Class90843] of  parada_bus

  (nombre "Bus linea H16 Cementiri de l'Est")
  (x 41.3958991)
  (y 2.2030233))

([onto_Class90844] of  parada_bus

  (nombre "Bus linea H16 Av. Icaria-alaba")
  (x 41.393104)
  (y 2.198985))

([onto_Class90845] of  parada_bus

  (nombre "Bus linea H16 Av Icaria-Joan Miro")
  (x 41.390328)
  (y 2.195773))

([onto_Class90846] of  parada_bus

  (nombre "Bus linea H16 Marina-Doctor Trueta")
  (x 41.390955)
  (y 2.1922549))

([onto_Class90848] of  parada_bus

  (nombre "Bus linea H16 Pg. Pujades-Parc de la Ciutadella")
  (x 41.388123)
  (y 2.1860152))

([onto_Class90849] of  parada_bus

  (nombre "Bus linea H16 Pg. Lluis Companys-Parc de la Ciutadella")
  (x 41.388123)
  (y 2.1860152))

([onto_Class90850] of  parada_bus

  (nombre "Bus linea H16 Pg. Lluis Companys-Almogavers")
  (x 41.391189)
  (y 2.181295))

([onto_Class90851] of  parada_bus

  (nombre "Bus linea H16 Arc de Triomf")
  (x 41.3910524)
  (y 2.1806449))

([onto_Class90852] of  parada_bus

  (nombre "Bus linea H16 Ronda Sant Pere-Girona")
  (x 41.9897244)
  (y 2.8255746))

([onto_Class90853] of  parada_bus

  (nombre "Bus linea H16 Metro Urquinaona")
  (x 41.3887867)
  (y 2.172799))

([onto_Class90854] of  parada_bus

  (nombre "Bus linea H16 Pl. Catalunya-Pg. de Gracia")
  (x 41.387757)
  (y 2.169316))

([onto_Class90855] of  parada_bus

  (nombre "Bus linea H16 Ronda Universitat-Pl Universitat")
  (x 41.386274)
  (y 2.165311))

([onto_Class90856] of  parada_bus

  (nombre "Bus linea H16 Sepulveda-Muntaner")
  (x 41.383412)
  (y 2.162684))

([onto_Class90857] of  parada_bus

  (nombre "Bus linea H16 Sepulveda-Comte d'Urgell")
  (x 41.3809105)
  (y 2.1593745))

([onto_Class90858] of  parada_bus

  (nombre "Bus linea H16 Sepulveda-Viladomat")
  (x 41.379066)
  (y 2.156891))

([onto_Class90859] of  parada_bus

  (nombre "Bus linea H16 Sepulveda-Rocafort")
  (x 41.3774088)
  (y 2.1547217))

([onto_Class90860] of  parada_bus

  (nombre "Bus linea H16 Sepulveda-Av. Mistral")
  (x 41.375683)
  (y 2.152409))

([onto_Class90861] of  parada_bus

  (nombre "Bus linea H16 Pl Espanya-FGC")
  (x 41.3745179)
  (y 2.1484676))

([onto_Class90862] of  parada_bus

  (nombre "Bus linea H16 Gran Via-Farell")
  (x 41.371993)
  (y 2.14495))

([onto_Class90863] of  parada_bus

  (nombre "Bus linea H16 Gran Via-Moianes")
  (x 41.3702405)
  (y 2.1425724))

([onto_Class90864] of  parada_bus

  (nombre "Bus linea H16 La Campana")
  (x 38.1039931)
  (y -121.2705447))

([onto_Class90865] of  parada_bus

  (nombre "Bus linea H16 Gran Via-Ildefons Cerda")
  (x 41.365781)
  (y 2.136445))

([onto_Class90866] of  parada_bus

  (nombre "Bus linea H16 Pg. Zona Franca-Ctra del Prat")
  (x 41.36206079999999)
  (y 2.1375426))

([onto_Class90867] of  parada_bus

  (nombre "Bus linea H16 Pg. Zona Franca-Foneria")
  (x 41.3601132)
  (y 2.138971))

([onto_Class90868] of  parada_bus

  (nombre "Bus linea H16 Pg. Zona Franca-Foc")
  (x 41.356714)
  (y 2.14134))

([onto_Class90869] of  parada_bus

  (nombre "Bus linea H16 Pg. Zona Franca-Cisell")
  (x 41.354813)
  (y 2.1432056))

([onto_Class90870] of  parada_bus

  (nombre "Bus linea H16 Pl del Nou")
  (x 21.3629302)
  (y -158.0826558))

([onto_Class90935] of  parada_bus

  (nombre "Bus linea V17 Maremagnum")
  (x 41.375182)
  (y 2.182867))

([onto_Class90936] of  parada_bus

  (nombre "Bus linea V17 Port Vell")
  (x 41.3751254)
  (y 2.1808292))

([onto_Class90937] of  parada_bus

  (nombre "Bus linea V17 Pas Sota Muralla-Pg. Colom   ( Barceloneta)")
  (x 41.381145)
  (y 2.182582))

([onto_Class90939] of  parada_bus

  (nombre "Bus linea V17 Via Laietana, 32-34  (Jaume I)")
  (x 41.3833363)
  (y 2.1778807))

([onto_Class90940] of  parada_bus

  (nombre "Bus linea V17 Via Laietana-Comtal  Urquinaona")
  (x 41.3887867)
  (y 2.172799))

([onto_Class90941] of  parada_bus

  (nombre "Bus linea V17 Lluria-Urquinaona    (Urquinaona)")
  (x 41.390187)
  (y 2.172837))

([onto_Class90942] of  parada_bus

  (nombre "Bus linea V17 Lluria-Gran Via  ")
  (x 41.392009)
  (y 2.170423))

([onto_Class90943] of  parada_bus

  (nombre "Bus linea V17 Lluria-Arago (  Pg. de Gracia), (Girona) ")
  (x 41.3922472)
  (y 2.1650446))

([onto_Class90944] of  parada_bus

  (nombre "Bus linea V17 Lluria-Mallorca ")
  (x 41.395576)
  (y 2.165706))

([onto_Class90945] of  parada_bus

  (nombre "Bus linea V17 Corsega-Pg. de Gracia (Diagonal)")
  (x 41.397099)
  (y 2.160005))

([onto_Class90946] of  parada_bus

  (nombre "Bus linea V17 Gran de Gracia-Jesus")
  (x 41.398356)
  (y 2.157307))

([onto_Class90947] of  parada_bus

  (nombre "Bus linea V17 Gran de Gracia-Travessera de Gracia  (Gracia)")
  (x 41.4015795)
  (y 2.1534483))

([onto_Class90948] of  parada_bus

  (nombre "Bus linea V17 Gran de Gracia-Metro Fontana (Fontana)")
  (x 41.40180900000001)
  (y 2.153274))

([onto_Class90949] of  parada_bus

  (nombre "Bus linea V17 Gran de Gracia (Fontana)")
  (x 41.4032771)
  (y 2.1524681))

([onto_Class90950] of  parada_bus

  (nombre "Bus linea V17 Gran de Gracia-Lesseps  (Lesseps)")
  (x 41.4053806)
  (y 2.1506861))

([onto_Class90951] of  parada_bus

  (nombre "Bus linea V17 Bolivar-Ballester")
  (x 41.409254)
  (y 2.147712))

([onto_Class90952] of  parada_bus

  (nombre "Bus linea V17 Av. Republica Argentina-Vallcarca (Vallcarca)")
  (x 41.411415)
  (y 2.144566))

([onto_Class90954] of  parada_bus

  (nombre "Bus linea V17 Mare de Deu del Coll-Cartago")
  (x 41.415551)
  (y 2.146632))

([onto_Class90955] of  parada_bus

  (nombre "Bus linea V17 Mare de Deu del Coll-Baixada Sant Maria")
  (x 41.4178734)
  (y 2.1477724))

([onto_Class90956] of  parada_bus

  (nombre "Bus linea V17 Mare de Deu del Coll-Santuari (La Teixonera)")
  (x 41.4200541)
  (y 2.1513745))

([onto_Class90957] of  parada_bus

  (nombre "Bus linea V17 Pl. Gibraltar")
  (x 34.4738248)
  (y -118.6467728))

([onto_Class90958] of  parada_bus

  (nombre "Bus linea V17 Santuari-La Murtra")
  (x 41.419847)
  (y 2.155033))

([onto_Class90959] of  parada_bus

  (nombre "Bus linea V17 Gran Vista-Doctor Bove")
  (x 41.419353)
  (y 2.158382))

([onto_Class90960] of  parada_bus

  (nombre "Bus linea V17 IES Ferran Tallada")
  (x 41.420565)
  (y 2.163426))

([onto_Class90961] of  parada_bus

  (nombre "Bus linea V17 Gran Vista-Turo de la Rovira")
  (x 41.420212)
  (y 2.164008))

([onto_Class90962] of  parada_bus

  (nombre "Bus linea V17 Gran Vista-Pl. de la Mitja Lluna")
  (x 41.420126)
  (y 2.164919))

([onto_Class90963] of  parada_bus

  (nombre "Bus linea V17 Gran Vista-Pl. de la Mitja Lluna")
  (x 41.420126)
  (y 2.164919))

([onto_Class90964] of  parada_bus

  (nombre "Bus linea V17 IES Ferran Tallada")
  (x 41.420565)
  (y 2.163426))

([onto_Class90965] of  parada_bus

  (nombre "Bus linea V17 Gran Vista-Doctor Bove")
  (x 41.419353)
  (y 2.158382))

([onto_Class90966] of  parada_bus

  (nombre "Bus linea V17 Santuari-La Murtra")
  (x 41.419847)
  (y 2.155033))

([onto_Class90967] of  parada_bus

  (nombre "Bus linea V17 Pl Gibraltar")
  (x 34.4738248)
  (y -118.6467728))

([onto_Class90968] of  parada_bus

  (nombre "Bus linea V17 Mare de Deu del Coll-Santuari (La Teixonera)")
  (x 41.4200541)
  (y 2.1513745))

([onto_Class90969] of  parada_bus

  (nombre "Bus linea V17 Mare de Deu del Coll-Baixada Solanell")
  (x 41.4176444)
  (y 2.1475863))

([onto_Class90970] of  parada_bus

  (nombre "Bus linea V17 Mare de Deu del Coll-Pl. Flandes")
  (x 41.415437)
  (y 2.146197))

([onto_Class90971] of  parada_bus

  (nombre "Bus linea V17 Vallcarca ( Vallcarca)")
  (x 41.415138)
  (y 2.1413415))

([onto_Class90972] of  parada_bus

  (nombre "Bus linea V17 Av Republica Argentina-Vallcarca (Vallcarca)")
  (x 41.411415)
  (y 2.144566))

([onto_Class90973] of  parada_bus

  (nombre "Bus linea V17 Av Republica Argentina-Escipio")
  (x 41.4086433)
  (y 2.1470614))

([onto_Class90974] of  parada_bus

  (nombre "Bus linea V17 Lesseps-Av. Princep d'Asturies   ")
  (x 41.4061648)
  (y 2.1498093))

([onto_Class90975] of  parada_bus

  (nombre "Bus linea V17 Av Princep d'Asturies-Guillem Tell")
  (x 41.402807)
  (y 2.15003))

([onto_Class90976] of  parada_bus

  (nombre "Bus linea V17 Gal·la Placidia ")
  (x 41.39943)
  (y 2.15304))

([onto_Class90978] of  parada_bus

  (nombre "Bus linea V17 Diagonal-Pau Claris   ")
  (x 41.396737)
  (y 2.160649))

([onto_Class90979] of  parada_bus

  (nombre "Bus linea V17 Pau Claris-Mallorca ")
  (x 41.394548)
  (y 2.164609))

([onto_Class90982] of  parada_bus

  (nombre "Bus linea V17 Urquinaona    ")
  (x 41.3893381)
  (y 2.1729284))

([onto_Class90983] of  parada_bus

  (nombre "Bus linea V17 Via Laietana-Comtal   ")
  (x 41.387104)
  (y 2.1748235))

([onto_Class90984] of  parada_bus

  (nombre "Bus linea V17 Via Laietana-Pl Ramon Berenguer ")
  (x 41.3850053)
  (y 2.1773116))

([onto_Class90985] of  parada_bus

  (nombre "Bus linea V17 Via Laietana-Correus")
  (x 41.3820787)
  (y 2.1804146))

([onto_Class90986] of  parada_bus

  (nombre "Bus linea V17 Pas Sota Muralla-Via Laietana   ")
  (x 41.38087)
  (y 2.182746))

([onto_Class90987] of  parada_bus

  (nombre "Bus linea V17 Port Vell")
  (x 41.3751254)
  (y 2.1808292))

([onto_Class90988] of  parada_bus

  (nombre "Bus linea V17 Maremagnum")
  (x 41.375182)
  (y 2.182867))

([onto_Class91118] of  parada_bus

  (nombre "Bus linea D20 Trelawny-Av. Litoral")
  (x 41.38625800000001)
  (y 2.194843))

([onto_Class91120] of  parada_bus

  (nombre "Bus linea D20 Platja de la Barceloneta")
  (x 41.3783713)
  (y 2.1924685))

([onto_Class91121] of  parada_bus

  (nombre "Bus linea D20 Pg Maritim-Almirall Cervera")
  (x 41.379075)
  (y 2.192087))

([onto_Class91122] of  parada_bus

  (nombre "Bus linea D20 Almirall Cervera")
  (x 41.3784345)
  (y 2.1898999))

([onto_Class91123] of  parada_bus

  (nombre "Bus linea D20 Pg. Joan de Borbo")
  (x 41.38003)
  (y 2.187419))

([onto_Class91124] of  parada_bus

  (nombre "Bus linea D20 Pla de Palau-Pl. Pau Vila")
  (x 41.3825493)
  (y 2.185005))

([onto_Class91125] of  parada_bus

  (nombre "Bus linea D20 Pg. Colom-Via Laietana")
  (x 41.3805142)
  (y 2.1816157))

([onto_Class91126] of  parada_bus

  (nombre "Bus linea D20 Moll de la Fusta")
  (x 41.3806835)
  (y 2.1820856))

([onto_Class91128] of  parada_bus

  (nombre "Bus linea D20 Les Drassanes")
  (x 41.3752357)
  (y 2.1759475))

([onto_Class91129] of  parada_bus

  (nombre "Bus linea D20 Paral·lel-Drassanes")
  (x 41.376538)
  (y 2.175664))

([onto_Class91130] of  parada_bus

  (nombre "Bus linea D20 Metro Paral·lel")
  (x 41.3748271)
  (y 2.1700247))

([onto_Class91131] of  parada_bus

  (nombre "Bus linea D20 Paral·lel, 80")
  (x 41.6854329)
  (y -70.07665039999999))

([onto_Class91132] of  parada_bus

  (nombre "Bus linea D20 Av. Paral·lel, 134")
  (x 41.375252)
  (y 2.160148))

([onto_Class91133] of  parada_bus

  (nombre "Bus linea D20 Av Paral·lel-Rocafort")
  (x 41.375108)
  (y 2.1594))

([onto_Class91134] of  parada_bus

  (nombre "Bus linea D20 Av. Paral·lel, 192-194")
  (x 41.375127)
  (y 2.152734))

([onto_Class91135] of  parada_bus

  (nombre "Bus linea D20 Creu Coberta-Pl. Espanya")
  (x 41.3750289)
  (y 2.1491173))

([onto_Class91137] of  parada_bus

  (nombre "Bus linea D20 Sants-Premia")
  (x 41.375385)
  (y 2.139312))

([onto_Class91138] of  parada_bus

  (nombre "Bus linea D20 Metro Pl Sants")
  (x 41.3757286)
  (y 2.135159))

([onto_Class91139] of  parada_bus

  (nombre "Bus linea D20 Pl de Sants")
  (x 41.3757286)
  (y 2.135159))

([onto_Class91140] of  parada_bus

  (nombre "Bus linea D20 Sants-Rambla del Brasil")
  (x 41.3773437)
  (y 2.1299272))

([onto_Class91141] of  parada_bus

  (nombre "Bus linea D20 Metro Badal")
  (x 41.37558)
  (y 2.127424))

([onto_Class91142] of  parada_bus

  (nombre "Bus linea D20 Arizala-Av Madrid")
  (x 41.376424)
  (y 2.125104))

([onto_Class91143] of  parada_bus

  (nombre "Bus linea D20 Arizala-Les Corts")
  (x 41.378609)
  (y 2.1238201))

([onto_Class91144] of  parada_bus

  (nombre "Bus linea D20 Les Corts-Ctra de Collblanc")
  (x 41.3758601)
  (y 2.1135793))

([onto_Class91145] of  parada_bus

  (nombre "Bus linea D20 Collblanc-Cardenal Reig")
  (x 41.375919)
  (y 2.114639))

([onto_Class91146] of  parada_bus

  (nombre "Bus linea D20  Passeig Maritim")
  (x 41.3812657)
  (y 2.1935088))

([onto_Class91147] of  parada_bus

  (nombre "Bus linea D20 Ernest Lluch")
  (x 41.3765126)
  (y 2.111447))

([onto_Class91149] of  parada_bus

  (nombre "Bus linea D20 Av Sant Ramon Nonat-Cardenal Reig")
  (x 41.378374)
  (y 2.117137))

([onto_Class91150] of  parada_bus

  (nombre "Bus linea D20 Metro Collblanc")
  (x 41.3758539)
  (y 2.1181504))

([onto_Class91151] of  parada_bus

  (nombre "Bus linea D20 Sants-Munne")
  (x 41.375645)
  (y 2.1229022))

([onto_Class91152] of  parada_bus

  (nombre "Bus linea D20 Metro Badal")
  (x 41.37558)
  (y 2.127424))

([onto_Class91153] of  parada_bus

  (nombre "Bus linea D20 Sants-Badal")
  (x 41.3753542)
  (y 2.1266126))

([onto_Class91154] of  parada_bus

  (nombre "Bus linea D20 Pl de Sants")
  (x 41.3757286)
  (y 2.135159))

([onto_Class91155] of  parada_bus

  (nombre "Bus linea D20 Metro Pl de Sants")
  (x 41.3753523)
  (y 2.1368619))

([onto_Class91156] of  parada_bus

  (nombre "Bus linea D20 Sants-Gayarre")
  (x 41.375257)
  (y 2.139662))

([onto_Class91157] of  parada_bus

  (nombre "Bus linea D20 Metro Hostafrancs")
  (x 41.37525400000001)
  (y 2.143291))

([onto_Class91158] of  parada_bus

  (nombre "Bus linea D20 Creu Coberta-Pl. Espanya")
  (x 41.3750289)
  (y 2.1491173))

([onto_Class91159] of  parada_bus

  (nombre "Bus linea D20 Av Paral·lel-Pl. Espanya")
  (x 41.3749416)
  (y 2.1628722))

([onto_Class91160] of  parada_bus

  (nombre "Bus linea D20 Av Paral·lel-Tamarit")
  (x 41.374889)
  (y 2.155468))

([onto_Class91161] of  parada_bus

  (nombre "Bus linea D20 Av. Paral·lel, 123-125")
  (x 45.2220553)
  (y -83.4761616))

([onto_Class91162] of  parada_bus

  (nombre "Bus linea D20 Av Paral·lel-Margarit")
  (x 41.374866)
  (y 2.164506))

([onto_Class91163] of  parada_bus

  (nombre "Bus linea D20 Paral·lel, 71")
  (x 41.6850389)
  (y -70.0778017))

([onto_Class91164] of  parada_bus

  (nombre "Bus linea D20 Paral·lel-Cabanes")
  (x 41.374794)
  (y 2.170635))

([onto_Class91165] of  parada_bus

  (nombre "Bus linea D20 Paral·lel-Drassanes")
  (x 41.376538)
  (y 2.175664))

([onto_Class91166] of  parada_bus

  (nombre "Bus linea D20 Les Drassanes")
  (x 41.3752357)
  (y 2.1759475))

([onto_Class91169] of  parada_bus

  (nombre "Bus linea D20 Pg. Colom-Via Laietana")
  (x 41.3805142)
  (y 2.1816157))

([onto_Class91170] of  parada_bus

  (nombre "Bus linea D20 Pla de Palau")
  (x 41.3829112)
  (y 2.1841122))

([onto_Class91171] of  parada_bus

  (nombre "Bus linea D20 Pg Joan de Borbo")
  (x 41.38003)
  (y 2.187419))

([onto_Class91174] of  parada_bus

  (nombre "Bus linea D20 Platja de la Barceloneta")
  (x 41.3783713)
  (y 2.1924685))

([onto_Class91175] of  parada_bus

  (nombre "Bus linea D20 Hospital del Mar")
  (x 32.9701164)
  (y -117.2641436))

([onto_Class91176] of  parada_bus

  (nombre "Bus linea D20 Trelawny-Av. Litoral")
  (x 41.38625800000001)
  (y 2.194843))

([onto_Class91233] of  parada_bus

  (nombre "Bus linea V21 Trelawny-Pg. Maritim")
  (x 41.385008)
  (y 2.195475))

([onto_Class91234] of  parada_bus

  (nombre "Bus linea V21 Marina-Pl. dels Voluntaris")
  (x 41.388373)
  (y 2.195587))

([onto_Class91235] of  parada_bus

  (nombre "Bus linea V21 Marina-Doctor Trueta")
  (x 41.390955)
  (y 2.1922549))

([onto_Class91236] of  parada_bus

  (nombre "Bus linea V21 Metro Marina")
  (x 44.9378067)
  (y -93.646789))

([onto_Class91237] of  parada_bus

  (nombre "Bus linea V21 Marina-Ausias Marc")
  (x 41.397652)
  (y 2.183325))

([onto_Class91238] of  parada_bus

  (nombre "Bus linea V21 Marina-Monumental")
  (x 41.3994705)
  (y 2.180774))

([onto_Class91240] of  parada_bus

  (nombre "Bus linea V21 Marina-Valencia")
  (x 39.4626603)
  (y -0.3279625))

([onto_Class91241] of  parada_bus

  (nombre "Bus linea V21 Av Gaudi")
  (x 41.407903)
  (y 2.1742985))

([onto_Class91242] of  parada_bus

  (nombre "Bus linea V21 Marina-Industria")
  (x 42.720685)
  (y -82.5107755))

([onto_Class91243] of  parada_bus

  (nombre "Bus linea V21 Padilla-Travessera de Gracia")
  (x 41.410353)
  (y 2.171144))

([onto_Class91244] of  parada_bus

  (nombre "Bus linea V21 Padilla-Mas Casanovas")
  (x 41.411644)
  (y 2.169474))

([onto_Class91245] of  parada_bus

  (nombre "Bus linea V21 Padilla-Tunel de la Rovira")
  (x 41.412898)
  (y 2.167563))

([onto_Class91246] of  parada_bus

  (nombre "Bus linea V21 Rambla del Carmel")
  (x 41.4246391)
  (y 2.1579174))

([onto_Class91247] of  parada_bus

  (nombre "Bus linea V21 Lletres-Dante Alighieri")
  (x 41.4279776)
  (y 2.157948))

([onto_Class91248] of  parada_bus

  (nombre "Bus linea V21 Pl de l'Estatut")
  (x 41.430287)
  (y 2.155543))

([onto_Class91249] of  parada_bus

  (nombre "Bus linea V21 Pl de Botticelli")
  (x 44.0807761)
  (y -103.2264281))

([onto_Class91250] of  parada_bus

  (nombre "Bus linea V21 Jorge Manrique-Can Travi")
  (x 41.4310552)
  (y 2.1521076))

([onto_Class91251] of  parada_bus

  (nombre "Bus linea V21 Arquitecte Moragas-Jorge Manrique")
  (x 41.432676)
  (y 2.147466))

([onto_Class91252] of  parada_bus

  (nombre "Bus linea V21 Pg de la Vall d'Hebron-Poesia")
  (x 41.4306679)
  (y 2.1447539))

([onto_Class91253] of  parada_bus

  (nombre "Bus linea V21 Pg de la Vall d'Hebron-Metro Montbau")
  (x 41.4302731)
  (y 2.1452177))

([onto_Class91254] of  parada_bus

  (nombre "Bus linea V21 Arquitectura-angel Marques")
  (x 41.429315)
  (y 2.141186))

([onto_Class91255] of  parada_bus

  (nombre "Bus linea V21 Vayreda-Arquitectura")
  (x 41.430385)
  (y 2.140176))

([onto_Class91256] of  parada_bus

  (nombre "Bus linea V21 Montbau")
  (x 41.4304873)
  (y 2.1438689))

([onto_Class91257] of  parada_bus

  (nombre "Bus linea V21 Montbau")
  (x 41.4304873)
  (y 2.1438689))

([onto_Class91258] of  parada_bus

  (nombre "Bus linea V21 Poesia-Jardins de Montbau")
  (x 41.43195499999999)
  (y 2.144059))

([onto_Class91259] of  parada_bus

  (nombre "Bus linea V21 Metro Montbau")
  (x 41.4302731)
  (y 2.1452177))

([onto_Class91260] of  parada_bus

  (nombre "Bus linea V21 Palau d'Esports de la Vall d'Hebron")
  (x 41.4278246)
  (y 2.1441513))

([onto_Class91261] of  parada_bus

  (nombre "Bus linea V21 Parc de la Vall d'Hebron")
  (x 41.4216213)
  (y 2.1411816))

([onto_Class91263] of  parada_bus

  (nombre "Bus linea V21 Av Cardenal Vidal i Barraquer-Pl Botticelli")
  (x 41.432762)
  (y 2.151829))

([onto_Class91264] of  parada_bus

  (nombre "Bus linea V21 Pl de l'Estatut")
  (x 41.430287)
  (y 2.155543))

([onto_Class91265] of  parada_bus

  (nombre "Bus linea V21 Rambla del Carmel-Dante Alighieri")
  (x 41.4272145)
  (y 2.1572417))

([onto_Class91267] of  parada_bus

  (nombre "Bus linea V21 Lepant-Guinardo")
  (x 41.411698)
  (y 2.167011))

([onto_Class91268] of  parada_bus

  (nombre "Bus linea V21 Lepant-Travessera de Gracia")
  (x 41.4095158)
  (y 2.1699088))

([onto_Class91269] of  parada_bus

  (nombre "Bus linea V21 Lepant-Sant Antoni Maria Claret")
  (x 41.4083345)
  (y 2.1704098))

([onto_Class91270] of  parada_bus

  (nombre "Bus linea V21 Av Gaudi")
  (x 41.407903)
  (y 2.1742985))

([onto_Class91271] of  parada_bus

  (nombre "Bus linea V21 Lepant-Mallorca")
  (x 41.40416)
  (y 2.176964))

([onto_Class91272] of  parada_bus

  (nombre "Bus linea V21 Lepant-Av Diagonal")
  (x 41.4044372)
  (y 2.1767127))

([onto_Class91273] of  parada_bus

  (nombre "Bus linea V21 Marina-Monumental")
  (x 41.3994705)
  (y 2.180774))

([onto_Class91274] of  parada_bus

  (nombre "Bus linea V21 Marina-Ctra de Ribes")
  (x 38.7743146)
  (y 0.1418648))

([onto_Class91275] of  parada_bus

  (nombre "Bus linea V21 Metro Marina")
  (x 44.9378067)
  (y -93.646789))

([onto_Class91276] of  parada_bus

  (nombre "Bus linea V21 Marina-Ramon Turro")
  (x 41.390871)
  (y 2.191899))

([onto_Class91278] of  parada_bus

  (nombre "Bus linea V21 Trelawny-Pg. Maritim")
  (x 41.385008)
  (y 2.195475))

([onto_Class91532] of  parada_bus

  (nombre "Bus linea V27 Pg. Maritim - Hospital del Mar")
  (x 41.3838634)
  (y 2.1943826))

([onto_Class91533] of  parada_bus

  (nombre "Bus linea V27 Dr. Aiguader - Pl. del Gas")
  (x 41.38452300000001)
  (y 2.192369))

([onto_Class91534] of  parada_bus

  (nombre "Bus linea V27 Av. Litoral - Trelawny")
  (x 41.38625800000001)
  (y 2.194843))

([onto_Class91535] of  parada_bus

  (nombre "Bus linea V27 Marina-Pl. dels Voluntaris")
  (x 41.388373)
  (y 2.195587))

([onto_Class91536] of  parada_bus

  (nombre "Bus linea V27 Av Icaria - Joan Miro")
  (x 41.390328)
  (y 2.195773))

([onto_Class91537] of  parada_bus

  (nombre "Bus linea V27 Av. Icaria-alaba")
  (x 41.393104)
  (y 2.198985))

([onto_Class91538] of  parada_bus

  (nombre "Bus linea V27 Cementiri de l'Est")
  (x 41.3958991)
  (y 2.2030233))

([onto_Class91539] of  parada_bus

  (nombre "Bus linea V27 Pg Calvell-Rambla del Poblenou")
  (x 41.397033)
  (y 2.205785))

([onto_Class91540] of  parada_bus

  (nombre "Bus linea V27 Jonquera-Taulat")
  (x 41.4000089)
  (y 2.2075558))

([onto_Class91541] of  parada_bus

  (nombre "Bus linea V27 Bac de Roda-Taulat")
  (x 41.403262)
  (y 2.209743))

([onto_Class91542] of  parada_bus

  (nombre "Bus linea V27 Bac de Roda-Llull")
  (x 41.405568)
  (y 2.206712))

([onto_Class91543] of  parada_bus

  (nombre "Bus linea V27 Bac de Roda-Cristobal de Moura")
  (x 41.408152)
  (y 2.203269))

([onto_Class91544] of  parada_bus

  (nombre "Bus linea V27 Bac de Roda-Peru")
  (x 41.4107274)
  (y 2.1999076))

([onto_Class91545] of  parada_bus

  (nombre "Bus linea V27 Bac de Roda-Gran Via")
  (x 41.411085)
  (y 2.197428))

([onto_Class91546] of  parada_bus

  (nombre "Bus linea V27 Bac de Roda-Rambla de Guipuscoa")
  (x 41.4144028)
  (y 2.1940235))

([onto_Class91547] of  parada_bus

  (nombre "Bus linea V27 Felip II-Palencia")
  (x 42.0042404)
  (y -4.5260595))

([onto_Class91548] of  parada_bus

  (nombre "Bus linea V27 Felip II-Av Meridiana")
  (x 41.4221205)
  (y 2.1867737))

([onto_Class91549] of  parada_bus

  (nombre "Bus linea V27 Pl. dels Indians")
  (x 41.42355999999999)
  (y 2.182623))

([onto_Class91550] of  parada_bus

  (nombre "Bus linea V27 Felip II-Pl del Congres")
  (x 41.425887)
  (y 2.179847))

([onto_Class91551] of  parada_bus

  (nombre "Bus linea V27 Felip II-Arnau d'Oms")
  (x 41.427247)
  (y 2.178064))

([onto_Class91552] of  parada_bus

  (nombre "Bus linea V27 Arnau d'Oms-La Jota")
  (x 41.429313)
  (y 2.177416))

([onto_Class91553] of  parada_bus

  (nombre "Bus linea V27 Fabra i Puig-Arnau d'Oms")
  (x 41.430324)
  (y 2.177722))

([onto_Class91555] of  parada_bus

  (nombre "Bus linea V27 Fabra i Puig-Petrarca")
  (x 41.430349)
  (y 2.168059))

([onto_Class91556] of  parada_bus

  (nombre "Bus linea V27 Parc del Turo de la Peira")
  (x 41.4331049)
  (y 2.1659017))

([onto_Class91557] of  parada_bus

  (nombre "Bus linea V27 Fabra i Puig-Pg. Urrutia")
  (x 41.436643)
  (y 2.164549))

([onto_Class91558] of  parada_bus

  (nombre "Bus linea V27 Pg. Valldaura-Guineueta")
  (x 41.439437)
  (y 2.165384))

([onto_Class91559] of  parada_bus

  (nombre "Bus linea V27 Ronda Guineueta Vella-Antonio Machado")
  (x 41.441537)
  (y 2.162056))

([onto_Class91560] of  parada_bus

  (nombre "Bus linea V27 Ronda Guineueta Vella-Juan Ramon Jimenez")
  (x 41.442855)
  (y 2.162305))

([onto_Class91561] of  parada_bus

  (nombre "Bus linea V27 Ronda Guineueta Vella-Barri Canyelles")
  (x 41.4443047)
  (y 2.1651998))

([onto_Class91562] of  parada_bus

  (nombre "Bus linea V27 Ronda Guineueta Vella-Col·legi Eugeni d'Ors")
  (x 41.4448285)
  (y 2.167823))

([onto_Class91563] of  parada_bus

  (nombre "Bus linea V27 Antonio Machado-Barri de Canyelles")
  (x 41.444218)
  (y 2.170234))

([onto_Class91564] of  parada_bus

  (nombre "Bus linea V27 Antonio Machado - Barri de Canyelles")
  (x 41.444218)
  (y 2.170234))

([onto_Class91565] of  parada_bus

  (nombre "Bus linea V27 Via Favencia - Barri Canyelles")
  (x 41.442965)
  (y 2.169598))

([onto_Class91566] of  parada_bus

  (nombre "Bus linea V27 Metro Canyelles")
  (x 41.4417534)
  (y 2.166388))

([onto_Class91567] of  parada_bus

  (nombre "Bus linea V27 Pl Karl Marx")
  (x 41.439434)
  (y 2.1625142))

([onto_Class91568] of  parada_bus

  (nombre "Bus linea V27 Pg. Valldaura-Fabra i Puig")
  (x 41.439202)
  (y 2.165761))

([onto_Class91569] of  parada_bus

  (nombre "Bus linea V27 Fabra i Puig-Pg. Urrutia")
  (x 41.436643)
  (y 2.164549))

([onto_Class91570] of  parada_bus

  (nombre "Bus linea V27 Parc Turo de la Peira")
  (x 41.4331049)
  (y 2.1659017))

([onto_Class91571] of  parada_bus

  (nombre "Bus linea V27 Fabra i Puig-Petrarca")
  (x 41.430349)
  (y 2.168059))

([onto_Class91574] of  parada_bus

  (nombre "Bus linea V27 Felip II-Ramon Albo")
  (x 41.427261)
  (y 2.177781))

([onto_Class91575] of  parada_bus

  (nombre "Bus linea V27 Felip II-Pl. Congres")
  (x 41.425721)
  (y 2.179802))

([onto_Class91576] of  parada_bus

  (nombre "Bus linea V27 Pl. dels Indians")
  (x 41.42355999999999)
  (y 2.182623))

([onto_Class91577] of  parada_bus

  (nombre "Bus linea V27 Av Meridiana-La Sagrera")
  (x 41.424854)
  (y 2.1865331))

([onto_Class91578] of  parada_bus

  (nombre "Bus linea V27 Espronceda-Pl Islandia")
  (x 41.417606)
  (y 2.188244))

([onto_Class91579] of  parada_bus

  (nombre "Bus linea V27 Espronceda-Rambla de Guipuscoa")
  (x 41.4141099)
  (y 2.194364600000001))

([onto_Class91580] of  parada_bus

  (nombre "Bus linea V27 Espronceda-Gran Via")
  (x 41.4115)
  (y 2.196362))

([onto_Class91581] of  parada_bus

  (nombre "Bus linea V27 Espronceda-Bolivia")
  (x 41.4093766)
  (y 2.1992256))

([onto_Class91582] of  parada_bus

  (nombre "Bus linea V27 Av Diagonal-Bac de Roda")
  (x 41.407592)
  (y 2.204838))

([onto_Class91583] of  parada_bus

  (nombre "Bus linea V27 Espronceda-Jardins Josep Trueta")
  (x 41.404384)
  (y 2.205776))

([onto_Class91585] of  parada_bus

  (nombre "Bus linea V27 Taulat-Bilbao")
  (x 41.400702)
  (y 2.207143))

([onto_Class91586] of  parada_bus

  (nombre "Bus linea V27 Taulat-Rambla del Poblenou")
  (x 41.39829900000001)
  (y 2.204189))

([onto_Class91587] of  parada_bus

  (nombre "Bus linea V27 Taulat-Ciutat de Granada")
  (x 41.39621)
  (y 2.201685))

([onto_Class91588] of  parada_bus

  (nombre "Bus linea V27 Av. Icaria-alaba")
  (x 41.393104)
  (y 2.198985))

([onto_Class91590] of  parada_bus

  (nombre "Bus linea V27 Vila Olimpica")
  (x 41.3915949)
  (y 2.1953889))

([onto_Class91591] of  parada_bus

  (nombre "Bus linea V27 Pg. Maritim-Hospital del Mar")
  (x 41.3838634)
  (y 2.1943826))


([onto_Class110000] of  parada_bus

  (nombre "linea-t1 Francesc Macia ")
  (x 42.678976)
  (y 2.2656755))

([onto_Class110003] of  parada_tram

  (nombre "linea-t1 Maria Cristina ")
  (x 41.38781600000001)
  (y 2.125791))

([onto_Class110004] of  parada_tram

  (nombre "linea-t1 Pius XII ")
  (x 41.386719)
  (y 2.121603))

([onto_Class110005] of  parada_tram

  (nombre "linea-t1 Palau Reial ")
  (x 41.385919)
  (y 2.118461))

([onto_Class110007] of  parada_tram

  (nombre "linea-t1 Avinguda de Xile ")
  (x 41.3807542)
  (y 2.1148403))

([onto_Class110010] of  parada_tram

  (nombre "linea-t1 Ca n’Oliveres ")
  (x 41.392206)
  (y 2.143175))

([onto_Class110011] of  parada_tram

  (nombre "linea-t1 Can Clota ")
  (x 41.392206)
  (y 2.143175))

([onto_Class110012] of  parada_tram

  (nombre "linea-t1 Pont d’Esplugues ")
  (x 41.37662479999999)
  (y 2.0881271))

([onto_Class110013] of  parada_tram

  (nombre "linea-t1 La Sardana ")
  (x 41.3807542)
  (y 2.1148403))

([onto_Class110015] of  parada_tram

  (nombre "linea-t1 El Pedro ")
  (x 41.4172212)
  (y 2.2135882))

([onto_Class110016] of  parada_tram

  (nombre "linea-t1 Ignasi Iglesias ")
  (x 41.392206)
  (y 2.143175))

([onto_Class110017] of  parada_tram

  (nombre "linea-t1 Cornella Centre ")
  (x 41.392206)
  (y 2.143175))

([onto_Class110019] of  parada_tram

  (nombre "linea-t1 Fontsanta/Fatjo ")
  (x 41.392206)
  (y 2.143175))

([onto_Class110020] of  parada_tram

  (nombre "linea-t1 Bon Viatge ")
  (x 41.364608)
  (y 2.058817))

([onto_Class110021] of  parada_tram

  (nombre "linea-t2 Francesc Macia ")
  (x 41.392206)
  (y 2.143175))

([onto_Class110022] of  parada_tram

  (nombre "linea-t2 L’Illa ")
  (x 41.3896183)
  (y 2.1350706))

([onto_Class110023] of  parada_tram

  (nombre "linea-t2 Numancia ")
  (x 41.389342)
  (y 2.131953))

([onto_Class110024] of  parada_tram

  (nombre "linea-t2 Maria Cristina ")
  (x 41.38781600000001)
  (y 2.125791))

([onto_Class110025] of  parada_tram

  (nombre "linea-t2 Pius XII ")
  (x 41.386719)
  (y 2.121603))

([onto_Class110026] of  parada_tram

  (nombre "linea-t2 Palau Reial ")
  (x 41.385919)
  (y 2.118461))

([onto_Class110027] of  parada_tram

  (nombre "linea-t2 Zona Universitaria ")
  (x 41.3843292)
  (y 2.1116123))

([onto_Class110029] of  parada_tram

  (nombre "linea-t2 Ernest Lluch ")
  (x 41.3765126)
  (y 2.111447))

([onto_Class110030] of  parada_tram

  (nombre "linea-t2 Can Rigal ")
  (x 41.376078)
  (y 2.105928))

([onto_Class110031] of  parada_tram

  (nombre "linea-t2 Ca n’Oliveres ")
  (x 41.392206)
  (y 2.143175))

([onto_Class110034] of  parada_tram

  (nombre "linea-t2 La Sardana ")
  (x 41.3807542)
  (y 2.1148403))

([onto_Class110036] of  parada_tram

  (nombre "linea-t2 El Pedro ")
  (x 41.4172212)
  (y 2.2135882))

([onto_Class110037] of  parada_tram

  (nombre "linea-t2 Ignasi Iglesias ")
  (x 41.392206)
  (y 2.143175))

([onto_Class110038] of  parada_tram

  (nombre "linea-t2 Cornella Centre ")
  (x 41.392206)
  (y 2.143175))

([onto_Class110039] of  parada_tram

  (nombre "linea-t2 Les Aigües ")
  (x 41.392206)
  (y 2.143175))

([onto_Class110040] of  parada_tram

  (nombre "linea-t2 Fontsanta/Fatjo ")
  (x 41.392206)
  (y 2.143175))

([onto_Class110041] of  parada_tram

  (nombre "linea-t2 Bon Viatge ")
  (x 41.364608)
  (y 2.058817))

([onto_Class110042] of  parada_tram

  (nombre "linea-t2 La Fontsanta ")
  (x 41.392206)
  (y 2.143175))

([onto_Class110044] of  parada_tram

  (nombre "linea-t2 Llevant-Les planes ")
  (x 41.370869)
  (y 2.076117))

([onto_Class110045] of  parada_tram

  (nombre "linea-t3 Francesc Macia ")
  (x 41.392206)
  (y 2.143175))

([onto_Class110047] of  parada_tram

  (nombre "linea-t3 Numancia ")
  (x 41.389342)
  (y 2.131953))

([onto_Class110048] of  parada_tram

  (nombre "linea-t3 Maria Cristina ")
  (x 41.38781600000001)
  (y 2.125791))

([onto_Class110049] of  parada_tram

  (nombre "linea-t3 Pius XII ")
  (x 41.386719)
  (y 2.121603))

([onto_Class110050] of  parada_tram

  (nombre "linea-t3 Palau Reial ")
  (x 41.385919)
  (y 2.118461))

([onto_Class110052] of  parada_tram

  (nombre "linea-t3 Avinguda de Xile ")
  (x 41.3807542)
  (y 2.1148403))

([onto_Class110054] of  parada_tram

  (nombre "linea-t3 Can Rigal ")
  (x 41.376078)
  (y 2.105928))

([onto_Class110055] of  parada_tram

  (nombre "linea-t3 Ca n’Oliveres ")
  (x 41.392206)
  (y 2.143175))

([onto_Class110056] of  parada_tram

  (nombre "linea-t3 Can Clota ")
  (x 41.392206)
  (y 2.143175))

([onto_Class110057] of  parada_tram

  (nombre "linea-t3 Pont d’Esplugues ")
  (x 41.37662479999999)
  (y 2.0881271))

([onto_Class110060] of  parada_tram

  (nombre "linea-t3 Hospital Sant Joan Despi/TV3 ")
  (x 41.372386)
  (y 2.075678))

([onto_Class110061] of  parada_tram

  (nombre "linea-t3 Rambla de Sant Just ")
  (x 41.378828)
  (y 2.07505))

([onto_Class110062] of  parada_tram

  (nombre "linea-t3 Walden ")
  (x 41.4172212)
  (y 2.2135882))

([onto_Class110064] of  parada_tram

  (nombre "linea-t3 Sant Feliu/Consell Comarcal ")
  (x 41.379356)
  (y 2.053372))

([onto_Class110065] of  parada_tram

  (nombre "linea-t4 Ciutadella/Vila Olimpica ")
  (x 41.3878244)
  (y 2.19337))

([onto_Class110066] of  parada_tram

  (nombre "linea-t4 Wellington ")
  (x 41.390167)
  (y 2.188483))

([onto_Class110067] of  parada_tram

  (nombre "linea-t4 Marina ")
  (x 41.3943155)
  (y 2.1875611))

([onto_Class110068] of  parada_tram

  (nombre "linea-t4 Auditori/Teatre Nacional ")
  (x 41.397536)
  (y 2.186892))

([onto_Class110070] of  parada_tram

  (nombre "linea-t4 Ca l’Aranyo ")
  (x 41.40445)
  (y 2.1915))

([onto_Class110071] of  parada_tram

  (nombre "linea-t4 Pere IV ")
  (x 41.406144)
  (y 2.198247))

([onto_Class110072] of  parada_tram

  (nombre "linea-t4 Fluvia ")
  (x 41.407822)
  (y 2.204908))

([onto_Class110073] of  parada_tram

  (nombre "linea-t4 Selva de Mar ")
  (x 41.408001)
  (y 2.209122))

([onto_Class110075] of  parada_tram

  (nombre "linea-t4 Forum ")
  (x 41.418824)
  (y 2.225752))

([onto_Class110076] of  parada_tram

  (nombre "linea-t4 Campus Diagonal-Besos ")
  (x 41.415247)
  (y 2.222928))

([onto_Class110077] of  parada_tram

  (nombre "linea-t4 Port Forum ")
  (x 41.4126766)
  (y 2.2279477))

([onto_Class110078] of  parada_tram

  (nombre "linea-t4 Estacio de Sant Adria ")
  (x 41.423953)
  (y 2.23055))

([onto_Class110079] of  parada_tram

  (nombre "linea-t5 Glories ")
  (x 41.40225299999999)
  (y 2.187534))

([onto_Class110080] of  parada_tram

  (nombre "linea-t5 La Farinera ")
  (x 41.4042)
  (y 2.188581))

([onto_Class110082] of  parada_tram

  (nombre "linea-t5 Espronceda ")
  (x 41.411361)
  (y 2.197942))

([onto_Class110083] of  parada_tram

  (nombre "linea-t5 Sant Marti de Provencals ")
  (x 41.418456)
  (y 2.2072961))

([onto_Class110084] of  parada_tram

  (nombre "linea-t5 Besos ")
  (x 41.4172212)
  (y 2.2135882))

([onto_Class110087] of  parada_tram

  (nombre "linea-t5 La Catalana ")
  (x 41.42265)
  (y 2.221953))

([onto_Class110089] of  parada_tram

  (nombre "linea-t5 Encants de Sant Adria ")
  (x 41.43024399999999)
  (y 2.222606))

([onto_Class110092] of  parada_tram

  (nombre "linea-t6 Glories ")
  (x 41.40225299999999)
  (y 2.187534))

([onto_Class110094] of  parada_tram

  (nombre "linea-t6 Can Jaumandreu ")
  (x 41.408108)
  (y 2.193597))

([onto_Class110096] of  parada_tram

  (nombre "linea-t6 Sant Marti de Provencals ")
  (x 41.418456)
  (y 2.2072961))

([onto_Class110097] of  parada_tram

  (nombre "linea-t6 Besos ")
  (x 41.4172212)
  (y 2.2135882))

([onto_Class110100] of  parada_tram

  (nombre "linea-t6 La Mina ")
  (x 41.4183429)
  (y 2.2209491))

([onto_Class110101] of  parada_tram

  (nombre "linea-t6 Port Forum ")
  (x 41.4126766)
  (y 2.2279477))

([onto_Class160000] of  zona_verde

  (nombre "Parque de la Ciutadella")
  (x 41.388123)
  (y 2.1860152))

([onto_Class160001] of  zona_verde

  (nombre "Parque del Laberint d'Horta")
  (x 41.4402362)
  (y 2.1457979))

([onto_Class160002] of  zona_verde

  (nombre "Jardines de la Tamarita")
  (x 41.410658)
  (y 2.1357251))

([onto_Class160003] of  zona_verde

  (nombre "Parque del Castell de l'Oreneta")
  (x 41.39920980000001)
  (y 2.1122933))

([onto_Class160004] of  zona_verde

  (nombre "Park Güell")
  (x 41.4144948)
  (y 2.1526945))

([onto_Class160005] of  zona_verde

  (nombre "Jardines del Palacio de Pedralbes")
  (x 41.3875923)
  (y 2.1176991))

([onto_Class160006] of  zona_verde

  (nombre "Jardin Botanico")
  (x 41.3620735)
  (y 2.1576895))

([onto_Class160007] of  zona_verde

  (nombre "Jardines de Mossen Costa i Llobera")
  (x 41.3686605)
  (y 2.1719017))

([onto_Class160008] of  zona_verde

  (nombre "Parque de la Creueta del Coll")
  (x 41.4186608)
  (y 2.1468122))

([onto_Class160009] of  zona_verde

  (nombre "Jardines de Laribal")
  (x 41.3676056)
  (y 2.1582782))

([onto_Class160010] of  zona_verde

  (nombre "Jardines de Joan Brossa")
  (x 41.36831)
  (y 2.1674475))

([onto_Class160011] of  zona_verde

  (nombre "Parque del Clot")
  (x 41.4080056)
  (y 2.1902812))

([onto_Class160012] of  zona_verde

  (nombre "Parque de La Pegaso")
  (x 41.4272893)
  (y 2.1891976))

([onto_Class160013] of  zona_verde

  (nombre "Parque de la Espanya Industrial")
  (x 41.3774138)
  (y 2.1404825))

([onto_Class160014] of  zona_verde

  (nombre "Parque de Joan Miro")
  (x 41.3780556)
  (y 2.1484207))

([onto_Class160016] of  zona_verde

  (nombre "Parque del Turo del Putxet")
  (x 41.4082916)
  (y 2.1423701))

([onto_Class160017] of  zona_verde

  (nombre "El parque de Cervantes")
  (x 41.3843061)
  (y 2.1057017))

([onto_Class160018] of  zona_verde

  (nombre "Parque Central de Nou Barris")
  (x 41.4391313)
  (y 2.162702700000001))

([onto_Class160019] of  zona_verde

  (nombre "Parque de Diagonal Mar")
  (x 41.40749)
  (y 2.2138601))

([onto_Class160020] of  zona_verde

  (nombre "Parque del Mirador del Poble-sec")
  (x 41.3717996)
  (y 2.1718933))

([onto_Class160021] of  zona_verde

  (nombre "Parque de la Trinitat")
  (x 41.44920640000001)
  (y 2.1954576))

([onto_Class160022] of  zona_verde

  (nombre "Jardines de Joan Maragall")
  (x 41.3671621)
  (y 2.155426))

([onto_Class160024] of  zona_verde

  (nombre "Jardin de los Drets Humans")
  (x 41.3611566)
  (y 2.1366515))

([onto_Class160025] of  zona_verde

  (nombre "Jardines del Palau Robert")
  (x 41.3958587)
  (y 2.1588387))

([onto_Class160026] of  zona_verde

  (nombre "Jardines de las Tres Xemeneies")
  (x 41.3740965)
  (y 2.1721112))

([onto_Class160027] of  zona_verde

  (nombre "Parque de Collserola")
  (x 41.44255039999999)
  (y 2.1016603))

([onto_Class160028] of  zona_verde

  (nombre "El parque de las Aigües")
  (x 41.4131591)
  (y 2.1654891))

([onto_Class160029] of  zona_verde

  (nombre "Parque del Guinardo")
  (x 41.4194591)
  (y 2.1673536))

([onto_Class160031] of  zona_verde

  (nombre "Parque de la Maquinista de Sant Andreu")
  (x 41.4367159)
  (y 2.1953359))

([onto_Class160032] of  zona_verde

  (nombre "Parque del Centre del Poblenou")
  (x 41.407503)
  (y 2.2008771))

([onto_Class160033] of  zona_verde

  (nombre "Jardines del Teatre Grec")
  (x 41.3700552)
  (y 2.1586352))

([onto_Class160034] of  zona_verde

  (nombre "Parque de Can Drago")
  (x 41.4338122)
  (y 2.1824732))

([onto_Class160035] of  zona_verde

  (nombre "Parque de la Barceloneta")
  (x 41.38283320000001)
  (y 2.1926914))

([onto_Class160036] of  zona_verde

  (nombre "Jardines de Mossen Cinto Verdaguer")
  (x 41.3673533)
  (y 2.1651338))

([onto_Class160037] of  zona_verde

  (nombre "Parque de Sant Marti")
  (x 41.4196786)
  (y 2.1981451))

([onto_Class160038] of  zona_verde

  (nombre "Parque de la Guineueta")
  (x 41.44002769999999)
  (y 2.1734243))

([onto_Class160039] of  zona_verde

  (nombre "Jardines del Mirador del Alcalde")
  (x 41.3671061)
  (y 2.1688851))

([onto_Class160040] of  zona_verde

  (nombre "Parque de las Rieres d'Horta")
  (x 41.4311795)
  (y 2.1551056))

([onto_Class160042] of  zona_verde

  (nombre "Parque de la Primavera")
  (x 41.3710646)
  (y 2.1670147))

([onto_Class160043] of  zona_verde

  (nombre "Jardines de Vil·la Cecilia")
  (x 41.3937521)
  (y 2.1218574))

([onto_Class160044] of  zona_verde

  (nombre "Parque de las Cascades")
  (x 41.3864514)
  (y 2.1943134))

([onto_Class160045] of  zona_verde

  (nombre "Parque del Poblenou")
  (x 41.3954816)
  (y 2.2049153))

([onto_Class160046] of  zona_verde

  (nombre "Jardines de la Maternitat")
  (x 41.3832848)
  (y 2.1248139))

([onto_Class160047] of  zona_verde

  (nombre "Parque de la Font Florida")
  (x 41.3666347)
  (y 2.1410346))

([onto_Class160048] of  zona_verde

  (nombre "Jardines del Princep de Girona")
  (x 41.40986119999999)
  (y 2.1683707))

([onto_Class160049] of  zona_verde

  (nombre "Parque de Can Sabate")
  (x 41.3629228)
  (y 2.1385795))

([onto_Class160050] of  zona_verde

  (nombre "Parque de la Estacio del Nord")
  (x 41.3930382)
  (y 2.182412))

([onto_Class160051] of  zona_verde

  (nombre "Parque del Turo de la Peira")
  (x 41.4331049)
  (y 2.1659017))

([onto_Class160054] of  zona_verde

  (nombre "Vivero Municipal de Plantas Tres Pins")
  (x 41.3671612)
  (y 2.1597577))

([onto_Class160056] of  zona_verde

  (nombre "Parque de la Vall d'Hebron")
  (x 41.4216213)
  (y 2.1411816))

([onto_Class160057] of  zona_verde

  (nombre "Parque del Port Olimpic")
  (x 41.3885977)
  (y 2.2007138))

([onto_Class160058] of  zona_verde

  (nombre "Jardines de Can Sentmenat")
  (x 41.4026434)
  (y 2.1148023))

([onto_Class160059] of  zona_verde

  (nombre "Parque de Monterols")
  (x 41.40159389999999)
  (y 2.1399221))

([onto_Class160060] of  zona_verde

  (nombre "Parque del Mirador del Migdia")
  (x 41.35846430000001)
  (y 2.1593464))

([onto_Class160062] of  zona_verde

  (nombre "Jardines plaza de Can Fabra")
  (x 41.4347105)
  (y 2.1908298))

([onto_Class160064] of  zona_verde

  (nombre "Jardines de Piscines i Esports")
  (x 41.3934011)
  (y 2.1369556))

([onto_Class160065] of  zona_verde

  (nombre "Parque de Can Rigal")
  (x 41.3793435)
  (y 2.1077672))

([onto_Class160067] of  zona_verde

  (nombre "Jardines de Rodrigo Caro")
  (x 41.4479807)
  (y 2.1714671))

([onto_Class160068] of  zona_verde

  (nombre "Parque de los Auditoris")
  (x 41.4034004)
  (y 2.1868695))

([onto_Class160069] of  zona_verde

  (nombre "Jardines de Frida Kahlo")
  (x 19.355143)
  (y -99.16252490000001))

([onto_Class160070] of  zona_verde

  (nombre "Jardines de la Font dels Ocellets")
  (x 41.3678275)
  (y 2.1575958))

([onto_Class160072] of  zona_verde

  (nombre "Jardines de Rosa Luxemburg")
  (x 41.4326049)
  (y 2.152489000000001))

([onto_Class160074] of  zona_verde

  (nombre "Parque del Pla de Fornells")
  (x 41.448503)
  (y 2.176362))

([onto_Class160075] of  zona_verde

  (nombre "Jardines de Can Castello")
  (x 41.39912500000001)
  (y 2.140236))

([onto_Class160076] of  zona_verde

  (nombre "Jardines de Sant Pau del Camp")
  (x 41.3764641)
  (y 2.1692941))

([onto_Class160077] of  zona_verde

  (nombre "Jardines de Ca n'Altimira")
  (x 41.4041223)
  (y 2.1322121))

([onto_Class160080] of  zona_verde

  (nombre "Parque de Xavier Montsalvatge")
  (x 41.4403337)
  (y 2.1531145))

([onto_Class160081] of  zona_verde

  (nombre "Jardines de Jaume Vicens Vives")
  (x 41.3868048)
  (y 2.125232))

([onto_Class160082] of  zona_verde

  (nombre "Jardines de Miquel Marti i Pol")
  (x 41.4036451)
  (y 2.1952352))

([onto_Class160085] of  zona_verde

  (nombre "Jardines de Joan Vinyoli")
  (x 41.3933703)
  (y 2.1284681))

([onto_Class160086] of  zona_verde

  (nombre "Jardines de Ca l'Alena")
  (x 41.4564802)
  (y 2.2528853))

([onto_Class160087] of  zona_verde

  (nombre "Parque de Les Corts")
  (x 41.3865431)
  (y 2.1362678))

([onto_Class160088] of  zona_verde

  (nombre "Jardines de Moragas")
  (x 41.3986338)
  (y 2.1454903))

([onto_Class190000] of  escuela

  (nombre "Escola Agora")
  (x 41.4375432)
  (y 2.1683817))

([onto_Class190001] of  escuela

  (nombre "Escola Aiguamarina")
  (x 41.4423974)
  (y 2.1765085))

([onto_Class190002] of  escuela

  (nombre "Escola Alexandre Gali")
  (x 41.3812657)
  (y 2.1935088))

([onto_Class190003] of  escuela

  (nombre "Escola Angels Garriga")
  (x 41.4259776)
  (y 2.1676971))

([onto_Class190004] of  escuela

  (nombre "Escola Antoni Balmanya")
  (x 41.4164263)
  (y 2.1828149))

([onto_Class190005] of  escuela

  (nombre "Escola Antoni Brusi")
  (x 41.39091450000001)
  (y 2.1895793))

([onto_Class190006] of  escuela

  (nombre "Escola Arc Iris")
  (x 41.4252363)
  (y 2.1714184))

([onto_Class190009] of  escuela

  (nombre "Escola Bac de Roda")
  (x 41.4160902)
  (y 2.1941489))

([onto_Class190010] of  escuela

  (nombre "Escola Baixeras")
  (x 41.3827775)
  (y 2.1797204))

([onto_Class190011] of  escuela

  (nombre "Escola Baldiri Reixac")
  (x 41.4136108)
  (y 2.152315))

([onto_Class190012] of  escuela

  (nombre "Escola Baloo")
  (x 41.4337981)
  (y 2.1400793))

([onto_Class190013] of  escuela

  (nombre "Escola Barcelona")
  (x 41.3820243)
  (y 2.1322034))

([onto_Class190015] of  escuela

  (nombre "Escola Baro de Viver")
  (x 41.4456085)
  (y 2.2001515))

([onto_Class190016] of  escuela

  (nombre "Escola Barrufet")
  (x 41.3767729)
  (y 2.1346399))

([onto_Class190017] of  escuela

  (nombre "Escola Bogatell")
  (x 41.39334110000001)
  (y 2.1945379))

([onto_Class190018] of  escuela

  (nombre "Escola Brasil")
  (x 41.4159252)
  (y 2.2052265))

([onto_Class190019] of  escuela

  (nombre "Escola Cal Maiol")
  (x 41.3728683)
  (y 2.1359021))

([onto_Class190020] of  escuela

  (nombre "Escola Calderon de la Barca")
  (x 41.4336498)
  (y 2.1716104))

([onto_Class190021] of  escuela

  (nombre "Escola Can Clos")
  (x 41.3607306)
  (y 2.1481467))

([onto_Class190022] of  escuela

  (nombre "Escola Can Fabra")
  (x 41.43373769999999)
  (y 2.1908377))

([onto_Class190023] of  escuela

  (nombre "Escola Carlit")
  (x 41.3973288)
  (y 2.1747677))

([onto_Class190024] of  escuela

  (nombre "Escola Casas")
  (x 41.4084368)
  (y 2.1928039))

([onto_Class190025] of  escuela

  (nombre "Escola Castella")
  (x 41.3849765)
  (y 2.1645953))

([onto_Class190026] of  escuela

  (nombre "Escola Catalonia")
  (x 41.4127731)
  (y 2.2015638))

([onto_Class190027] of  escuela

  (nombre "Escola Cavall Bernat")
  (x 41.37076829999999)
  (y 2.1318895))

([onto_Class190028] of  escuela

  (nombre "Escola Cervantes")
  (x 41.38724209999999)
  (y 2.1774021))

([onto_Class190029] of  escuela

  (nombre "Escola Ciutat Comtal")
  (x 41.4625413)
  (y 2.1823926))

([onto_Class190030] of  escuela

  (nombre "Escola Collaso i Gil")
  (x 41.3759322)
  (y 2.1691632))

([onto_Class190031] of  escuela

  (nombre "Escola Concepcion Arenal")
  (x 41.4174611)
  (y 2.2153221))

([onto_Class190032] of  escuela

  (nombre "Escola Congres-Indians")
  (x 41.4254477)
  (y 2.1892882))

([onto_Class190033] of  escuela

  (nombre "Escola Coves d'en Cimany")
  (x 41.4205263)
  (y 2.1525828))

([onto_Class190034] of  escuela

  (nombre "Escola de Bosc de Montjuïc")
  (x 41.3694832)
  (y 2.1610831))

([onto_Class190035] of  escuela

  (nombre "Escola de la Concepcio")
  (x 41.3955954)
  (y 2.1682954))

([onto_Class190036] of  escuela

  (nombre "Escola del Mar")
  (x 41.41793)
  (y 2.17017))

([onto_Class190037] of  escuela

  (nombre "Escola dels Encants")
  (x 41.4040502)
  (y 2.1832839))

([onto_Class190039] of  escuela

  (nombre "Escola Doctor Ferran i Clua")
  (x 41.4242171)
  (y 2.178964))

([onto_Class190040] of  escuela

  (nombre "Escola Dolors MonserdA-Santapau")
  (x 41.4049548)
  (y 2.1155093))

([onto_Class190041] of  escuela

  (nombre "Escola Dovella")
  (x 41.4113881)
  (y 2.1856933))

([onto_Class190042] of  escuela

  (nombre "Escola Drassanes")
  (x 41.3790391)
  (y 2.177311))

([onto_Class190043] of  escuela

  (nombre "Escola Duran i Bas")
  (x 41.3854675)
  (y 2.1334542))

([onto_Class190044] of  escuela

  (nombre "Escola Eduard Marquina")
  (x 41.4184018)
  (y 2.214055))

([onto_Class190045] of  escuela

  (nombre "Escola El Carmel")
  (x 41.4233213)
  (y 2.1540839))

([onto_Class190047] of  escuela

  (nombre "Escola El Sagrer")
  (x 41.422182)
  (y 2.189392))

([onto_Class190049] of  escuela

  (nombre "Escola Elisenda de Montcada")
  (x 41.4602261)
  (y 2.1791882))

([onto_Class190050] of  escuela

  (nombre "Escola Els Horts")
  (x 41.4242824)
  (y 2.201358700000001))

([onto_Class190051] of  escuela

  (nombre "Escola Els Llorers")
  (x 41.3835856)
  (y 2.1535038))

([onto_Class190052] of  escuela

  (nombre "Escola Els Pins")
  (x 41.4352896)
  (y 2.1450404))

([onto_Class190053] of  escuela

  (nombre "Escola Els Porxos")
  (x 41.41475399999999)
  (y 2.1982397))

([onto_Class190054] of  escuela

  (nombre "Escola Els Xiprers")
  (x 41.4197964)
  (y 2.1020932))

([onto_Class190055] of  escuela

  (nombre "Escola Emili Juncadella")
  (x 41.4180107)
  (y 2.1814336))

([onto_Class190056] of  escuela

  (nombre "Escola Enric Granados")
  (x 41.3568302)
  (y 2.1417228))

([onto_Class190057] of  escuela

  (nombre "Escola Estel-Guinardo")
  (x 41.4175283)
  (y 2.1725785))

([onto_Class190058] of  escuela

  (nombre "Escola EulAlia Bota")
  (x 41.44455010000001)
  (y 2.192545))

([onto_Class190059] of  escuela

  (nombre "Escola Ferran Sunyer")
  (x 41.3755747)
  (y 2.1629349))

([onto_Class190060] of  escuela

  (nombre "Escola Ferrer i GuArdia")
  (x 41.46136449999999)
  (y 2.1750555))

([onto_Class190062] of  escuela

  (nombre "Escola Font d'en Fargas")
  (x 41.4226472)
  (y 2.1655351))

([onto_Class190063] of  escuela

  (nombre "Escola Fort Pienc")
  (x 41.3951298)
  (y 2.1825278))

([onto_Class190064] of  escuela

  (nombre "Escola Francesc MaciA")
  (x 41.3748992)
  (y 2.1478782))

([onto_Class190065] of  escuela

  (nombre "Escola Fructuos Gelabert")
  (x 41.406017)
  (y 2.169979))

([onto_Class190066] of  escuela

  (nombre "Escola Gayarre")
  (x 41.3730302)
  (y 2.1404638))

([onto_Class190067] of  escuela

  (nombre "Escola Heura")
  (x 41.4270348)
  (y 2.1638936))

([onto_Class190068] of  escuela

  (nombre "Escola Ignasi Iglesias")
  (x 41.4414474)
  (y 2.1913561))

([onto_Class190069] of  escuela

  (nombre "Escola itaca")
  (x 41.3883533)
  (y 2.1341908))

([onto_Class190070] of  escuela

  (nombre "Escola Jaume I")
  (x 41.3809198)
  (y 2.1379014))

([onto_Class190072] of  escuela

  (nombre "Escola Joaquim Ruyra")
  (x 41.4112887)
  (y 2.2145284))

([onto_Class190073] of  escuela

  (nombre "Escola Josep Maria de Sagarra")
  (x 41.4192657)
  (y 2.1401523))

([onto_Class190074] of  escuela

  (nombre "Escola Josep Maria Jujol")
  (x 41.3981789)
  (y 2.1559094))

([onto_Class190075] of  escuela

  (nombre "Escola Jovellanos")
  (x 41.4089479)
  (y 2.166478))

([onto_Class190076] of  escuela

  (nombre "Escola L'Arc de Sant Marti")
  (x 41.42283219999999)
  (y 2.1996588))

([onto_Class190077] of  escuela

  (nombre "Escola L'Arenal de Llevant")
  (x 41.40303249999999)
  (y 2.2092203))

([onto_Class190078] of  escuela

  (nombre "Escola L'Estel")
  (x 41.419114)
  (y 2.1890909))

([onto_Class190079] of  escuela

  (nombre "Escola l'Univers")
  (x 41.4029381)
  (y 2.1623034))

([onto_Class190080] of  escuela

  (nombre "Escola La Caixa")
  (x 41.417084)
  (y 2.2030783))

([onto_Class190081] of  escuela

  (nombre "Escola La Farigola de Vallcarca")
  (x 41.4138071)
  (y 2.1466601))

([onto_Class190082] of  escuela

  (nombre "Escola La Farigola del Clot")
  (x 41.4063738)
  (y 2.1881951))

([onto_Class190083] of  escuela

  (nombre "Escola La Llacuna del Poblenou")
  (x 41.401944)
  (y 2.1982228))

([onto_Class190084] of  escuela

  (nombre "Escola La Maquinista")
  (x 41.4392114)
  (y 2.1960764))

([onto_Class190085] of  escuela

  (nombre "Escola La Mar Bella")
  (x 41.3997157)
  (y 2.2046842))

([onto_Class190086] of  escuela

  (nombre "Escola La Muntanyeta")
  (x 41.3701928)
  (y 2.1477366))

([onto_Class190087] of  escuela

  (nombre "Escola La Palmera")
  (x 41.42021949999999)
  (y 2.2043184))

([onto_Class190088] of  escuela

  (nombre "Escola La Pau")
  (x 41.42261)
  (y 2.2078443))

([onto_Class190089] of  escuela

  (nombre "Escola La Sedeta")
  (x 41.4055392)
  (y 2.1679143))

([onto_Class190091] of  escuela

  (nombre "Escola Les AcAcies")
  (x 41.4043814)
  (y 2.2046149))

([onto_Class190092] of  escuela

  (nombre "Escola Les Corts")
  (x 41.3841611)
  (y 2.1285809))

([onto_Class190093] of  escuela

  (nombre "Escola Lluis Vives")
  (x 41.3718413)
  (y 2.1281025))

([onto_Class190094] of  escuela

  (nombre "Escola Mallorca")
  (x 41.3912604)
  (y 2.1481641))

([onto_Class190095] of  escuela

  (nombre "Escola Mare de Deu de Montserrat")
  (x 41.426185)
  (y 2.1354))

([onto_Class190096] of  escuela

  (nombre "Escola Mare Nostrum")
  (x 41.4366896)
  (y 2.1549726))

([onto_Class190097] of  escuela

  (nombre "Escola Marinada")
  (x 41.4097896)
  (y 2.1368331))

([onto_Class190098] of  escuela

  (nombre "Escola Mas Casanovas")
  (x 41.4134251)
  (y 2.1705843))

([onto_Class190099] of  escuela

  (nombre "Escola MediterrAnia")
  (x 41.3796343)
  (y 2.1916223))

([onto_Class190100] of  escuela

  (nombre "Escola Merce Rodoreda")
  (x 41.4459162)
  (y 2.1854723))

([onto_Class190101] of  escuela

  (nombre "Escola Mestre Enric Gibert i Camins")
  (x 41.4314061)
  (y 2.1857498))

([onto_Class190102] of  escuela

  (nombre "Escola Mestre Morera")
  (x 41.459284)
  (y 2.168068))

([onto_Class190103] of  escuela

  (nombre "Escola MilA i Fontanals")
  (x 41.3815827)
  (y 2.1685687))

([onto_Class190104] of  escuela

  (nombre "Escola Miquel Bleach")
  (x 41.3757883)
  (y 2.1424537))

([onto_Class190105] of  escuela

  (nombre "Escola Miralletes")
  (x 41.4136304)
  (y 2.1795912))

([onto_Class190106] of  escuela

  (nombre "Escola Moli de Finestrelles")
  (x 41.4388551)
  (y 2.1895785))

([onto_Class190107] of  escuela

  (nombre "Escola Montseny")
  (x 41.41567269999999)
  (y 2.1485108))

([onto_Class190108] of  escuela

  (nombre "Escola Mossen Jacint Verdaguer")
  (x 41.3732491)
  (y 2.1546746))

([onto_Class190110] of  escuela

  (nombre "Escola Octavio Paz")
  (x 41.4142356)
  (y 2.1896022))

([onto_Class190111] of  escuela

  (nombre "Escola Orlandai")
  (x 41.3973349)
  (y 2.1195376))

([onto_Class190112] of  escuela

  (nombre "Escola Palma de Mallorca")
  (x 41.433053)
  (y 2.1774206))

([onto_Class190113] of  escuela

  (nombre "Escola Parc de la Ciutadella")
  (x 41.3849706)
  (y 2.187823))

([onto_Class190114] of  escuela

  (nombre "Escola Parc del Guinardo")
  (x 41.420026)
  (y 2.170208))

([onto_Class190115] of  escuela

  (nombre "Escola Pare Poveda")
  (x 41.4171196)
  (y 2.1414461))

([onto_Class190116] of  escuela

  (nombre "Escola Patronat Domenech")
  (x 41.39854529999999)
  (y 2.1581922))

([onto_Class190117] of  escuela

  (nombre "Escola Pau Casals")
  (x 41.4092619)
  (y 2.1617909))

([onto_Class190118] of  escuela

  (nombre "Escola Pau Casals-GrAcia")
  (x 41.4092619)
  (y 2.1617909))

([onto_Class190119] of  escuela

  (nombre "Escola Pau Romeva")
  (x 41.376472)
  (y 2.1133085))

([onto_Class190120] of  escuela

  (nombre "Escola Pau Vila")
  (x 41.367144)
  (y 2.142483))

([onto_Class190121] of  escuela

  (nombre "Escola Pegaso")
  (x 41.4283146)
  (y 2.1871543))

([onto_Class190122] of  escuela

  (nombre "Escola Pere IV")
  (x 41.4048552)
  (y 2.2003422))

([onto_Class190123] of  escuela

  (nombre "Escola Pere Vila")
  (x 41.3913349)
  (y 2.1819438))

([onto_Class190124] of  escuela

  (nombre "Escola Pit-Roig")
  (x 41.4235552)
  (y 2.1703646))

([onto_Class190125] of  escuela

  (nombre "Escola Poble-sec")
  (x 41.370617)
  (y 2.1646338))

([onto_Class190126] of  escuela

  (nombre "Escola Poblenou")
  (x 41.40994389999999)
  (y 2.1971638))

([onto_Class190127] of  escuela

  (nombre "Escola Poeta Foix")
  (x 41.4023921)
  (y 2.1460167))

([onto_Class190128] of  escuela

  (nombre "Escola Pompeu Fabra")
  (x 41.4214502)
  (y 2.1832162))

([onto_Class190129] of  escuela

  (nombre "Escola PrActiques")
  (x 41.3808554)
  (y 2.1387496))

([onto_Class190130] of  escuela

  (nombre "Escola Prim")
  (x 41.4201358)
  (y 2.211794))

([onto_Class190131] of  escuela

  (nombre "Escola Prosperitat")
  (x 41.4450119)
  (y 2.1813804))

([onto_Class190133] of  escuela

  (nombre "Escola Rambleta del Clot")
  (x 41.410509)
  (y 2.1896933))

([onto_Class190134] of  escuela

  (nombre "Escola Ramon Berenguer III")
  (x 41.4504911)
  (y 2.1924261))

([onto_Class190135] of  escuela

  (nombre "Escola Ramon Casas")
  (x 41.3587645)
  (y 2.1345234))

([onto_Class190136] of  escuela

  (nombre "Escola Ramon Llull")
  (x 41.40085620000001)
  (y 2.1777838))

([onto_Class190137] of  escuela

  (nombre "Escola Ramon y Cajal")
  (x 41.3989416)
  (y 2.1646707))

([onto_Class190138] of  escuela

  (nombre "Escola Reina Violant")
  (x 41.40415100000001)
  (y 2.1531782))

([onto_Class190139] of  escuela

  (nombre "Escola Rius i Taulet")
  (x 41.4079258)
  (y 2.1501678))

([onto_Class190140] of  escuela

  (nombre "Escola Ruben Dario")
  (x 41.37693489999999)
  (y 2.1658229))

([onto_Class190141] of  escuela

  (nombre "Escola Sagrada Familia")
  (x 41.391323)
  (y 2.147493))

([onto_Class190142] of  escuela

  (nombre "Escola Sant Marti")
  (x 41.4046843)
  (y 2.1977762))

([onto_Class190143] of  escuela

  (nombre "Escola Santiago Rusiñol")
  (x 41.4394788)
  (y 2.1804616))

([onto_Class190144] of  escuela

  (nombre "Escola Seat")
  (x 41.35927299999999)
  (y 2.1380781))

([onto_Class190145] of  escuela

  (nombre "Escola Splai")
  (x 41.4332767)
  (y 2.1740092))

([onto_Class190147] of  escuela

  (nombre "Escola Tabor")
  (x 41.4073412)
  (y 2.1792107))

([onto_Class190148] of  escuela

  (nombre "Escola Taxonera")
  (x 41.42630219999999)
  (y 2.1531435))

([onto_Class190149] of  escuela

  (nombre "Escola Tibidabo")
  (x 41.4427315)
  (y 2.1830661))

([onto_Class190150] of  escuela

  (nombre "Escola Timbaler del Bruc")
  (x 41.4283526)
  (y 2.1774355))

([onto_Class190151] of  escuela

  (nombre "Escola TomAs Moro")
  (x 41.4426041)
  (y 2.1636717))

([onto_Class190152] of  escuela

  (nombre "Escola Torrent d'en Melis")
  (x 41.417303)
  (y 2.1721575))

([onto_Class190153] of  escuela

  (nombre "Escola Torrent de Can Carabassa")
  (x 41.4269774)
  (y 2.1627147))

([onto_Class190154] of  escuela

  (nombre "Escola Tres Pins")
  (x 41.3671612)
  (y 2.1597577))

([onto_Class190155] of  escuela

  (nombre "Escola Turo Blau")
  (x 41.4279899)
  (y 2.191263))

([onto_Class190156] of  escuela

  (nombre "Escola Turo del Cargol")
  (x 41.41122499999999)
  (y 2.1525161))

([onto_Class190157] of  escuela

  (nombre "Escola Victor CatalA")
  (x 41.4395997)
  (y 2.1771915))

([onto_Class190158] of  escuela

  (nombre "Escola Vila Olimpica")
  (x 41.3940781)
  (y 2.2025578))

([onto_Class190159] of  escuela

  (nombre "Escola Antaviana")
  (x 41.44864159999999)
  (y 2.1786045))

([onto_Class190160] of  escuela

  (nombre "Escola Artistic Oriol Martorell")
  (x 41.441363)
  (y 2.172739))

([onto_Class190161] of  escuela

  (nombre "Escola Costa i Llobera")
  (x 41.401713)
  (y 2.108474))

([onto_Class190162] of  escuela

  (nombre "Escola El Til·ler")
  (x 41.438485)
  (y 2.202666))

([onto_Class190163] of  escuela

  (nombre "Escola Trinitat Nova")
  (x 41.447849)
  (y 2.1866944))

([onto_Class190164] of  escuela

  (nombre "Escola Turo de Roquete")
  (x 41.4465578)
  (y 2.1729558))





)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Final del codigo generado por Protege ;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass Recomendacion 
  (is-a Oferta)
  (role concrete)
  (slot oferta
    (type INSTANCE)
    (create-accessor read-write))
  (slot puntuacion
    (type INTEGER)
    (create-accessor read-write))
  (multislot justificaciones
    (type STRING)
    (create-accessor read-write))
)

;;; Fin de declaracion de clases propias --------------

;;; Declaracion de modulos ----------------------------

(defmodule MAIN (export ?ALL))

(defmodule prefs
  (import MAIN ?ALL)
  (export ?ALL)
)

(defmodule procesado
  (import MAIN ?ALL)
  (import prefs ?ALL)
  (export ?ALL)
)

(defmodule generacion
  (import MAIN ?ALL)
  (import prefs ?ALL)
  (import procesado ?ALL)
  (export ?ALL)
)

(defmodule filtro
  (import MAIN ?ALL)
  (import prefs ?ALL)
  (import procesado ?ALL)
  (import generacion ?ALL)
  (export ?ALL)
)

(defmodule presentacion
  (import MAIN ?ALL)
  (import prefs ?ALL)
  (import procesado ?ALL)
  (import generacion ?ALL)
  (import filtro ?ALL)
  (export ?ALL)
)

;;; Fin declaracion de modulos ------------------------


;;; Declaracion de messages ---------------------------

;; Imprime los datos de una Oferta
(defmessage-handler MAIN::Oferta imprimir ()
  (format t "Tipo de casa: %s" (class ?self))
  (printout t crlf)
  (format t "Precio: %d" ?self:precio)
  (printout t crlf)
  (format t "Habitaciones: %d" ?self:habitaciones)
  (printout t crlf)
  (format t "superficie: %d" ?self:superficie)
  (printout t crlf)
  (format t "terraza: %s" ?self:terraza)
  (printout t crlf)
  (format t "amueblada: %s" ?self:amueblada)
  (printout t crlf)
  (format t "piscina: %s" ?self:piscina)
  (printout t crlf)
  (format t "latitud: %f" ?self:latitud)
  (printout t crlf)
  (format t "longitud: %f" ?self:longitud)
  (printout t crlf)

)

(defmessage-handler MAIN::Recomendacion imprimir ()
  (printout t "-----------------------------------" crlf)
  (printout t (send ?self:oferta imprimir))
  (if (eq (class ?self:oferta) piso)
    then
    (printout t "Ascensor: " (send ?self:oferta get-ascensor-piso) crlf)
    (printout t "Planta: " (send ?self:oferta get-altura-piso) crlf)
    else
    (if (eq (class ?self:oferta) duplex)
      then
      (printout t "Ascensor: " (send ?self:oferta get-ascensor-duplex) crlf)
      (printout t "Planta: " (send ?self:oferta get-altura-duplex) crlf)
    )
  )
  (printout t crlf)
  (format t "Nivel de recomendacion: %d %n" ?self:puntuacion)
  (printout t "Justificacion: " crlf)
  (progn$ (?curr-just ?self:justificaciones)
    (printout t ?curr-just crlf)
  )
  (printout t crlf)
  (printout t "-----------------------------------" crlf)
)


; ;;; Fin declaracion de messages -----------------------



; ;;; Declaracion de funciones --------------------------

(deffunction MAIN::maximo-puntuacion ($?lista)
  (bind ?maximo -1)
  (bind ?elemento nil)
  (progn$ (?curr-rec $?lista)
    (bind ?curr-punt (send ?curr-rec get-puntuacion))
    (if (> ?curr-punt ?maximo)
      then 
      (bind ?maximo ?curr-punt)
      (bind ?elemento ?curr-rec)
    )
  )
  ?elemento
)

(deffunction MAIN::ask-question (?question $?allowed-values)
   (printout t ?question)
   (bind ?answer (read))
   (if (lexemep ?answer) 
       then (bind ?answer (lowcase ?answer)))
   (while (not (member ?answer ?allowed-values)) do
      (printout t ?question)
      (bind ?answer (read))
      (if (lexemep ?answer) 
          then (bind ?answer (lowcase ?answer))))
   ?answer)

(deffunction MAIN::ask-question-simple (?question)
    (printout t ?question)
    (bind ?answer (read))
    ?answer
) 
   
(deffunction MAIN::si-or-no-p (?question)
   (bind ?response (ask-question ?question si no y n))
   (if (or (eq ?response si) (eq ?response y))
       then TRUE
       else FALSE))


; ;;; Fin declaracion de funciones -----------------------
  
  
  
; ;;; Declaracion de reglas y hechos ---------------------

(defrule MAIN::initialRule "Regla inicial"
  (declare (salience 10))
  =>
  (printout t crlf)
  (printout t "     _" crlf)
  (printout t "  __| | ___  _ __ ___  _   _ _ __ ___ " crlf)
  (printout t " \/ _` |\/ _ \\| '_ ` _ \\| | | | '_ ` _ \\ " crlf)
  (printout t "| (_| | (_) | | | | | | |_| | | | | | |" crlf)
  (printout t " \\__,_|\\___\/|_| |_| |_|\\__,_|_| |_| |_|" crlf)


  (printout t crlf)   
  (printout t "A continuacion se le formularan una serie de preguntas para poder recomendarle adequadamente." crlf)
  (printout t crlf)
  (focus prefs)
)

(defrule prefs::determinar-nombre
    (not ( nombre ?))
    =>
    (bind ?nom (ask-question-simple "Cual es su nombre? "))
    (assert (nombre ?nom))
    (assert (p1))
)

(defrule prefs::determinar-edad-1 ""
 (p1)
 (not (generacion ?))
=>
  (bind ?eda (ask-question-simple "Cual es tu edad? "))
  (assert (edad ?eda))
  (if  (< ?eda 25)
    then 
     (assert (generacion joven))  
    else
     (if  (< ?eda 65) ; o 68?
      then
      (assert (generacion adulto))
    else 
    (assert (generacion jubilado))
    )
  )
  (assert (p2))
)

(defrule prefs::determinar-presupuesto-1 ""
  (p2)
 (not ( presupuesto ?))
=>
  (bind ?max (ask-question-simple "Cual es tu presupuesto? "))
  (assert (precio_max ?max)) 
  (if  (< ?max 500)
    then 
   (assert (presupuesto bajo))   
    else
     (if  (< ?max 1300)
      then
    (assert (presupuesto normal))
    else 
    (assert (presupuesto alto))
    )
  )
  (if (si-or-no-p "Te importa el precio? (si/no)? ")
    then 
      (assert (importancia-precio mucha))    
    else
      (assert (importancia-precio poca)) 
   )
   (assert (p3))
)

(defrule prefs::determinar-animales-1 ""
 (p3)
 (not (mascota ?))
=>
  (if (si-or-no-p "Tienes una mascota (si/no)? ")
    then 
   (assert (mascota SI))   
    else
   (assert (mascota NO)) 
    )
    (assert (p4))
)


(defrule prefs::determinar-habitaciones-1 ""
 (p4)
 (not (habitaciones ?))
=>
  (bind ?mha (ask-question-simple "Cual es el numero minimo de habitaciones que necesitas? "))
  (assert (habit_min ?mha))
  (if  (< ?mha 2)
    then 
     (assert (habitaciones pocas))     
    else
     (if  (< ?mha 4)
      then
      (assert (habitaciones normal))
    else 
    (assert (habitaciones muchas))
    )
  )
  (assert (p5))
)

(defrule prefs::determinar-espacio-1 ""
  (p5)
 (not (espacio ?))
=>
  (if (si-or-no-p "Necesitas un lugar espacioso (si/no)? ")
    then 
   (assert (espacio grande))   
    else
   (assert (espacio pequeno)) 
    )
    (assert (p6))
  
)

(defrule prefs::determinar-transporte-1 ""
 (p6)
 (not (transporte ?))
 (presupuesto normal|alto)
 (edad ?e)
 (test (> ?e 18))
=>
  (if (si-or-no-p "Tienes coche (si/no)? ")
    then 
   (assert (transporte coche))
     (if (si-or-no-p "Necesitas un garaje (si/no)? ")
       then (assert (necesita-garaje)))
    else
   (assert (transporte publico))
    )
    (assert (p7))
)

(defrule prefs::determinar-transporte-2 ""
 (p6)
 (not (transporte ?))
=>
  (assert (transporte publico))
  (assert (p7))
)

(defrule prefs::determinar-terraza-1 ""
 (p7)
 (not (terraza ?))
 (presupuesto normal|alto)
 (espacio grande)
=>
  (if (si-or-no-p "Necesitas terraza (si/no)? ")
    then 
   (assert (terraza SI)) 
    else
   (assert (terraza NO)) 
    )
    (assert (p8))
)

(defrule prefs::determinar-terraza-2 ""
 (p7)
 (not (terraza ?))
=>
  (assert (terraza NO))
  (assert (p8))
)

(defrule prefs::determinar-piscina-1 ""
 (p8)
 (not (piscina ?))
 (presupuesto normal|alto)
 (espacio grande)
 (habitaciones normal|muchas)
=>
  (if (si-or-no-p "Necesitas piscina (si/no)? ")
    then 
   (assert (piscina SI)) 
    else
   (assert (piscina NO)) 
    )
    (assert (p9))
)

(defrule prefs::determinar-piscina-2 ""
 (p8)
 (not (piscina ?))
=>
  (assert (piscina NO))
  (assert (p9))
)

(defrule prefs::determinar-amueblado-1 ""
 (p9)
 (not (amueblado ?))
 (presupuesto normal|alto)
=>
  (if (si-or-no-p "Necesitas que este amueblado (si/no)? ")
    then 
   (assert (amueblado SI)) 
    else
   (assert (amueblado NO)) 
    )
    (assert (p10))
)

(defrule prefs::determinar-amueblado-2 ""
 (p9)
 (not (amueblado ?))
=>
  (assert (amueblado NO))
  (assert (p10))
)

(defrule prefs::pasar_a_procesado "Pasa al modulo de procesado"
    (p10)
    =>
    (focus procesado)
)

(defrule procesado::estado-s-1 ""
  (presupuesto bajo|normal)
  (espacio pequeno)
  (habitaciones pocas)
  (not(estado-s ?))
  (generacion joven)
  =>
  (assert (estado-s estudiante))
)

(defrule procesado::estado-s-1-2 ""
  (presupuesto bajo|normal)
  (habitaciones normal|muchas)
  (not(estado-s ?))
  (generacion joven)
  =>
  (assert (estado-s grupo-de-estudiantes))
)


(defrule procesado::estado-s-2 ""
  (habitaciones normal|muchas)
  (not(estado-s ?))
  =>
  (assert (estado-s familia))
)

(defrule procesado::estado-s-3 ""
  (habitaciones pocas)
  (not(estado-s ?))
  =>
  (assert (estado-s soltero))
)

(defrule procesado::tipo-casa-1 ""
 (not (tipo-casa))
 (estado-s familia)
 =>
 (assert (duplex))
 (assert (unifamiliar))
 (assert (tipo-casa))
)

(defrule procesado::tipo-casa-2 ""
 (not(tipo-casa))
 (estado-s grupo-de-estudiantes|estudiante|individuo)
 =>
 (assert (piso))
 (assert (tipo-casa))
)

(defrule procesado::tipo-casa-3 ""
 (not (tipo-casa))
 (estado-s soltero)
 =>
 (assert (piso))
 (assert (duplex))
 (assert (tipo-casa))
)

(defrule procesado::ascensor-1 ""
 (not (ascensor ?))
 (piso)
 (generacion jubilado)
 =>
 (assert (ascensor SI))
)

(defrule procesado::ascensor-2 ""
 (not (ascensor ?))
 =>
 (assert (ascensor NO))
)

(defrule procesado::pasar_a_generacion "Pasa a modulo generacion y filtro"
    (estado-s ?)
    (tipo-casa)
    (ascensor ?)
    =>
    (focus generacion)
)


(defrule generacion::anadir-ofertas "Se anade todas las ofertas, luego se filtran"
  (not (gener))
  =>
  (assert (gener))
  (bind $?lista (find-all-instances ((?inst Oferta)) TRUE))
  (progn$ (?curr-con ?lista)
    (make-instance (gensym) of Recomendacion (oferta ?curr-con) (puntuacion 0))
  )
)



; ;;filtrar   

(defrule generacion::Pasar_a_filtro "Pasamos a filtrar"
    (gener)
    =>
    (focus filtro)
)

(defrule filtro::descartar-presupost-sup "Se descartan ofertas con presupuesto superior a presupuesto maximo"
  (importancia-precio mucha)
  (precio_max ?max)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) )
  (test (> (send ?of get-precio) ?max) )
  =>
  (send ?rec delete)
)

(defrule filtro::descartar-no-mascotas "Se descartan ofertas que no admiten mascotas si el usuario tiene mascota"
  (mascota SI)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) )
  (test (eq (send ?of get-admite-mascotas) FALSE))
  =>
  (send ?rec delete)
)

(defrule filtro::descartar-no-garajes "Se descartan ofertas que no admiten mascotas si el usuario tiene mascota"
  (coche SI)
  (necesita-garaje)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) )
  (test (eq (send ?of get-garaje) FALSE))
  =>
  (send ?rec delete)
)

(defrule filtro::descartar-jubil-no-ascensor-piso "Se puntua el tipo de casa que la abstraccion recomienda"
  (generacion jubilado)
  (ascensor NO)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  (test (eq (class ?of) piso))
  (test (> (send ?of get-altura-piso) 0))
  (test (eq (send ?of get-ascensor-piso) FALSE))
  =>
  (send ?rec delete)
)

(defrule filtro::descartar-jubil-no-ascensor-duplex "Se puntua el tipo de casa que la abstraccion recomienda"
  (generacion jubilado)
  (ascensor NO)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  (test (eq (class ?of) duplex))
  (test (> (send ?of get-altura-duplex) 0))
  (test (eq (send ?of get-ascensor-duplex) FALSE))
  =>
  (send ?rec delete)
)

(defrule filtro::descartar-menos-hab "Se puntua el tipo de casa que la abstraccion recomienda"
  (habitaciones normal)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  (test (< (send ?of get-habitaciones) 2))
  =>
  (send ?rec delete)
)

(defrule filtro::descartar-menos-hab "Se puntua el tipo de casa que la abstraccion recomienda"
  (habitaciones muchas)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  (test (< (send ?of get-habitaciones) 4))
  =>
  (send ?rec delete)
)

(defrule filtro::descartar-doble-precio "Se puntua el tipo de casa que la abstraccion recomienda"
  (precio_max ?pres)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  (test (> (send ?of get-precio) (* ?pres 2)))
  =>
  (send ?rec delete)
)


(defrule filtro::puntuar-aproximacion-precio-alza "Se puntua la aproximacion del precio de la vivienda"
  (precio_max ?pres)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  (not (aprox-precio ?rec))
  =>
  (bind ?p (+ ?p (max (- ?pres (abs (- ?pres (send ?of get-precio)))) 0)))
  (bind $?just (insert$ $?just (+ (length$ $?just) 1) (str-cat "se suma el precio de la vivienda ponderado -> +" (max (- ?pres (abs (- ?pres (send ?of get-precio)))) 0))))
  (send ?rec put-puntuacion ?p)
  (send ?rec put-justificaciones $?just)
  (assert (aprox-precio ?rec))
)

(defrule filtro::puntuar-tipo-piso "Se puntua el tipo de casa que la abstraccion recomienda"
  (piso)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  (test (eq (class ?of) piso))
  (not (punt-piso ?of))
  =>
  (bind ?p (+ ?p 1000))
  (bind $?just (insert$ $?just (+ (length$ $?just) 1) "Creemos que puede estar buscando un piso -> +1000"))
  (send ?rec put-puntuacion ?p)
  (send ?rec put-justificaciones $?just)
  (assert (punt-piso ?of))
)

(defrule filtro::puntuar-tipo-duplex "Se puntua el tipo de casa que la abstraccion recomienda"
  (duplex)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  (test (eq (class ?of) duplex))
  (not (punt-duplex ?of))
  =>
  (bind ?p (+ ?p 1000))
  (bind $?just (insert$ $?just (+ (length$ $?just) 1) "Creemos que puede estar buscando un duplex -> +1000"))
  (send ?rec put-puntuacion ?p)
  (send ?rec put-justificaciones $?just)
  (assert (punt-duplex ?of))
)

(defrule filtro::puntuar-tipo-unif "Se puntua el tipo de casa que la abstraccion recomienda"
  (unifamiliar)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  (test (eq (class ?of) unifamiliar))
  (not (punt-unif ?of))
  =>
  (bind ?p (+ ?p 1000))
  (bind $?just (insert$ $?just (+ (length$ $?just) 1) "Creemos que puede estar buscando una vivienda unifamiliar -> +1000"))
  (send ?rec put-puntuacion ?p)
  (send ?rec put-justificaciones $?just)
  (assert (punt-unif ?of))
)

(defrule filtro::puntuar-habitaciones-pocas "Se puntua el tipo de casa que la abstraccion recomienda"
  (habitaciones pocas)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  (test (< (send ?of get-habitaciones) 2))
  (not (hab-pocas ?of))
  =>
  (bind ?p (+ ?p 100))
  (bind $?just (insert$ $?just (+ (length$ $?just) 1) "pocas habitaciones -> +100"))
  (send ?rec put-puntuacion ?p)
  (send ?rec put-justificaciones $?just)
  (assert (hab-pocas ?of))
)

(defrule filtro::puntuar-habitaciones-normal "Se puntua el tipo de casa que la abstraccion recomienda"
  (habitaciones normal)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  (test (>= (send ?of get-habitaciones) 2))
  (test (< (send ?of get-habitaciones) 4))
  (not (hab-normal ?of))
  =>
  (bind ?p (+ ?p 100))
  (bind $?just (insert$ $?just (+ (length$ $?just) 1) "numero de habitaciones normal -> +100"))
  (send ?rec put-puntuacion ?p)
  (send ?rec put-justificaciones $?just)
  (assert (hab-normal ?of))
)

(defrule filtro::puntuar-habitaciones-muchas "Se puntua el tipo de casa que la abstraccion recomienda"
  (habitaciones muchas)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  (test (>= (send ?of get-habitaciones) 4))
  (not (hab-muchas ?of))
  =>
  (bind ?p (+ ?p 100))
  (bind $?just (insert$ $?just (+ (length$ $?just) 1) "numero de habitaciones muchas -> +100"))
  (send ?rec put-puntuacion ?p)
  (send ?rec put-justificaciones $?just)
  (assert (hab-muchas ?of))
)

(defrule filtro::puntuar-superficie-grande "Se puntua el tipo de casa que la abstraccion recomienda"
  (espacio grande)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  (not (sup-grand ?rec))
  =>
  (bind ?sup5 (* (send ?of get-superficie) 5))
  (bind ?p (+ ?p ?sup5))
  (bind $?just (insert$ $?just (+ (length$ $?just) 1) (str-cat "superficie -> +" ?sup5)))
  (send ?rec put-puntuacion ?p)
  (send ?rec put-justificaciones $?just)
  (assert (sup-grand ?rec))
)

(defrule filtro::puntuar-superficie-no-grande "Se puntua el tipo de casa que la abstraccion recomienda"
  (espacio pequeno)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  (not (sup-grand ?rec))
  =>
  (bind ?sup3 (send ?of get-superficie))
  (bind ?p (+ ?p ?sup3))
  (bind $?just (insert$ $?just (+ (length$ $?just) 1) (str-cat "superficie -> +" ?sup3)))
  (send ?rec put-puntuacion ?p)
  (send ?rec put-justificaciones $?just)
  (assert (sup-grand ?rec))
)

(defrule filtro::puntuar-piscina-si "Se puntua el tipo de casa que la abstraccion recomienda"
  (piscina SI)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  (test (eq (send ?of get-piscina) TRUE))
  (not (piscina-si ?of))
  =>
  (bind ?p (+ ?p 100))
  (bind $?just (insert$ $?just (+ (length$ $?just) 1) "el lugar tiene piscina -> +100"))
  (send ?rec put-puntuacion ?p)
  (send ?rec put-justificaciones $?just)
  (assert (piscina-si ?of))
)

(defrule filtro::puntuar-mascota-espacio-grande "Se puntua el tipo de casa que la abstraccion recomienda"
  (espacio grande)
  (mascota SI)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  (test (and (eq (send ?of get-admite-mascotas) TRUE) (>= (send ?of get-superficie) 100)))
  (not (masc-espacioso ?of))
  =>
  (bind ?p (+ ?p 100))
  (bind $?just (insert$ $?just (+ (length$ $?just) 1) "lugar espacioso para mascotas -> +100"))
  (send ?rec put-puntuacion ?p)
  (send ?rec put-justificaciones $?just)
  (assert (masc-espacioso ?of))
)

(defrule filtro::puntuar-mascota-terraza "Se puntua el tipo de casa que la abstraccion recomienda"
  (terraza SI)
  (mascota SI)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  (test (and (eq (send ?of get-admite-mascotas) TRUE) (eq (send ?of get-terraza) TRUE)))
  (not (masc-terraza ?of))
  =>
  (bind ?p (+ ?p 100))
  (bind $?just (insert$ $?just (+ (length$ $?just) 1) "hay terraza para mascota -> +100"))
  (send ?rec put-puntuacion ?p)
  (send ?rec put-justificaciones $?just)
  (assert (masc-terraza ?of))
)

(defrule filtro::puntuar-terraza-si "Se puntua el tipo de casa que la abstraccion recomienda"
  (terraza SI)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  (test (eq (send ?of get-terraza) TRUE))
  (not (terraza-si ?of))
  =>
  (bind ?p (+ ?p 100))
  (bind $?just (insert$ $?just (+ (length$ $?just) 1) "el lugar tiene terraza -> +100"))
  (send ?rec put-puntuacion ?p)
  (send ?rec put-justificaciones $?just)
  (assert (terraza-si ?of))
)

(defrule filtro::puntuar-amueblado-si "Se puntua el tipo de casa que la abstraccion recomienda"
  (amueblado SI)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  (test (eq (send ?of get-amueblada) TRUE))
  (not (amueblada-si ?rec))
  =>
  (bind ?p (+ ?p 250))
  (bind $?just (insert$ $?just (+ (length$ $?just) 1) "La oferta esta amueblada -> +250"))
  (send ?rec put-puntuacion ?p)
  (send ?rec put-justificaciones $?just)
  (assert (amueblada-si ?rec))
)

(defrule filtro::puntuar-ascensor-piso "Se puntua el tipo de casa que la abstraccion recomienda"
  (piso)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  (test (eq (class ?of) piso))
  (test (and (eq (send ?of get-ascensor-piso) TRUE) (>= (send ?of get-altura-piso) 1)))
  (not (asc-piso ?of))
  =>
  (bind ?p (+ ?p 100))
  (bind $?just (insert$ $?just (+ (length$ $?just) 1) "piso elevado tiene ascensor -> +100"))
  (send ?rec put-puntuacion ?p)
  (send ?rec put-justificaciones $?just)
  (assert (asc-piso ?of))
)

(defrule filtro::puntuar-ascensor-duplex "Se puntua el tipo de casa que la abstraccion recomienda"
  (duplex)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  (duplex ?of)
  (test (and (eq (send ?of get-ascensor-duplex) TRUE) (>= (send ?of get-altura-duplex) 1)))
  (not (asc-duplex ?of))
  =>
  (bind ?p (+ ?p 100))
  (bind $?just (insert$ $?just (+ (length$ $?just) 1) "duplex elevado tiene ascensor -> +100"))
  (send ?rec put-puntuacion ?p)
  (send ?rec put-justificaciones $?just)
  (assert (asc-duplex ?of))
)

(defrule filtro::puntuar-metro-cerca "Se puntua la distancia de la vivienda a distintos servicios"
  (transporte publico)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  ?serv <- (object (is-a parada_metro) (nombre ?nomb))
  (test (<= (+ (** (- (send ?of get-latitud) (send ?serv get-x)) 2) (** (- (send ?of get-longitud) (send ?serv get-y)) 2)) (** (/ 500 95722.3276320630) 2)))
  (not (metro-cerca ?rec))
  =>
  (bind ?p (+ ?p 100))
  (bind $?just (insert$ $?just (+ (length$ $?just) 1) (str-cat (str-cat (str-cat (str-cat "la vivienda esta a " (* (sqrt (+ (** (- (send ?of get-latitud) (send ?serv get-x)) 2) (** (- (send ?of get-longitud) (send ?serv get-y)) 2))) 95722.3276320630)) " m del metro ") ?nomb) " -> +100")))
  (send ?rec put-puntuacion ?p)
  (send ?rec put-justificaciones $?just)
  (assert (metro-cerca ?rec))
)

(defrule filtro::puntuar-bus-cerca "Se puntua la distancia de la vivienda a distintos servicios"
  (transporte publico)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  ?serv <- (object (is-a parada_bus) (nombre ?nomb))
  (test (<= (+ (** (- (send ?of get-latitud) (send ?serv get-x)) 2) (** (- (send ?of get-longitud) (send ?serv get-y)) 2)) (** (/ 500 95722.3276320630) 2)))
  (not (bus-cerca ?rec))
  =>
  (bind ?p (+ ?p 100))
  (bind $?just (insert$ $?just (+ (length$ $?just) 1) (str-cat (str-cat (str-cat (str-cat "la vivienda esta a " (* (sqrt (+ (** (- (send ?of get-latitud) (send ?serv get-x)) 2) (** (- (send ?of get-longitud) (send ?serv get-y)) 2))) 95722.3276320630)) " m del ") ?nomb) " -> +100")  ) )
  (send ?rec put-puntuacion ?p)
  (send ?rec put-justificaciones $?just)
  (assert (bus-cerca ?rec))
)

(defrule filtro::puntuar-tram-cerca "Se puntua la distancia de la vivienda a distintos servicios"
  (transporte publico)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  ?serv <- (object (is-a parada_tram) (nombre ?nomb))
  (test (<= (+ (** (- (send ?of get-latitud) (send ?serv get-x)) 2) (** (- (send ?of get-longitud) (send ?serv get-y)) 2)) (** (/ 1000 95722.3276320630) 2)))
  (not (tram-cerca ?rec))
  =>
  (bind ?p (+ ?p 100))
  (bind $?just (insert$ $?just (+ (length$ $?just) 1) (str-cat (str-cat (str-cat (str-cat "la vivienda esta a " (* (sqrt (+ (** (- (send ?of get-latitud) (send ?serv get-x)) 2) (** (- (send ?of get-longitud) (send ?serv get-y)) 2))) 95722.3276320630)) " m del tram ") ?nomb) " -> +100")))
  (send ?rec put-puntuacion ?p)
  (send ?rec put-justificaciones $?just)
  (assert (tram-cerca ?rec))
)

(defrule filtro::puntuar-zonaverde-cerca "Se puntua la distancia de la vivienda a distintos servicios"
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  ?serv <- (object (is-a zona_verde) (nombre ?nomb))
  (test (<= (+ (** (- (send ?of get-latitud) (send ?serv get-x)) 2) (** (- (send ?of get-longitud) (send ?serv get-y)) 2)) (** (/ 750 95722.3276320630) 2)))
  (not (zv-cerca ?rec))
  =>
  (bind ?p (+ ?p 50))
  (bind $?just (insert$ $?just (+ (length$ $?just) 1) (str-cat (str-cat (str-cat (str-cat "la vivienda esta a " (* (sqrt (+ (** (- (send ?of get-latitud) (send ?serv get-x)) 2) (** (- (send ?of get-longitud) (send ?serv get-y)) 2))) 95722.3276320630)) " m del parque ") ?nomb) " -> +75")))
  (send ?rec put-puntuacion ?p)
  (send ?rec put-justificaciones $?just)
  (assert (zv-cerca ?rec))
)

(defrule filtro::puntuar-mascota-zonaverde-cerca "Se puntua la distancia de la vivienda a distintos servicios"
  (mascota SI)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  ?serv <- (object (is-a zona_verde) (nombre ?nomb))
  (test (<= (+ (** (- (send ?of get-latitud) (send ?serv get-x)) 2) (** (- (send ?of get-longitud) (send ?serv get-y)) 2)) (** (/ 750 95722.3276320630) 2)))
  (not (masc-zv-cerca ?rec))
  =>
  (bind ?p (+ ?p 50))
  (bind $?just (insert$ $?just (+ (length$ $?just) 1) "zona verde cerca para mascotas -> +50"))
  (send ?rec put-puntuacion ?p)
  (send ?rec put-justificaciones $?just)
  (assert (masc-zv-cerca ?rec))
)

(defrule filtro::puntuar-unif-hipermercado-cerca "Se puntua la distancia de la vivienda a distintos servicios"
  (estado-s familia)
  (unifamiliar)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  ?serv <- (object (is-a hipermercado))
  (unifamiliar ?of)
  (test (<= (+ (** (- (send ?of get-latitud) (send ?serv get-x)) 2) (** (- (send ?of get-longitud) (send ?serv get-y)) 2)) (** (/ 1250 95722.3276320630) 2)))
  (not (unif-hiper ?rec))
  =>
  (bind ?p (+ ?p 100))
  (bind $?just (insert$ $?just (+ (length$ $?just) 1) "hipermercado cerca de vivienda unifamiliar -> +100"))
  (send ?rec put-puntuacion ?p)
  (send ?rec put-justificaciones $?just)
  (assert (unif-hiper ?rec))
)

(defrule filtro::puntuar-unif-escuela-cerca "Se puntua la distancia de la vivienda a distintos servicios"
  (estado-s familia)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  ?serv <- (object (is-a escuela) (nombre ?nomb))
  (test (<= (+ (** (- (send ?of get-latitud) (send ?serv get-x)) 2) (** (- (send ?of get-longitud) (send ?serv get-y)) 2)) (** (/ 1250 95722.3276320630) 2)))
  (not (unif-escuela ?rec))
  =>
  (bind ?p (+ ?p 100))
  (bind $?just (insert$ $?just (+ (length$ $?just) 1) (str-cat (str-cat (str-cat (str-cat "la vivienda esta a " (* (sqrt (+ (** (- (send ?of get-latitud) (send ?serv get-x)) 2) (** (- (send ?of get-longitud) (send ?serv get-y)) 2))) 95722.3276320630)) " m de la ") ?nomb) " -> +100")))
  (send ?rec put-puntuacion ?p)
  (send ?rec put-justificaciones $?just)
  (assert (unif-escuela ?rec))
)

(defrule filtro::puntuar-unif-hipermercado-media "Se puntua la distancia de la vivienda a distintos servicios"
  (estado-s familia)
  (unifamiliar)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  ?serv <- (object (is-a hipermercado))
  (unifamiliar ?of)
  (test (and (> (+ (** (- (send ?of get-latitud) (send ?serv get-x)) 2) (** (- (send ?of get-longitud) (send ?serv get-y)) 2)) (** (/ 1250 95722.3276320630) 2))
	(<= (+ (** (- (send ?of get-latitud) (send ?serv get-x)) 2) (** (- (send ?of get-longitud) (send ?serv get-y)) 2)) (** (/ 2000 95722.3276320630) 2))))
  (not (unif-hiper ?rec))
  =>
  (bind ?p (+ ?p 50))
  (bind $?just (insert$ $?just (+ (length$ $?just) 1) "hipermercado a distancia media de vivienda unifamiliar -> +50"))
  (send ?rec put-puntuacion ?p)
  (send ?rec put-justificaciones $?just)
  (assert (unif-hiper ?rec))
)

(defrule filtro::puntuar-unif-escuela-media "Se puntua la distancia de la vivienda a distintos servicios"
  (estado-s familia)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  ?serv <- (object (is-a escuela))
  (test (and (> (+ (** (- (send ?of get-latitud) (send ?serv get-x)) 2) (** (- (send ?of get-longitud) (send ?serv get-y)) 2)) (** (/ 1250 95722.3276320630) 2))
	(<= (+ (** (- (send ?of get-latitud) (send ?serv get-x)) 2) (** (- (send ?of get-longitud) (send ?serv get-y)) 2)) (** (/ 2000 95722.3276320630) 2))))
  (not (unif-escuela ?rec))
  =>
  (bind ?p (+ ?p 50))
  (bind $?just (insert$ $?just (+ (length$ $?just) 1) "escuela a distancia media de vivienda unifamiliar -> +50"))
  (send ?rec put-puntuacion ?p)
  (send ?rec put-justificaciones $?just)
  (assert (unif-escuela ?rec))
)

(defrule filtro::puntuar-jubilado-hipermercado-cerca "Se puntua la distancia de la vivienda a distintos servicios"
  (generacion jubilado)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  ?serv <- (object (is-a hipermercado))
  (test (<= (+ (** (- (send ?of get-latitud) (send ?serv get-x)) 2) (** (- (send ?of get-longitud) (send ?serv get-y)) 2)) (** (/ 1250 95722.3276320630) 2)))
  (not (jubil-hiper ?rec))
  =>
  (bind ?p (+ ?p 100))
  (bind $?just (insert$ $?just (+ (length$ $?just) 1) "hipermercado cerca de generacion jubilado -> +100"))
  (send ?rec put-puntuacion ?p)
  (send ?rec put-justificaciones $?just)
  (assert (jubil-hiper ?rec))
)

(defrule filtro::puntuar-jubilado-hipermercado-media "Se puntua la distancia de la vivienda a distintos servicios"
  (generacion jubilado)
  ?rec <- (object (is-a Recomendacion) (oferta ?of) (puntuacion ?p) (justificaciones $?just))
  ?serv <- (object (is-a hipermercado))
  (test (and (> (+ (** (- (send ?of get-latitud) (send ?serv get-x)) 2) (** (- (send ?of get-longitud) (send ?serv get-y)) 2)) (** (/ 1250 95722.3276320630) 2))
	(<= (+ (** (- (send ?of get-latitud) (send ?serv get-x)) 2) (** (- (send ?of get-longitud) (send ?serv get-y)) 2)) (** (/ 2000 95722.3276320630) 2))))
  (not (jubil-hiper ?rec))
  =>
  (bind ?p (+ ?p 50))
  (bind $?just (insert$ $?just (+ (length$ $?just) 1) "hipermercado a distancia media de generacion jubilado -> +50"))
  (send ?rec put-puntuacion ?p)
  (send ?rec put-justificaciones $?just)
  (assert (jubil-hiper ?rec))
)


(defrule filtro::pasar_a_presentacion "Pasamos a presentacion"
    (declare (salience -1))
    =>
    (focus presentacion)
)


(defrule presentacion::escriberesultado-1 ""
    (presupuesto ?p)
    (espacio ?e)
    (habitaciones ?h)
    (transporte ?t) 
    (piscina ?pis)
    (mascota ?mas)
    (terraza ?te)
    (amueblado ?a)
    (nombre ?nom)
    =>
    (printout t crlf)
    (format t "%s, creemos que te interesa una vivienda: %n precio: %s%n espacio: %s%n habitaciones: %s%n transporte: %s%n con piscina: %s%n admite mascota: %s%n con terraza: %s%n amueblado: %s%n " ?nom ?p ?e ?h ?t ?pis ?mas ?te ?a)
    (assert (e1))
)

(defrule presentacion::escriberesultado-2 "" 
    (e1)
    (necesita-garaje) 
    =>
    (format t "con garaje%n ")
)


(defrule presentacion::escriberesultado-3 "" 
    (duplex) 
    (e1)
    =>
    (format t "duplex%n ")
)

(defrule presentacion::escriberesultado-4 "" 
    (unifamiliar)
    (e1)    
    =>
    (format t "unifamiliar%n ")
)

(defrule presentacion::escriberesultado-5 "" 
    (piso) 
    (e1)
    =>
    (format t "piso ")
    (assert (e2))
)

(defrule presentacion::escriberesultado-6 "" 
    (ascensor SI)
    (e2)
    =>
    (format t "con ascensor%n ")
)

(defrule presentacion::escriberesultado-7 "" 
    (ascensor NO)
    (e2)
    =>
    (format t "%n ")
)


(defrule presentacion::print-las-recomendaciones-en-orden "Se printea una lista de recomendaciones"
  =>
  (bind $?lista (find-all-instances ((?inst Recomendacion)) TRUE))
  (if (eq (length$ $?lista) 0) then
    (printout t "No tenemos recomendaciones para ti." crlf)
  )
  (printout t crlf)
  (printout t "Recomendaciones:" crlf)
  (printout t crlf)
  (printout t "-----------------------------------" crlf)
  (bind ?k 1)
  (bind ?end false)
  (while (and (not (eq (length$ $?lista) 0)) (not (eq ?end true)))  do
    (bind ?curr-rec (maximo-puntuacion $?lista))
    ; (printout t (send ?curr-rec imprimir))
    (if (< ?k 6)
      then
      (bind ?punt (send ?curr-rec get-puntuacion))
      (if (> ?punt 1800)
        then
          (printout t "muy recomendable: " crlf)
        else
          (if (> ?punt 1500)
            then
              (printout t "adecuado: " crlf)
            else
              (printout t "parcialmente adecuado: " crlf)
          )
      )
      (bind $?lista (delete-member$ $?lista ?curr-rec))
      (printout t (send ?curr-rec imprimir))
      (bind ?k (+ ?k 1))
      else
      (if (si-or-no-p "ver 5 ofertas mas (si/no)? ")
        then 
        (bind ?k 1)
        else
        (bind ?end true)
      )
    )
  )
)